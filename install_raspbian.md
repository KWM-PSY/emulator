# Procedure to install the PlatformCommander emulator on a Raspberry pie running Raspbian

* Install Raspberry Pi OS as described [here](https://www.raspberrypi.org/software/).

* Install these packages:

```sh
apt-get install gcc make ruby ruby-dev git libgtk-3-dev libglib2.0-dev libglew-dev libxml2-dev libhidapi-dev libxcb-ewmh-dev mesa-utils libgl1-mesa-glx libx11-xcb-dev gobject-introspection libgirepository1.0-dev
```

* Install two *Ruby* `gems`:

```sh
gem install -V rake gtk3
```

* Copy the `PlatformCommander` archive (that you find
  [here](https://gitlab.com/KWM-PSY/emulator/-/blob/master/PlatformCommander_0.X.tar.bz2))
  to the home directory of your new user.

* Extract it somewhere, and change directory to the root of the
  extracted material (`PlatformCommander_0.X`).

```sh
  tar -xvf PlatformCommander_0.X.tar.bz2
``` 

* Copy *Ruby* script `cx.rb` to a place where it will be found by
  default:
  
  ```sh
  sudo cp scripts/cx.rb /usr/local/lib/site_ruby/
  ```

* Execute script `builder.rb`, which will compile the `PlatformCommander` material:

```sh
./builder.rb
```

**The script might once ask you for your password** in order to create
the default directories at the root of the disk space. 

If you receive no errors, the install procedure is completed. 

Open two terminal windows. On both, change to where you have
extracted the `PlatformCommander` material (`PlatformCommander_0.X`).

* On the first one, execute

```sh
./emu_runner.rb
```

The master program and the emulator will show their screens.

* On the second one, execute

```sh
./emu_demo.rb
```

that will execute a test script. You should now see the visualization 
of the plaform moving. 




