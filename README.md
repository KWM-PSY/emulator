**This repository provides a demo of PlatformCommander. The version is for testing purposes only. If you plan to install PlatformCommander and use it in combination with a motion platform, you might want to contact the devlopers (e.g. open an issue) for further instructions.**

This repository is part of PlatformCommander, follow this link for more information:
https://gitlab.com/KWM-PSY/platform-commander

We have written install instructions for Debian, Devuan, and Raspbian. 
**Update**
It seems there are problems with our Debian installation procedure in desktop environments other than xfce. A solution to the problem has been suggested here https://gitlab.com/KWM-PSY/platform-commander/-/issues/2 

The file install_debian.md describes how to install the emulator on a Debian system. This allows for running the emulator without a Rapsberry Pi. This is optimal for developing and testing of experiments.

The files install_devuan.md and install_raspbian.md describe the setup of the server on a Raspberry Pi. 
This allows to useing the client on a non-LINUX operating system while the emulator is running on a Raspberry Pi.
If you are a beginner, we recommend using raspbian as operating system. 
