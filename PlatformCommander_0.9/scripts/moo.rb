# moo.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Main include script.
Among others, defines protocol message constants

=end

require('cx')
require('carlobase')
require('maths')
require('moo')
require('tempfile')
require('hw')
require_relative('mailer')

module Moo
  #
  # Remember to add
  # Defaults env_keep+="MOOG DISPLAY"
  # in sudoers file
  #
  
  DEVMODE=ENV['MOOG']=='dev'
  
  BASE=File::absolute_path('./moog_base/')+'/'
  LOGBASE=BASE+'logs/'
  SESSBASE=BASE+'sessions/'
  CSSBASE=BASE+'css/'
  SHDBASE=BASE+'shaders/'
  AUDIOBASE=BASE+'audio_samples/'
  STILLBASE=BASE+'stills/'
  MOVIEBASE=BASE+'movies/'
  TRANSDUCERBASE=BASE+'transducer_samples/'
  PLYBASE=BASE+'ply/'

  MOVIE_INFO_APPEND='.INFO'

  TOPIC_FILE=BASE+'topic'

  DEG2RAD=Math::PI/180.0
  RAD2DEG=180.0/Math::PI
  
  SERVER_INPORT=ENV['SERVER_INPORT'] ? ENV['SERVER_INPORT'].to_i : 1966
  SERVER_OUTPORT=ENV['SERVER_OUTPORT'] ? ENV['SERVER_OUTPORT'].to_i : 1860

  CAM_DISPLAY=ENV['CAM_DISPLAY'] ? ENV['CAM_DISPLAY'] : ':100.0'
  HMD_DISPLAY=ENV['HMD_DISPLAY'] ? ENV['HMD_DISPLAY'] : ':100.1'
  HMD_OFFSET=ENV['HMD_OFFSET'] ? ENV['HMD_OFFSET'].split(',').map do |s|
                                   s.to_i
                                 end : [0,0]
  STIM_DISPLAY=ENV['STIM_DISPLAY'] ? ENV['STIM_DISPLAY'] : ':100.0'
  STIM_XYWH=ENV['STIM_XYWH'] ? ENV['STIM_XYWH'].split(',').map do |s|
                                 s.to_i
                               end : [0,0,640,480]

  AUDIO_BOARD=ENV['AUDIO_BOARD'] ? ENV['AUDIO_BOARD'].to_i : 0
  AUDIO_DSP=ENV['AUDIO_DSP'] ? ENV['AUDIO_DSP'].to_i : 0

=begin  
    SERVER_LOCAL_ADDRESS='10.30.1.11'
    LOCAL_ADDRESS='10.30.1.11'
    MOOG_ADDRESS='10.30.1.11'
    MOOG_LOCPORT=12020
    MOOG_REMPORT=12021
    SERVER_LOCAL_ADDRESS='130.92.154.46'
    LOCAL_ADDRESS='192.168.1.12'
    MOOG_ADDRESS='192.168.1.120'
    MOOG_LOCPORT=992
    MOOG_REMPORT=991
=end

  SERVER_LOCAL_ADDRESS=ENV['SERVER_LOCAL_ADDRESS'] ? ENV['SERVER_LOCAL_ADDRESS'] : '192.168.1.200'
  LOCAL_ADDRESS=ENV['LOCAL_ADDRESS'] ? ENV['LOCAL_ADDRESS'] : '192.168.1.12'
  MOOG_ADDRESS=ENV['MOOG_ADDRESS'] ? ENV['MOOG_ADDRESS'] : '192.168.1.120'
  MOOG_RUNNING_ON_PI=ENV['MOOG_RUNNING_ON_PI'] ? true : false

  MOO_USER=ENV['MOO_USER'] ? ENV['MOO_USER'] : 'moog'
  MOO_HOME=ENV['MOO_HOME'] ? ENV['MOO_HOME'] : '/home/moog/'

  MOOG_LOCPORT=992
  MOOG_REMPORT=991

  TRANSDUCER_VIRTUAL='TRANS-VIRT'
  
  SESSION_IMMEDIATE=1
  SESSION_SHORT_SEQ=2
  SESSION_LONG_SEQ=3
  SESSION_JOYSTICK=4
  SESSION_EFFE=5
  SESSION_MACACO=6

  MSG_LOGIN=0
  MSG_LOGIN_ACK=1
  MSG_LOGIN_NAK=2
  MSG_ENGAGE=3  
  MSG_ENGAGE_ACK=4
  MSG_ENGAGE_NAK=5
  MSG_ENGAGE_DONE=6
  MSG_PARK=7
  MSG_PARK_ACK=8
  MSG_PARK_NAK=9
  MSG_PARK_DONE=10
  MSG_LOGOUT=11
  MSG_LOGOUT_ACK=12
  MSG_LOGOUT_NAK=13
  MSG_IDLE_TIMEOUT=14
  MSG_LIST_SESSIONS=15
  MSG_LIST_SESSIONS_SESSION=16
  MSG_LIST_SESSIONS_NAK=17
  MSG_LIST_SESSIONS_DONE=18
  MSG_POST_PRODUCE=19
  MSG_POST_PRODUCE_NAK=20
  MSG_POST_PRODUCE_DONE=21
  MSG_ACTIVATE_HW_INPUT=22
  MSG_HW_INPUT_ACK=23
  MSG_HW_INPUT_NAK=24
  MSG_HW_INPUT_EVENT=25
  MSG_SEED_RNDGEN=26
  MSG_GET_BRANCH=27
  MSG_BRANCH=28
  MSG_GET_TOPIC=29
  MSG_TOPIC=30

  MSG_STATE=100
  MSG_DPIN_CHANGE=101
  MSG_PING=199

  MSG_IMMEDIATE_REQ=200
  MSG_IMMEDIATE_ACK=201
  MSG_IMMEDIATE_NAK=202
  MSG_IMMEDIATE_AT_HEIGHT=203

  MSG_BEGIN_SHORT_SEQ=300
  MSG_SHORT_SEQ_STEP=301
  MSG_COMPLETE_SHORT_SEQ=302
  MSG_SHORT_SEQ_ACK=303
  MSG_SHORT_SEQ_NAK=304
  MSG_SHORT_SEQ_AT_HEIGHT=305
  MSG_SHORT_SEQ_DONE=306
  MSG_SHORT_SEQ_FREEZE=307
  MSG_SHORT_SEQ_FREEZE_ACK=308
  
  MSG_BEGIN_LONG_SEQ=400
  MSG_LONG_SEQ_STEP=401
  MSG_COMPLETE_LONG_SEQ=402
  MSG_LONG_SEQ_ACK=403
  MSG_LONG_SEQ_NAK=404
  MSG_LONG_SEQ_AT_HEIGHT=405
  MSG_LONG_SEQ_AT_START=406
  MSG_LONG_SEQ_READY=407
  MSG_LONG_SEQ_EXECUTE=408
  MSG_LONG_SEQ_EXECUTE_NAK=409
  MSG_LONG_SEQ_DONE=410
  MSG_LONG_SEQ_FREEZE=411
  MSG_LONG_SEQ_FREEZE_ACK=412
  MSG_LONG_SEQ_STEP_SINEACC=413
  MSG_LONG_SEQ_STEP_DAMPEDOSC=414

  MSG_EFFE_ACK=500
  MSG_EFFE_NAK=501
  MSG_EFFE_CALIBRATE_VISOR=502
  MSG_EFFE_VISOR_CALIBRATED=503
  MSG_EFFE_SAVE_REFERENCE=504
  MSG_EFFE_EXERCISEMODE_ON=505
  MSG_EFFE_EXERCISEMODE_OFF=506
  MSG_EFFE_GET_IPD=507
  MSG_EFFE_SET_IPD=508
  MSG_EFFE_IPD_VAL=509
  MSG_EFFE_TESTMODE_ON=510
  MSG_EFFE_TESTMODE_OFF=511
  MSG_EFFE_GET_CURR_ANGLE=512
  MSG_EFFE_CURR_ANGLE=513
  MSG_EFFE_BEGIN_RUN=514
  MSG_EFFE_POSITION=515
  MSG_EFFE_RUN_TEST=516
  MSG_EFFE_EVALUATE=517
  MSG_EFFE_COMPLETE_RUN=518
  MSG_EFFE_PARK=519

  MSG_AV_AUDIO_ACK=600
  MSG_AV_AUDIO_NAK=601
  MSG_AV_OPEN_AUDIO=602
  MSG_AV_AUDIO_PLAY=603
  MSG_AV_AUDIO_STOP=604
  MSG_AV_AUDIO_STOP_ALL=605
  MSG_AV_AUDIO_SAMPLE_ENDED=606

  MSG_AV_STILLS_ACK=620
  MSG_AV_STILLS_NAK=621
  MSG_AV_OPEN_STILLS=622
  MSG_AV_STILLS_STILL=623
  MSG_AV_STILLS_FILL_RGB=624
  MSG_AV_STILLS_ACTIVATE=625
  MSG_AV_STILLS_DEACTIVATE=626
  MSG_AV_STILLS_GET_IPD=627
  MSG_AV_STILLS_SET_IPD=628
  MSG_AV_STILLS_IPD_VAL=629
  MSG_AV_STILLS_ROTATE_SCREEN=630

  MSG_AV_TRANSD_ACK=640
  MSG_AV_TRANSD_NAK=641
  MSG_AV_OPEN_TRANSD=642
  MSG_AV_TRANSD_PLAY=643
  MSG_AV_TRANSD_STOP=644
  MSG_AV_TRANSD_CHANNEL_IDLE=645

  MSG_AV_MOVIES_ACK=660
  MSG_AV_MOVIES_NAK=661
  MSG_AV_OPEN_MOVIES=662
  MSG_AV_MOVIES_MOVIE=663
  MSG_AV_MOVIES_ROTATE_SCREEN=664
  MSG_AV_MOVIES_PAUSE=665
  MSG_AV_MOVIES_FILL_RGB=666
  MSG_AV_MOVIES_GET_IPD=667
  MSG_AV_MOVIES_SET_IPD=668
  MSG_AV_MOVIES_IPD_VAL=669
  
  MSG_AV_INQUIRE=680
  MSG_AV_INQUIRE_REPLY=681

  MSG_MACACO_REGENERATE_TRIAS=700
  MSG_MACACO_RUN_TASK=701
  MSG_MACACO_ABORT_TASK=702
  MSG_MACACO_TASK_ACK=703
  MSG_MACACO_TASK_NAK=704
  MSG_MACACO_TASK_COMPLETED=705
  MSG_MACACO_GET_IPD=706
  MSG_MACACO_SET_IPD=707
  MSG_MACACO_IPD_VAL=708
  MSG_MACACO_PARK=709
  MSG_MACACO_ENGAGED=710
  MSG_MACACO_BEGIN_CAPTURE=711
  MSG_MACACO_END_CAPTURE=712
  MSG_MACACO_TESTMODE_ON=713
  MSG_MACACO_TESTMODE_OFF=714
  MSG_MACACO_RUN_TASK_RING=715

  SESSION_TYPES={SESSION_IMMEDIATE=>'Immediate',
                 SESSION_SHORT_SEQ=>'Short sequences',
                 SESSION_LONG_SEQ=>'Long sequences',
                 SESSION_JOYSTICK=>'Joystick',
                 SESSION_EFFE=>'Effe',
                 SESSION_MACACO=>'Macaco',
                }

  COMMAND_MODES={Moo::MODE_LENGTH=>'Actuator lengths',
                 Moo::MODE_DOF=>'Degrees-of-freedom',
                 Moo::MODE_MDA=>'MDA',
                 Moo::MODE_SPARE=>'Spare'}
  
  CRITICAL_MOOG_STATES=[Moo::MACH_STATE_FAULT1,Moo::MACH_STATE_FAULT2,Moo::MACH_STATE_FAULT3,Moo::MACH_STATE_INHIBITED]

  DEFAULT_MAX_SPEED=0.05 # in M/S
  DEFAULT_ROT_CENTER=[0.0,0.0,0.0].pack('fff')

  QLABELS=['Qx','Qy','Qz','Qw']
         
  FREEZE_KEY='***FrEeZe***'

  PING_PORT=11339
  PING_INTERVAL=0.2

  GPAD_NAMES=['GreenAsia_Inc._USB_Joystick-e','MY-POWER_CO._LTD._USB_Joystick-e','DragonRise_Inc._Generic_USB_Joystick-e']

  def self::parse_master_pkt(pkt)
    a=pkt.split(',')
    return "Packet incomplete" if(a.length<2)
    [a[0].to_i,a[1].to_i,a[2..-1]]
  end

  def self::prepare_master_pkt(tstamp,msgid,body)
    sprintf('%d,%d%s',tstamp,msgid,body ? ','+body : '')
  end

  def self::parse_client_pkt(pkt)
    a=pkt.split(',')
    return "Packet incomplete" if(a.length<1)
    [a[0].to_i]+a[1..-1]    
  end

  def self::prepare_client_pkt(msgid,body=nil)
    sprintf('%d%s',msgid,body ? ','+body : '')
  end

  def self::find_input_device(name)
    base='/dev/input/by-id/'
    regex=/#{name}/
    found=nil
    Dir::foreach(base) do |n|
      next if(n[0,1]=='.')
      if(regex.match(n))
        found=File::readlink(base+n)
        break
      end
    end
    return false unless(found)
    '/dev/input/'+found.split('/')[-1]
  end

  def self::find_serial_device(name)
    base='/dev/serial/by-id/'
    return false unless(File::directory?(base))
    
    regex=/#{name}/
    found=nil
    Dir::foreach(base) do |n|
      next if(n[0,1]=='.')
      if(regex.match(n))
        found=File::readlink(base+n)
        break
      end
    end
    return false unless(found)
    '/dev/'+found.split('/')[-1]
  end

  def self::read_info(base)
    fn=base+Session::INFO_FILE
    return "Info file #{fn} not found!" unless(File::file?(fn))

    h={}
    File::foreach(fn) do |l|
      next if(l[0,1]=='#')
      a=l.chomp.split('#')
      next unless(a.length==2)
      return "Info file #{fn}: tag #{a[0]} doubly specified!" if(h[a[0]])
      h[a[0].to_sym]=a[1]
    end    

    h
  end

  def self::comedi_device_detector(dev)
    df='/proc/comedi'
    raise 'COMEDI not present' unless(File::file?(df))
    found=false
    File::foreach(df) do |l|
      next unless(l[2,1]==':')
      tag=l[25,20].strip
      return l[0,2].to_i if(/#{dev}/.match(tag))
    end
    raise "COMEDI device [#{dev}] not found"
  end

  def self::parse_col(s)
    return "Bad colour (#{s}: should be 6 hex digits (RRGGBB))" unless(/^[[:xdigit:]]{6}$/.match(s))

    [s[0,2].to_i(16),s[2,2].to_i(16),s[4,2].to_i(16)]
  end
  def self::parse_col_val(s)
    v=parse_col(s)
    return v if(v.is_a?(String))

    (v[0]<< 16)|(v[1]<< 8)|v[2]
  end

# def self::oper_base(s)
#   sprintf('/home/moog/b/%s/',s)
# end
  
  class Platf_tria
    attr_reader(:d1,:d2,:bord,:tr,:joints)
    
    def initialize(d1,d2,bord,ang)
      tp=Math::tan(Math::PI/3.0)
      
      @d1,@d2,@bord,@ang=d1,d2,bord,ang
      
      @d1half=@d1*0.5
      @l=@d1*2.0+@d2
      @lhalf=@l*0.5
      @h=tp*@lhalf
      @hthird=@h/3.0
      @htwothirds=@h-@hthird
      @small_h=tp*@d1half
      @bord_l=4.0*@bord/tp

      #
      # Triangle vertices  (including missing tips)
      #
      
      @tr=[[-@lhalf-@bord_l*0.75,0.0,@hthird+@bord],[0.0,0.0,-@htwothirds-@bord*2],[@lhalf+@bord_l*0.75,0.0,@hthird+@bord]]

      #
      # The six points where the actuators connect
      #

      @joints=[[[-@lhalf+@d1,0.0,@hthird],[-@lhalf+@d1half,0.0,@hthird-@small_h]],
               [[-@d1half,0.0,-@htwothirds+@small_h],[@d1half,0.0,-@htwothirds+@small_h]],
               [[@lhalf-@d1half,0.0,@hthird-@small_h],[@lhalf-@d1,0.0,@hthird]]]

      if(@ang)
        mr=Maths::composed_mat([[Maths::MATTYPE_ROTAXIS,0.0,1.0,0.0,@ang]])
        @tr=Maths::apply_mat_to_dots(mr,@tr)
        jtemp=Maths::apply_mat_to_dots(mr,@joints[0]+@joints[1]+@joints[2])
        @joints=[jtemp[0,2],jtemp[2,2],jtemp[4,2]]
      end
    end
    
    def point_to_s(pt)
      sprintf("{%.3f,%.3f,%.3f}",pt[0],pt[1],pt[2])
    end
    
    def to_s
      <<EOF
{{#{point_to_s(@tr[0])},#{point_to_s(@tr[1])},#{point_to_s(@tr[2])}},
 {#{point_to_s(@joints[0][0])},#{point_to_s(@joints[0][1])}},
 {#{point_to_s(@joints[1][0])},#{point_to_s(@joints[1][1])}},
 {#{point_to_s(@joints[2][0])},#{point_to_s(@joints[2][1])}}}
EOF
    end
    
    def self::dist(a1,a2)
      d1=a2[0]-a1[0]
      d2=a2[1]-a1[1]
      len=Math::sqrt(d1*d1+d2*d2)
      lg("From #{a1} to #{a2}: #{len}")
      len
    end
  end

  class Session          
    INFO_FILE='info'
    SESS_FILE='.sess'
    VIVE_FILE='.vive'
    POSTPROD_FILE='post_prod.zip'
    POSTPROD_DIR='post_prod/'
    
    SSTATE_IDLE=0
    SSTATE_ENGAGING=1
    SSTATE_ENGAGED=2
    SSTATE_PARKING=3
    SSTATE_FAULT=4
    SSTATE_FROZEN=5

    SSTATE_DESC={SSTATE_IDLE=>'Idle',SSTATE_ENGAGING=>'Engaging',SSTATE_ENGAGED=>'Engaged',
      SSTATE_PARKING=>'Parking',SSTATE_FAULT=>'Fault',SSTATE_FROZEN=>'Frozen'}
    
    ACCSIN_TYPES={
      BOTH_LIN_ANG_ACC:Maths::Accsin::BOTH_LIN_ANG_ACC,
      BOTH_LIN_ANG_DISP:Maths::Accsin::BOTH_LIN_ANG_DISP,
      BOTH_ANG_LIN_ACC:Maths::Accsin::BOTH_ANG_LIN_ACC,
      BOTH_ANG_LIN_DISP:Maths::Accsin::BOTH_ANG_LIN_DISP,
      LIN_ACC_ANG_ACC_TIME:Maths::Accsin::LIN_ACC_ANG_ACC_TIME,
      LIN_ACC_ANG_DISP_TIME:Maths::Accsin::LIN_ACC_ANG_DISP_TIME,
      LIN_DISP_ANG_ACC_TIME:Maths::Accsin::LIN_DISP_ANG_ACC_TIME,
      LIN_DISP_ANG_DISP_TIME:Maths::Accsin::LIN_DISP_ANG_DISP_TIME,
    }
  end

  class Sess_effe
    LOGFORMAT=3
    OPER_H=-0.199
    MAX_ANG=21.75*Moo::DEG2RAD
    MAX_OFFSET_ANG=90*Moo::DEG2RAD
  end

  class Sess_macaco
    OPER_H=-0.205
    HEAVE_MIN=-0.221
  end
 
  class Platf
    #
    # These measurement from empirical work
    #
    #SHORT_SIDE=175.0#200.0
    #LONG_SIDE_LOWER=1230.0
    #LONG_SIDE_UPPER=850.0
    
    #
    # These measurement from Moog's blueprint
    #
    
    SHORT_SIDE=203.1
    LONG_SIDE_LOWER=1262.3
    LONG_SIDE_UPPER=888.4    
    
    BORD=80.0
    
    def self::trias
      tlower=Platf_tria::new(SHORT_SIDE,LONG_SIDE_LOWER,BORD,nil)
      tupper=Platf_tria::new(SHORT_SIDE,LONG_SIDE_UPPER,BORD,-60*Math::PI/180.0)
      [tlower,tupper,(tlower.tr+tlower.joints+tupper.tr+tupper.joints).flatten.pack('f*')]
    end
  end
  
  class Effe_test
    ACTIVE=0
    PASSIVE=1
    IMAG=2
    GUIDED=3
    DECOUPLED=4

    N_TYPES=5
    
    TESTS={ACTIVE=>["ACT","Active"],PASSIVE=>["PAS","Passive"],IMAG=>["IMG","Imagination"],
           GUIDED=>["GUI","Guided"],DECOUPLED=>["DEC","Decoupled"]}
    IDS={}
    TESTS.each do |k,v|
      raise "Sorry! Effe test #{v[0]} doubly defined!" if(IDS[v[0]])
      IDS[v[0]]=k
    end
  end

  class Macaco_task
    VESTIB=0
    VISUAL=1
    COMB=2

    TASKS={VESTIB=>["VSB","Vestibular",false,true],VISUAL=>["VSL","Visual",true,false],COMB=>["CMB","Combined",true,true]}

    IDS={}
    TASKS.each do |k,v|
      raise "Sorry! Macaco task #{v[0]} doubly defined!" if(IDS[v[0]])
      IDS[v[0]]=k
    end
  end
end

