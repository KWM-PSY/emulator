# main_process.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

The process to run at boot.

Manages a GTK status screen, and the master class instance

=end

require_relative('master')
require_relative('gtk')
require('optparse')

class Mb < Gtk::Button
  MARG=8
  
  def initialize(lbl,cls)
    super(label:lbl)
    set_property(:margin,MARG)

    style_context.add_class(cls)

    signal_connect('clicked') do
      yield
    end
  end
end

class Session_info < Gtk::Fixed
  W=800
  H=80

  BKG_NOSESSION="#ffbbbb"
  BKG_SESSION="#bbffbb"
  
  def initialize(mp)
    @mp=mp
    
    super()
                                                         
    set_size_request(W,H)
    
    @da=Gtk::DrawingArea::new
    @da.set_size_request(W,H)
    @da.signal_connect('draw') do |w,cr|        
      layout=cr.create_pango_layout
      layout.font_description=Pango::FontDescription.new('Monospace')
      
      if(!@mp.latest_session)
        cr.set_source_color(BKG_NOSESSION)
        cr.paint
        
        cr.update_pango_layout(layout)
        
        layout.set_markup("<span font='30' foreground='#2345aa'>Session not underway</span>")

        l=layout.iter.line_readonly
        sw=l.pixel_extents[1].width
        cr.move_to((w.allocation.width-sw)/2.0,w.allocation.height-(w.allocation.height-l.pixel_extents[1].height)/2.0)
        cr.show_pango_layout_line(l)
        
        cr.show_page
      else
        cr.set_source_color(BKG_SESSION)
        cr.paint
        
        cr.update_pango_layout(layout)        
        layout.set_markup("<span font='15' foreground='black'>Session type </span><span font='15' foreground='#d02020'>#{Moo::SESSION_TYPES[@mp.latest_session.type]}</span>")

        cr.move_to(15,20)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        cr.update_pango_layout(layout)        
        layout.set_markup("<span font='15' foreground='black'>From </span><span font='15' foreground='#d02020'>#{@mp.latest_session.rem_addr}/#{@mp.latest_session.inport}/#{@mp.latest_session.outport}</span>")

        cr.move_to(15,45)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        cr.update_pango_layout(layout)        
        layout.set_markup("<span font='15' foreground='black'>Begun </span><span font='15' foreground='#d02020'>#{@mp.latest_session.tstart_ruby.to_s}</span>")

        cr.move_to(15,70)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        cr.show_page
      end
      
      true
    end

    put(@da,0,0)
  end

  def touch
    @da.queue_draw()
  end  
end

class Status_info < Gtk::Fixed
  W=800
  H=50
  BKG_SESSION="#dddddd"

  def initialize(mp)
    @mp=mp
    
    super()
                                                         
    set_size_request(W,H)
    
    @da=Gtk::DrawingArea::new
    @da.set_size_request(W,H)

    @da.signal_connect('draw') do |w,cr|
      if(!@mp.latest_states)
        cr.set_source_color('black')
        cr.paint
      else
        cr.set_source_color(BKG_SESSION)
        cr.paint
        
        layout=cr.create_pango_layout
        layout.font_description=Pango::FontDescription.new('Monospace')        
        cr.update_pango_layout(layout)
        
        layout.set_markup("<span font='20' foreground='black'>MST: </span><span font='25' foreground='#d02020'>#{Moo::Session::SSTATE_DESC[@mp.latest_states[0]]}</span>")
        cr.move_to(15,35)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        layout.set_markup("<span font='20' foreground='black'>SRV: </span><span font='25' foreground='#d02020'>#{Moo::SRVR_STATE_LABELS[@mp.latest_states[1]]}</span>")
        cr.move_to(W/3.0,35)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        layout.set_markup("<span font='20' foreground='black'>PLF: </span><span font='25' foreground='#d02020'>#{Moo::MACH_STATE_LABELS[@mp.latest_states[2]]}</span>")
        cr.move_to((W/3.0)*2.0,35)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        cr.show_page
      end
      
      true
    end

    put(@da,0,0)
  end

  def touch
    @da.queue_draw()
  end  
end

class Status2_info < Gtk::Fixed
  W=800
  H=50
  BKG_SESSION="#dddddd"

  def initialize(mp)
    @mp=mp
    
    super()
                                                         
   set_size_request(W,H)
    
    @da=Gtk::DrawingArea::new
    @da.set_size_request(W,H)
    @da.signal_connect('draw') do |w,cr|
      if(!@mp.latest_states2)
        cr.set_source_color('black')
        cr.paint
      else
        cr.set_source_color(BKG_SESSION)
        cr.paint
        
        layout=cr.create_pango_layout
        layout.font_description=Pango::FontDescription.new('Monospace')
        
        cr.update_pango_layout(layout)        
        layout.set_markup("<span font='25' foreground='black'>Feedb: </span><span font='25' foreground='#d02020'>#{@mp.latest_states2[0]==0 ? 'Actu' : 'DOF'}</span>")

        cr.move_to(15,35)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        cr.update_pango_layout(layout)        
        layout.set_markup("<span font='25' foreground='black'>Mode: </span><span font='25' foreground='#d02020'>#{Moo::MODE_LABELS[@mp.latest_states2[1]]}</span>")
        cr.move_to(W/2,35)
        cr.show_pango_layout_line(layout.iter.line_readonly)
        
        cr.show_page
      end
      
      true
    end

    put(@da,0,0)
  end

  def touch
    @da.queue_draw()
  end  
end

class Status3_info < Gtk::Box
  W=800
  H=100
  WBTN=150
  BKG_SESSION="#dddddd"
  TXT_COL="#1892ed"
  DEF_MSGSIZE=35
  
  def initialize(mp)
    @mp=mp
    
    super(:horizontal)
    set_size_request(W,H)

    @msg=nil

    @freeze_btn=Mb::new("FREEZE\nSEQUENCE",'abobtn') do
      if(@mp.master && @mp.master.session)
        @mp.master.freeze_session
      end
    end
    
    @abort_btn=Mb::new("ABORT\nSESSION",'abobtn') do
      if(@mp.master && @mp.master.session)
        @mp.master.abort_session
      end
    end
    
    @freeze_btn.set_size_request(WBTN-Mb::MARG*2,H-Mb::MARG*2)
    pack_start(@freeze_btn,:expand=>false,:fill=>false)
    @abort_btn.set_size_request(WBTN-Mb::MARG*2,H-Mb::MARG*2)
    pack_start(@abort_btn,:expand=>false,:fill=>false)
    @da=Gtk::DrawingArea::new
    @da.set_size_request(W-WBTN*2,H)
    @da.signal_connect('draw') do |w,cr|
      if(@msg==nil)
        cr.set_source_color('black')
        cr.paint
      else
        cr.set_source_color(BKG_SESSION)
        cr.paint

        layout=cr.create_pango_layout
        layout.font_description=Pango::FontDescription.new('Monospace')
        
        cr.update_pango_layout(layout)        
        layout.set_markup("<span font='#{@msgsize}' foreground='#{TXT_COL}'>#{@msg}</span>")

        cr.move_to(15,H*0.75)
        cr.show_pango_layout_line(layout.iter.line_readonly)
      end
      
      true
    end

    pack_start(@da,:expand=>false,:fill=>false)
    touch
  end

  def touch(msg=nil,msgsize=DEF_MSGSIZE)
    @msg,@msgsize=msg,msgsize
    @freeze_btn.set_sensitive(@msg ? true : false)
    @abort_btn.set_sensitive(@msg ? true : false)
    @da.queue_draw()
  end  
end

class Indicators_info < Gtk::Fixed
  W=800
  H=300
  MARG=4
  COLS=4
  ROWS=6
  PER_COL=W.to_f/COLS
  PER_ROW=H.to_f/ROWS
  BW=PER_COL-MARG*2
  BH=PER_ROW-MARG*2
  
  BKG_SESSION='#dddddd'
  GRN='#33bb33'
  RED='#ff3333'
  ORNG='#f39013'  

  INDICATORS={Moo::RESP_ESTOP=>[0,0,RED],Moo::RESP_SNUBBER=>[0,1,RED],
              Moo::RESP_ACTU_RUNAWAY=>[0,2,RED],Moo::RESP_BATT_FAULT=>[0,3,RED],
              Moo::RESP_LOW_IDLE_RATE=>[1,0,RED],Moo::RESP_MOTOR_THERMAL_FAULT=>[1,1,RED],
              Moo::RESP_RANGE_ERROR=>[1,2,RED],Moo::RESP_INVALID_FRAME=>[1,3,RED],
              Moo::RESP_WATCHDOG=>[2,0,RED],Moo::RESP_LIMIT_SWITCH_FAULT=>[2,1,RED],
              Moo::RESP_DRIVE_BUS_FAULT=>[2,2,RED],Moo::RESP_AMPLIFIER_FAULT=>[2,3,RED],
              Moo::RESP_COMM_FAULT=>[3,0,RED],Moo::RESP_HOMING_FAULT=>[3,1,RED],
              Moo::RESP_ENVELOPE_FAULT=>[3,2,RED],Moo::RESP_TORQUE_MON_FAULT=>[3,3,RED],
              Moo::RESP_ESTOP_SENSE=>[4,0,ORNG],Moo::RESP_AMPLI_ENABLE=>[4,1,GRN],
              Moo::RESP_DRIVE_BUS_SENSE=>[4,2,GRN],Moo::RESP_LIMIT_SHUNT=>[4,3,ORNG],
              Moo::RESP_LIMIT_SWITCH_SENSE=>[5,0,ORNG],Moo::RESP_AMPLI_FAULT=>[5,1,ORNG],
              Moo::RESP_THERMAL_FAULT_SENSE=>[5,2,ORNG],Moo::RESP_BASE_AT_HOME=>[5,3,GRN]}

  def initialize(mp)
    @mp=mp
    
    super()
    
    set_size_request(W,H)
    @da=Gtk::DrawingArea::new
    @da.set_size_request(W,H)
    @da.signal_connect('draw') do |w,cr|
      if(!@mp.latest_session)
        cr.set_source_color('black')
        cr.paint
      else
        cr.set_source_color(BKG_SESSION)
        cr.paint

        layout=cr.create_pango_layout
        layout.font_description=Pango::FontDescription.new('Monospace')       

        li=@mp.latest_indicators
        li.each do |ind|
          a=INDICATORS[ind]
          raise "INDICATOR #{ind} NOT FOUND" unless(a)
          x=a[1]*PER_COL
          y=a[0]*PER_ROW

          cr.set_source_color(a[2])
          cr.rectangle(x+MARG,y+MARG,BW,BH)
          cr.fill

          cr.update_pango_layout(layout)
          layout.set_markup("<span font='15' foreground='white'>#{Moo::RESP_LABELS[ind]}</span>")

          cr.move_to(x+MARG*2,y+PER_ROW/2.0)
          cr.show_pango_layout_line(layout.iter.line_readonly)
        end if(li)
      end
        
      cr.show_page
            
      true
    end

    put(@da,0,0)
  end

  def touch
    @da.queue_draw()
  end  
end

class Buttons_info < Gtk::Fixed
  W=800
  H=75
  MARG=5
  HB=H-MARG*2
  COL='#101090'
  WCOL='#ff7040'

  def initialize(mp,n_btns)
    @mp,@n_btns=mp,n_btns
    @per_btn=W.to_f/@n_btns
    @wb=@per_btn-MARG*2

    @valid=true
    @lbls=[]    
    @cur_cond=Array::new(@n_btns,false)
    
    super()

    @da=Gtk::DrawingArea::new
    @da.set_size_request(W,H)

    @da.signal_connect('draw') do |w,cr|
      unless(@valid)
        cr.set_source_color(WCOL)
        cr.paint

        layout=cr.create_pango_layout
        layout.font_description=Pango::FontDescription.new('Monospace')
        layout.set_markup("<span font='45' foreground='yellow'>BUTTONS ARE INACTIVE</span>")

        l=layout.iter.line_readonly
        sw=l.pixel_extents[1].width

        cr.move_to((w.allocation.width-sw)/2.0,w.allocation.height*0.7)
        cr.show_pango_layout_line(l)
      else
        cr.set_source_color('black')
        cr.paint
        
        layout=cr.create_pango_layout
        layout.font_description=Pango::FontDescription.new('Monospace')
        
        @cur_cond.each_with_index do |cond,i|
          next unless(@lbls[i] && cond)
          pos=i*@per_btn
          cr.set_source_color(COL)
          cr.rectangle(pos+MARG,MARG,@wb,HB)
          cr.fill
          
          layout.set_markup("<span font='15' foreground='white'>#{@lbls[i]}</span>")
          
          cr.move_to(pos+MARG*2,H/2)
          cr.show_pango_layout_line(layout.iter.line_readonly)
        end
      end
      cr.show_page
      true
    end
    
    put(@da,0,0)
  end

  def set_labels(lbls)
    @lbls=lbls
  end

  def invalidate
    @valid=false
    @da.queue_draw()
  end

  def touch(new_data)
    new_data.each do |bn,cond,t|
      if(bn==Moo::FREEZE_KEY)        
        @mp.master.freeze_session if(cond)
        return
      end
      
      idx=@lbls.index(bn)
      if(idx)
        @cur_cond[idx]=cond
      else
        lg("Warning: bad button name #{bn}")
      end
    end
    @da.queue_draw()
  end  
end 

class Stimulus_info < Gtk::Box
  W=800
  H=25
  FONTSIZE=12
  
  def initialize(mp,labels)
    @mp,@labels=mp,labels
    super(:horizontal)
    
    set_size_request(W,H)

    w_per_cl=W/labels.length
    @lblw=@labels.map.with_index do |l,i|
#      l=Gtk::Label::new(halign=Gtk::Align::START)
      l=Gtk::Label::new()
      l.set_size_request(w_per_cl,-1)
      l.set_alignment(0.0,0.5)
      l.style_context.add_class("stimulinfo#{i+1}")
      pack_start(l,expand:true,fill:true)
      [l,nil]
    end

    fill_labels([])
  end

  def fill_labels(a)
    @lblw.each_with_index do |aa,i|
      if(a[i]==false)
        aa[1]=nil
      elsif(a[i])
        aa[1]=a[i]
      end

      s=((aa[1]) ? "<span font='Monospace #{FONTSIZE}' foreground='black'>#{@labels[i] ? ' '+@labels[i]+': ' : ''}</span><span font='Monospace #{FONTSIZE}' foreground='red'>#{aa[1] ? aa[1] : ''}</span>" : '').force_encoding('UTF-8')
#      lg("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA lblw #{aa}/#{i} -> [#{s}]")      
      aa[0].set_markup(s)
    end
  end
end

class Values_da < Gtk::Fixed
  attr_reader(:da)
  
  def initialize(vi,prog)    
    @vi,@prog=vi,prog
    @a=@vi.paras[@prog]
    @adiff=@a[2]-@a[1]
    
    super()

    @da=Gtk::DrawingArea::new
    @da.set_size_request(Values_info::W-Values_info::WLABEL,Values_info::H/@vi.paras.length)
    
    @da.signal_connect('draw') do |w,cr|
      cr.set_source_color('black')
      cr.paint
      
      v=(@vi.cur_val[@prog]-@a[1])/@adiff
      v=0.0 if(v<0.0)
      v=1.0 if(v>1.0)
      v*=Values_info::WP
#      printf("El%d -> %f -> %f\n",@prog,@vi.cur_val[@prog],v)
      
      cr.set_source_color('yellow')
      cr.rectangle(Values_info::MARG,Values_info::MARG,v,@vi.hp)
      cr.fill        
    end

    put(@da,0,0)
  end
end
    
class Values_info < Gtk::Frame
  W=400
  WLABEL=100
  H=250
  MARG=4
  WP=W-WLABEL-MARG*2

  attr_reader(:paras,:hp,:cur_val)

  def initialize(label,paras)
    @label,@paras=label,paras
    
    super(@label)
    set_size_request(W,H)

    @cur_val=@paras.map do |a|
      a[1]+(a[2]-a[1])/2.0
    end

    @heach=H/@paras.length
    @hp=@heach-MARG*2

    vbox=Gtk::Box::new(:vertical)
    add(vbox)
    @bs=[]
    @paras.each_with_index do |a,i|
      hbox=Gtk::Box::new(:horizontal)
      vbox.add(hbox)
      l=Gtk::Label::new()
      l.set_size_request(W*0.2,0)
      l.set_markup("<span font='Monospace 12' foreground='black' >#{a[0]}</span>")

      hbox.pack_start(l,:expand=>false,:fill=>false)

      da=Values_da::new(self,i)
      hbox.pack_start(da,:expand=>true,:fill=>true)
      @bs.push(da)
    end
  end

  def touch(nv)
    begin
      nv.each_with_index do |v,i|
        break if(i>=@cur_val.length)
        @cur_val[i]=v      
        @bs[i].da.queue_draw()
      end
    rescue => err
      lg("Warning at touch: #{err}")
    end
  end  
end

class MP_wnd < Gtk::ApplicationWindow
  attr_reader(:sessinfo,:statinfo,:stat2info,:stat3info,:indinfo,:buttinfo,:stimulinfo,:dof_values,:actu_values)
  
  def initialize(mp,app)
    @mp=mp
    super(app)

    signal_connect("key_press_event") do |w,e|
      mp.buttonpress(e.keycode,true)
      false
    end
    signal_connect("key_release_event") do |w,e|
      mp.buttonpress(e.keycode,false)
      false
    end
    
    set_title("Platform commander")

    @mainbox=Gtk::Box.new(:vertical)
    add(@mainbox)

    @sessinfo=Session_info::new(@mp)
    @mainbox.pack_start(@sessinfo,:expand=>false,:fill=>false)
    @statinfo=Status_info::new(@mp)
    @mainbox.pack_start(@statinfo,:expand=>false,:fill=>false)
    @stat2info=Status2_info::new(@mp)
    @mainbox.pack_start(@stat2info,:expand=>false,:fill=>false)
    @indinfo=Indicators_info::new(@mp)
    @mainbox.pack_start(@indinfo,:expand=>false,:fill=>false)
    @stat3info=Status3_info::new(@mp)
    @mainbox.pack_start(@stat3info,:expand=>false,:fill=>false)

    b=Gtk::Box.new(:horizontal)
    @mainbox.pack_start(b,:expand=>false,:fill=>false)

    @dof_values=Values_info::new('DOF (from platform)',
                                 [['Lateral',Moo::LX_MIN,Moo::LX_MAX],['Heave',Moo::LY_MIN,Moo::LY_MAX],['Surge',Moo::LZ_MIN,Moo::LZ_MAX],
                                   ['YAW',Moo::YAW_MIN,Moo::YAW_MAX],['PITCH',Moo::PITCH_MIN,Moo::PITCH_MAX],
                                   ['ROLL',Moo::ROLL_MIN,Moo::ROLL_MAX]])
    b.pack_start(@dof_values,:expand=>false,:fill=>false)
    mnv=Moo::ACTU_MIN_MM+Moo::ACTU_FIXED_MM
    mxv=Moo::ACTU_MAX_MM+Moo::ACTU_FIXED_MM
    @actu_values=Values_info::new('Actuators (from server)',[['Act1',mnv,mxv],['Act2',mnv,mxv],['Act3',mnv,mxv],['Act4',mnv,mxv],['Act5',mnv,mxv],['Act6',mnv,mxv]])    
    b.pack_start(@actu_values,:expand=>false,:fill=>false)
    
    @buttinfo=Buttons_info::new(@mp,Main_process::KEYS.length)
    @mainbox.pack_start(@buttinfo,:expand=>false,:fill=>false)
    
    @stimulinfo=Stimulus_info::new(@mp,['Aud','Still',nil])
    @mainbox.pack_start(@stimulinfo,:expand=>false,:fill=>false)
    
    show_all
  end
end

class Main_process < Gtk::Application  
  INTERVAL=1 #10
  KEYS=[0,1,2,3,4,5,6,7]
#  COMEDI_DEV='K8061'
  COMEDI_DEV='cb_pcimdas'
  COMEDI_FREQ=100
  COMEDI_ACTION_BTNS=[[2,0],[2,1],[2,2],[2,3],[2,4],[2,5],[2,6],[2,7]]
  COMEDI_FREEZE_BTN=[2,23]

  BTN_KC_FREEZE=82
  BTN_KC_MAP=[87,88,89,83,84,85,79,80]
  
  attr_reader(:wnd,:master,:latest_session,:latest_states,:latest_states2,:latest_indicators)
  
  def initialize(server_local_address,server_inport,server_outport,local_address,moog_address,moog_locport,moog_remport,opt_use_kb)
    #    Moo::core_unlimited
    
    @server_local_address,@server_inport,@server_outport,@local_address,@moog_address,@moog_locport,@moog_remport,@opt_use_kb=server_local_address,
    server_inport,server_outport,local_address,moog_address,moog_locport,moog_remport,opt_use_kb
    
    super("ch.unibe.psy.main_process",:flags_none)
#    super("ch.unibe.psy.main_process",:send_environment)

    signal_connect("activate") do |app|
      @cssp=Gtk::CssProvider.new
      css_stuff=File::read(CSSBASE+self.class.to_s+'.css')
      @cssp.load(data:css_stuff)
      
      Gtk::StyleContext.add_provider_for_screen(Gdk::Screen.default,@cssp,Gtk::StyleProvider::PRIORITY_APPLICATION)
      
      @wnd=MP_wnd.new(self,app)
      @cur_btn_lbls=@kb_def_labels.clone
      if(@opt_use_kb || @kbinp)
        @wnd.buttinfo.set_labels(@cur_btn_lbls)
      else
        @wnd.buttinfo.invalidate
      end
      @wnd.present

      @latest_session=nil
      @latest_states=nil
      @latest_states2=nil
      @latest_indicators=nil
    end

    @master=Moo::Master::new(@server_local_address,@server_inport,@server_outport,@local_address,@moog_address,@moog_locport,@moog_remport,self)
    @kb_def_labels=KEYS.map do |v|
      sprintf("Btn%d",v)
    end+[Moo::FREEZE_KEY]

    if(@opt_use_kb)
      @kbinp=false
      @bp_map=Array::new(BTN_KC_MAP.length,false)
      @bp_modified=[]
    else
      # begin
      #   @kbinp=Moo::Keyb_input::new(KEYDEV,KEYS)
      #   @kbinp.feed_labels(@kb_def_labels)
      # rescue
      #   lg("Sorry: keyb input device not present. No buttons.")
      # end
      begin
        comedi_dev=Moo::comedi_device_detector(COMEDI_DEV)
        #      @kbinp=Comedi_dio::new(path:"/dev/comedi#{comedi_dev}",freq:COMEDI_FREQ,digital_io:[[[2,0],[2,1],[2,2],[2,3],[2,4],[2,5],[2,6],[2,7],[2,8],[2,9],[2,10],[2,11],[2,12],[2,13],[2,14],[2,15],[2,16],[2,17],[2,18],[2,19],[2,20],[2,21],[2,22],[2,23]],[]])
        @kbinp=Comedi_dio::new(path:"/dev/comedi#{comedi_dev}",freq:COMEDI_FREQ,digital_io:[COMEDI_ACTION_BTNS+[COMEDI_FREEZE_BTN],[]])
      @kbinp.feed_labels(@kb_def_labels)
      rescue => err
        lg("Sorry: comedi input device not present. No buttons (#{err.to_s}).")
        @kbinp=false
      end
    end
    
    iru=GLib::Timeout::add(INTERVAL) do
      interval_run()
    end
    
    def interval_run
      if(@in_ir)
        lg("Oops busy")
        return true
      end

      @in_ir=true

      @master.loop

      if(@opt_use_kb)
        if(@bp_modified.length>0)
          @wnd.buttinfo.touch(@bp_modified)
          @master.session.process_kbinp(@bp_modified) if(@master.session)
          @bp_modified=[]
        end          
      elsif(@kbinp)
        kp=@kbinp.poll
        if(kp)
          @wnd.buttinfo.touch(kp)
          @master.session.process_kbinp(kp) if(@master.session)
        end
      end
      
      if(@master.session!=@latest_session)
        @latest_session=@master.session

        if(@opt_use_kb || @kbinp)
          @cur_btn_lbls=@kb_def_labels

          if(@opt_use_kb)
            @bp_map=Array::new(BTN_KC_MAP.length,false)
            @bp_modified=[]
          end

          if(@latest_session)
            @cur_btn_lbls=@latest_session.btn_labels
            @cur_btn_lbls.length.upto(KEYS.length-1) do
              @cur_btn_lbls.push(nil)
            end
          end

          @kbinp.feed_labels(@cur_btn_lbls) if(@kbinp)
          @wnd.buttinfo.set_labels(@cur_btn_lbls)          
        end
        @wnd.sessinfo.touch
      end

      if((@master.session && @master.session.latest_states!=@latest_states) ||
         (!@master.session && @latest_states))
        @latest_states=@master.session ? @master.session.latest_states : nil
        @wnd.statinfo.touch
      end

      new_sts2=(@master.session && @master.session.latest_sts) ? @master.session.latest_sts[12+Moo::N_ACTU][1,2] : false
      if(@latest_states2!=new_sts2)
        @latest_states2=new_sts2
        @wnd.stat2info.touch if(@wnd)
      end

      if((@master.session && @master.session.latest_sts && @master.session.latest_sts[12+Moo::N_ACTU][0]!=@latest_indicators) ||
         (!@master.session && @latest_indicators))
        @latest_indicators=(@master.session && @master.session.latest_sts) ? @master.session.latest_sts[12+Moo::N_ACTU][0] : nil
        @wnd.indinfo.touch
      end

      if(@master.session)
        lv=@master.session.latest_values
        if(lv)
          srv_lv=Moo::moog_idea_to_server_idea(lv[0])
          #         printf("\r%10.3f %10.3f %10.3f | %10.3f %10.3f %10.3f",*srv_lv)
          #         @wnd.dof_values.touch([(lv[0][2]-Moo::ACTU_MOOG_IDLE+Moo::ACTU_IDLE_H)*1000.0,
          @wnd.dof_values.touch(srv_lv)
          @wnd.actu_values.touch(lv[1])
        end
      end

      @in_ir=false
      true        
    end
  end

  def buttonpress(kc,flg)
    return unless(@bp_map)

    if(kc==BTN_KC_FREEZE)
      @bp_modified.push([Moo::FREEZE_KEY,flg,Cg::time_now[1]])
    else      
      v=BTN_KC_MAP.index(kc)
      if(v && @bp_map[v]!=flg)
        @bp_map[v]=flg
        @bp_modified.push([@cur_btn_lbls[v],flg,Cg::time_now[1]]) if(@cur_btn_lbls[v])
      end
    end
  end

  def update_state_msg(msg,msgsize)
    @wnd.stat3info.touch(msg,msgsize)
  end
end

if(__FILE__==$0)
  opt_use_kb=false
  OptionParser.new do |opts|
    opts.banner="Usage: #{$0} [-K]"
    
    opts.on('-K','--use-keyboard','Use keyboard\'s numeric pad for buttons') do 
      opt_use_kb=true
    end
  end.parse!
  
  Main_process::new(SERVER_LOCAL_ADDRESS,SERVER_INPORT,SERVER_OUTPORT,LOCAL_ADDRESS,MOOG_ADDRESS,MOOG_LOCPORT,MOOG_REMPORT,opt_use_kb).run
end

