# msg_extractor.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Utility to extract messages from session directories

=end

# msg_extractor.rb

require_relative('moo')

raise "Usage: #{$0} path" unless(ARGV.length==1)

fn=ARGV[0]+'.sess'
raise "#{fn} not found!" unless(File::file?(fn))

start_time=nil

File::open(fn,'rb') do |unit|
  ts=nil
  loop do
    ts=unit.read(16)
    break unless(ts)          
    type=unit.read(1)

    if(type=='B')
      tl=unit.read(1).bytes[0]
      unit.read(tl+1)
    elsif(type=='S')
      unit.read(2)
    elsif(type=='s')
      unit.read(6)
    elsif(type=='I')
      start_time=Cg::msec_from_timespec(ts) unless(start_time)
      td=Cg::msec_from_timespec(ts)-start_time
      len=unit.read(2).unpack('S')[0]
      stuff=unit.read(len)
      sec=td/1000
      STDERR.printf("I: %2.2d:%2.2d:%2.2d.%3.3d %s\n",sec/3600,(sec/60)%60,sec%60,td&1000,stuff)
    elsif(type=='O')
      start_time=Cg::msec_from_timespec(ts) unless(start_time)
      td=Cg::msec_from_timespec(ts)-start_time
      len=unit.read(2).unpack('S')[0]
      stuff=unit.read(len)
      sec=td/1000
      STDERR.printf("O: %2.2d:%2.2d:%2.2d.%3.3d %s\n",sec/3600,(sec/60)%60,sec%60,td&1000,stuff)
    else
      lg "Sess type #{type} (#{type}) unknown (at #{unit.seek(0,:CUR)})" 
    end
  end
end
