# post_production.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Code to run post-production and generate csv files

(This code is imported within module Moo)

=end

class Pp_data_generator
  DOF_LBLS=['ROLL','PITCH','heave','surge','YAW','lateral']
  
  attr_reader(:fn,:exist,:stuff,:last_time)
  
  def initialize(pp,fn)
    @pp,@fn=pp,fn
    @exist=case(@fn)
           when(Array)
             @fn.length==0 ? false : begin
                                       val=true
                                       @fn.each do |fn|
                                         val=false unless(File::file?(fn))
                                       end
                                       val
                                     end             
           when(String)
             File::file?(@fn)
           else
             false
           end    
    @n_fields=0
  end
  
  def n_cols
    0
  end
  def headers
    []
  end
  def values(time)
    []
  end
end

class Pp_dg_sess < Pp_data_generator
  NAME='.sess'
  STATE_HDRS=['Master state','Server state','Moog state']

  attr_reader(:stuff_MI_ipol,:stuff_MO_ipol)

  def initialize(pp)
    super(pp,pp.base+NAME)
  end

  def n_cols
    STATE_HDRS.length+@pp.btns.length
  end
  def headers
    STATE_HDRS+(@pp.btns ? @pp.btns.map do |bname|
                             "Btn #{bname}"
                           end : [])
  end
  
  def collect_data
    @stuff_B=[]
    @stuff_s=[]
    @stuff_S=[]
    @stuff_MI=[]
    @stuff_MO=[]
    
    File::open(@fn,'rb') do |unit|
      ts=nil
      loop do
        ts=unit.read(Cg::SIZEOF_STRUCT_TIMESPEC)
        break unless(ts)          
        type=unit.read(1)
        if(type=='B')
          tl=unit.read(1).bytes[0]
          t=unit.read(tl)
          idx=@pp.btns.find_index(t)
          cond=unit.read(1).unpack('c')[0]==0 ? false : true
          if(idx)
            @stuff_B.push([ts,[idx,cond]])
          else
            lg("Found Btn [#{t}] UNKNOWN!")
          end
        elsif(type=='S')
          @stuff_S.push([ts,unit.read(2).unpack('S')])
        elsif(type=='s')
          @stuff_s.push([ts,unit.read(6).unpack('SSS')[1,2]])
        elsif(type=='I')
          len=unit.read(2).unpack('S')[0]
          @stuff_MI.push([ts,unit.read(len)])
        elsif(type=='O')
          len=unit.read(2).unpack('S')[0]
          @stuff_MO.push([ts,unit.read(len)])
        else
          lg("Sess type #{type} (#{type.ord}) unknown (at #{unit.sysseek(0,IO::SEEK_CUR)})")          
        end
      end
    end
  end

  def first_times
    [@stuff_B.length>0 ? @stuff_B[0][0] : nil,
     @stuff_S.length>0 ? @stuff_S[0][0] : nil,
     @stuff_s.length>0 ? @stuff_s[0][0] : nil,
     @stuff_MI.length>0 ? @stuff_MI[0][0] : nil,
     @stuff_MO.length>0 ? @stuff_MO[0][0] : nil].compact
  end
  
  def interpolate(start_time)
    last_time=0
    @stuff_B_ipol={}
    
    if(@stuff_B.length>0)
      a=@stuff_B.map do |t,d|
        [Cg::msec_from_timespec(t)-start_time,d]
      end
      @pp.cadenced_btns(a,@pp.millisec_interval,@pp.btns.length).each do |t,d|
        @stuff_B_ipol[t]=d.map do |val|
          val ? 1 : nil
        end
      end
      
      last_time=@stuff_B_ipol.keys[-1] if(last_time<@stuff_B_ipol.keys[-1])
    end
    if(@stuff_S.length>0 || @stuff_s.length>0)
      @stuff_S_ipol=[]
      @stuff_s_ipol=[]

      @stuff_S.each do |t,d|
        @stuff_S_ipol.push([Cg::msec_from_timespec(t)-start_time,d])
      end
      @stuff_s.each do |t,d|
        @stuff_s_ipol.push([Cg::msec_from_timespec(t)-start_time,d])
      end

      @stuff_status_ipol=@pp.cadenced_statuses(@stuff_S_ipol,@stuff_s_ipol,@pp.millisec_interval)

      last_time=@stuff_S_ipol[-1][0] if(@stuff_S_ipol.length>0 && last_time<@stuff_S_ipol[-1][0])
      last_time=@stuff_s_ipol[-1][0] if(@stuff_s_ipol.length>0 && last_time<@stuff_s_ipol[-1][0])
    end
    if(@stuff_MI.length>0)
      @stuff_MI_ipol={}

      @stuff_MI.each do |t,d|
        @stuff_MI_ipol[Cg::msec_from_timespec(t)-start_time]=[] unless(@stuff_MI_ipol[Cg::msec_from_timespec(t)-start_time])
        @stuff_MI_ipol[Cg::msec_from_timespec(t)-start_time].push(d)
      end
      last_time=@stuff_MI_ipol.keys[-1] if(last_time<@stuff_MI_ipol.keys[-1])
    end
    if(@stuff_MO.length>0)
      @stuff_MO_ipol={}

      @stuff_MO.each do |t,d|
        @stuff_MO_ipol[Cg::msec_from_timespec(t)-start_time]=[] unless(@stuff_MO_ipol[Cg::msec_from_timespec(t)-start_time])
        @stuff_MO_ipol[Cg::msec_from_timespec(t)-start_time].push(d)
      end
      last_time=@stuff_MO_ipol.keys[-1] if(last_time<@stuff_MO_ipol.keys[-1])
    end

    last_time
  end

  def values(time)
    a=[]
    a+=(@stuff_status_ipol && @stuff_status_ipol[time]) ? @stuff_status_ipol[time] : [nil,nil,nil]
    a+=(@stuff_B_ipol && @stuff_B_ipol[time]) ? @stuff_B_ipol[time] : (@pp.btns ? Array::new(@pp.btns.length) : [])
    a
  end
end

class Pp_dg_raw_in < Pp_data_generator
  NAME='.raw_in'
  HDRS=DOF_LBLS.map do |s|
    "#{s} (moog)"
  end+['Lin vel','Ang vel','Lin acc','Ang acc']
  
  def initialize(pp)
    super(pp,pp.base+NAME)
  end

  def n_cols
    HDRS.length
  end
  def headers
    HDRS
  end
  
  def collect_data
    @stuff=[]
    File::open(@fn,'rb') do |unit|
      ts=nil
      loop do
        ts=unit.read(Cg::SIZEOF_STRUCT_TIMESPEC)
        break unless(ts)
        @stuff.push([ts,unit.read(40)])
      end
    end
  end

  def first_times
    @stuff.length>0 ? [@stuff[0][0]] : []
  end

  def interpolate(start_time)
    @stuff_ipol={}
    return nil if(@stuff.length<=0)
    
    old_stuff=nil
    old_vel=nil
    vel=[nil,nil]
    acc=[nil,nil]

    sm=@stuff.map do |t,d|
      d ? [Cg::msec_from_timespec(t)-start_time,d[12,24]] : nil
    end.compact
    
    ret=Maths::interpolate_series(sm,@pp.millisec_interval)
    return nil unless(ret)
    
    ret.each do |t,d|
      stuff=d.unpack('f*')[0,6]
      if(old_stuff)
        vel=Maths::velocity(old_stuff,stuff,@pp.millisec_interval/1000.0)
        acc=[(vel[0]-old_vel[0])/(@pp.millisec_interval/1000.0),(vel[1]-old_vel[1])/(@pp.millisec_interval/1000.0)] if(old_vel)
        old_vel=vel
      end
      old_stuff=stuff

      @stuff_ipol[t]=stuff+vel+acc
    end

    @stuff_ipol.keys[-1]
  end

  def values(time)
    return Array::new(n_cols) unless(@stuff_ipol[time])
    @stuff_ipol[time]
  end
end

class Pp_dg_raw_out < Pp_data_generator
  NAME='.raw_out'
  HDRS=DOF_LBLS.map do |s|
    "#{s} (server)"
  end+Moo::N_ACTU.times.to_a.map do |v|
    "Act.#{v+1}"
  end

  def initialize(pp)
    super(pp,pp.base+NAME)
  end
  
  def n_cols
    HDRS.length
  end
  def headers
    HDRS
  end

  def collect_data
    @stuff=[]
    File::open(@fn,'rb') do |unit|
      ts=nil
      loop do
        ts=unit.read(Cg::SIZEOF_STRUCT_TIMESPEC)
        break unless(ts)          
        
        r=unit.read((6+Moo::N_ACTU)*4)
        @stuff.push([ts,r])
      end
    end
  end

  def first_times
    @stuff.length>0 ? [@stuff[0][0]] : []
  end

  def interpolate(start_time)
    @stuff_ipol={}
    return nil if(@stuff.length<=0)
    
    ret=Maths::interpolate_series(@stuff.map do |t,d|
                                    if(d)
                                      aa=d.unpack('f*')
                                      a2=Moo::server_idea_to_moog_idea(aa[0,6])+aa[6,Moo::N_ACTU].map do |v|
                                        (v/1000.0)-Moo::ACTU_FIXED
                                      end
                                      if(a2.length==12)
                                        [Cg::msec_from_timespec(t)-start_time,a2.pack('f*')]
                                      else
                                        lg("STRANGE! #{a2}")
                                        nil
                                      end
                                    else
                                      nil
                                    end
                                  end.compact,@pp.millisec_interval)
    return nil unless(ret)
    
    ret.each do |t,d|
      @stuff_ipol[t]=d.unpack('f*')
    end

    @stuff_ipol.keys[-1]
  end

  def values(time)
    return Array::new(n_cols) unless(@stuff_ipol[time])
    @stuff_ipol[time]
  end
end

class Pp_dg_surv < Pp_data_generator
  NAME='.surv'
  LBLS=['lin#1','lin#2','lin#3','quat#1','quat#2','quat#3','quat#4']

  def initialize(pp)
    super(pp,pp.base+NAME)
  end
  
  def n_cols
    LBLS.length*@dev.length
  end
  def headers
    @dev.map do |n,p|
      LBLS.map do |s|
        "#{n} #{s}"
      end
    end.flatten
  end
  
  def collect_data
    @stuff=[]
    @dev=[]
    
    File::open(@fn,'rb') do |unit|
      ts=nil
      loop do
        ts=unit.read(Cg::SIZEOF_STRUCT_TIMESPEC)
        break unless(ts)
        t=unit.read(1)
        srl=nil
        if(t=='S')
          sl=unit.read(2).unpack('S')[0]
          srl=unit.read(sl)
          t=unit.read(1)
        end
        data=(t+unit.read(57)).unpack('SD*')
        @dev.push([srl,data[0]]) if(srl)
        @stuff[data[0]]=[] unless(@stuff[data[0]])
        @stuff[data[0]].push([ts,data[1..-1]])
      end
    end

    @dev.sort! do |a,b|
      a[0]<=>b[0]
    end
  end

  def first_times
    @stuff.map do |a|
      a[0]
    end
  end

  def interpolate(start_time)
    last_time=0
    @stuff_ipol=@stuff.map do |s|
      a=s.map do |t,d|
        [Cg::msec_from_timespec(t)-start_time,d[0,7].pack('f*')]
      end
      h={}
      Maths::interpolate_series(a,@pp.millisec_interval).each do |t,v|
        h[t]=v.unpack('f*')
      end
      last_time=h.keys[-1] if(last_time<h.keys[-1])
      h
    end
    
    last_time
  end

  def values(time)
    a=[]
    @dev.each do |s,p|
      a+=@stuff_ipol[p][time] ? @stuff_ipol[p][time] : Array::new(LBLS.length)
    end
    a
  end
end

class Pp_dg_hmd < Pp_data_generator
  NAME='.hmd_%d'
  MAX_N=10
  HDRS=['Yaw','Pitch','Roll']+QLABELS
  
  def initialize(pp)
    a=[]
    MAX_N.times do |v|
      fn=pp.base+sprintf(NAME,v)
      a.push(fn) if(File::file?(fn))
    end
    super(pp,a)
  end
  
  def n_cols
    @fn.length*HDRS.length
  end
  
  def headers
    @fn.length.times.to_a.map do |n|
      HDRS.map do |s|
        sprintf("hmd%d-%s",n,s)
      end
    end.flatten
  end

  def collect_data
    @stuff=[]
    @fn.each do |fn|
      stuff=[]
      File::open(fn,'rb') do |unit|
        ts=nil
        loop do
          ts=unit.read(Cg::SIZEOF_STRUCT_TIMESPEC)
          break unless(ts)
          t=unit.read(1)
          if(t=='Q')
            q=Maths::quat_normalize(unit.read(16))
            ypr=Maths::quat_to_tait_bryan(q).unpack('fff')

            #
            # It seems OpenHDR returns yaw and roll in inverted direction
            #

            ypr[0]=-ypr[0]
            ypr[2]=-ypr[2]
            
            stuff.push([ts,ypr.pack('fff')+q])
            #          else
            #            stuff.push([ts,nil])
          end
        end
      end
      @stuff.push(stuff)
    end
  end
  
  def first_times
    @stuff.map do |a|
      a[0][0]
    end
  end

  def interpolate(start_time)
    last_time=0
    @stuff_ipol=@stuff.map do |s|
      a=s.map do |t,d|
        [Cg::msec_from_timespec(t)-start_time,d]
      end.delete_if do |a|
        a[0]<=0
      end
      
      h={}
      Maths::interpolate_series(a,@pp.millisec_interval).each do |t,v|
        a=v.unpack('f*')
        h[t]=a[0,3].map do |v|
          v*Moo::RAD2DEG
        end+a[3,4]
      end
      last_time=h.keys[-1] if(last_time<h.keys[-1])
      h
    end
    
    last_time
  end

  def values(time)
    a=[]
    @stuff_ipol.each do |h|      
      a+=h[time] ? h[time] : Array::new(HDRS.length)
    end
    a
  end
end

class Pp_dg_hwuser < Pp_data_generator
  NAME='.hwuser'
  XSENSE_HDRS=['Acc','Gyr','Mag'].map do |t|
    ['X','Y','Z'].map do |ax|
      "xsense-#{t}/#{ax}"
    end
  end.flatten
   
  def initialize(pp)
    @gp_btn_idx={}
    Hw::D_gamepad::BUTTONS.values.each do |b|
      @gp_btn_idx[b.code]=b
    end
    super(pp,pp.base+NAME)
  end

  def collect_data
    @types,@recs=Hw::Hw_user::dotfile_parser(@pp.base)
  end

  def first_times
    (@recs && @recs.length>0) ? [@recs[0][1]] : []
  end

  def interpolate(start_time)
    last_time=0
    
    @gpd=nil
    if(@types.include?(Hw::DEV_GAMEPAD))
      acc=[]
      @recs.select do |a|
        a[0]==Hw::DEV_GAMEPAD
      end.each do |a|
        next unless(a[4]==1) # currently only manage buttons

        idx=@gp_btn_idx.keys.index(a[5])
        unless(idx)
          lg("Ouch! COuld not find gpad btn #{a[5]}")
          next
        end
                                                      
        acc.push([Cg::msec_from_timespec(a[1])-start_time,[idx,a[6]==1 ? true : false]])
      end

      @gpd={}
      @pp.cadenced_btns(acc,@pp.millisec_interval,@gp_btn_idx.length).each do |t,d|
        @gpd[t]=d.map do |val|
          val ? 1 : nil
        end
      end

      last_time=@gpd.keys[-1]
    end

    @spd=nil
    if(@types.include?(Hw::DEV_SPNAV))
      acc=[]
      @recs.select do |a|
        a[0]==Hw::DEV_SPNAV
      end.each do |a|
        next unless(a[2]==1) # currently only manage buttons
        acc.push([Cg::msec_from_timespec(a[1])-start_time,[a[3],a[4]==1 ? true : false]])
      end

      @spd={}
      @pp.cadenced_btns(acc,@pp.millisec_interval,2).each do |t,d|
        @spd[t]=d.map do |val|
          val ? 1 : nil
        end
      end
      last_time=@spd.keys[-1] if(last_time && @spd.keys[-1]>last_time)
    end

    @xpd=nil
    
    if(@types.include?(Hw::DEV_XSENSE))
      acc=[]
      @recs.select do |a|
        a[0]==Hw::DEV_XSENSE
      end.each do |a|
        next unless(a[2]==50)
        acc.push([Cg::msec_from_timespec(a[1])-start_time,a[3][0,36].unpack('g*').pack('f*')])
      end
      @xpd={}
      Maths::interpolate_series(acc,@pp.millisec_interval).each do |t,d|
        @xpd[t]=d.unpack('f*')
      end
    end
    
    last_time
  end
  
  def headers
    h=[]
    h+=@gp_btn_idx.values.map do |b|
      "Gpad btn #{b.tag}"
    end if(@gpd)
    h+=["Spnav btn left","Spnav btn right"] if(@spd)
    h+=XSENSE_HDRS if(@xpd)
    h
  end

  def values(time)
    a=[]

    a+=@gpd[time] ? @gpd[time] : Array::new(@gp_btn_idx.length,nil) if(@gpd)
    a+=@spd[time] ? @spd[time] : [nil,nil] if(@spd)
    a+=@xpd[time] ? @xpd[time] : Array::new(XSENSE_HDRS.length,nil) if(@xpd)
    
    a
  end
end

class Pp_dg_video_stim_times < Pp_data_generator
  NAME='.stim_video'

  def initialize(pp)
    super(pp,pp.base+NAME)
  end

  def collect_data(reftime)
    data=[]
    File::open(@fn,'rb') do |f|
      loop do
        t=f.read(16)
        break unless(t)
        len=f.read(4).unpack('L')[0]
        s=f.read(len)
        t2=s[0,16]
        stuff=Marshal::restore(s[16..-1])

        data.push([Cg::msec_from_timespec(t)-reftime,Cg::msec_from_timespec(t2)-reftime,*stuff])
      end
    end
    data
  end
      
end 

class Post_producer
  attr_reader(:base,:millisec_interval,:btns)

#  FMTF='%.7f'
  FMTF='%f'
  NORMAL_USER=ENV['NORMAL_USER'] ? ENV['NORMAL_USER'] : 'moog'
  SPECIAL_PROJ_CLASSES=[Sess_effe,Sess_macaco]
  SUBST_CLS=/[\-\(\)\#\.\*\/ ]/
  
  def initialize(base,tstamp,millisec_interval,force_flg=false)
    @base,@tstamp,@millisec_interval,@force_flg=base,tstamp,millisec_interval,force_flg
    
    @resfile=sprintf("%s/%d_%s",@base,@millisec_interval,Session::POSTPROD_FILE)
  end

  def generate
    return true if(File::file?(@resfile) && !@force_flg)    

    dir=@base+Session::POSTPROD_DIR
    begin
      FileUtils::mkdir_p(dir)
    rescue => err
      return "Error! "+err.to_s
    end

    h=read_info
    return h unless(h.is_a?(Hash))

    @btns=(!h[:btns] || h[:btns].length<=0) ? nil : h[:btns].split(',')+[Moo::FREEZE_KEY]
    sttime=h[:zerotime].unpack('m')[0]

    prod_h={}
    [Pp_dg_sess,Pp_dg_raw_in,Pp_dg_raw_out,Pp_dg_surv,Pp_dg_hmd,Pp_dg_hwuser].each do |cl|
      inst=cl.send(:new,self)
      prod_h[cl]=inst if(inst.exist)
    end
    producers=prod_h.values.flatten

    times=[]
    producers.each do |p|
      p.collect_data
      times+=p.first_times
    end
    
    first_time=Cg::msec_from_timespec(times.sort do |d1,d2|
                                        Cg::time_diff(d2,d1)
                                      end[0])
    highest_times=[]
    producers.each do |p|
      highest_times.push(p.interpolate(first_time))      
    end
    highest_times.compact!
    
    highest_time=highest_times.length>0 ? highest_times.sort[-1] : 0

    headers=producers.map do |p|
      p.headers
    end.flatten.map do |s|
      s.downcase.gsub(SUBST_CLS,'_')
    end

    fn=dir+@tstamp+'.situation.csv'
    File::open(fn,'w') do |f|
      f.puts((['"time"']+headers.map do |s|
                "\"#{s}\""
              end).join(','))
      
      0.step(highest_time,@millisec_interval) do |td|
        f.puts(([td]+producers.map do |p|
                  p.values(td)
                end.flatten).map do |v|
                 case(v)
                 when(Float)
                 #                   sprintf(FMTF,v)
                   v.to_s
                 when(Integer)
                   v.to_s
                 when(String)
                   "\"#{v}\""
                 else
                   ''
                 end
               end.join(','))
      end        
    end

    #
    # If the session file was found (i.e. always, but let's check...) produce the messages file
    #

    if(prod_h[Pp_dg_sess])
      stuff_MI=prod_h[Pp_dg_sess].stuff_MI_ipol
      stuff_MO=prod_h[Pp_dg_sess].stuff_MO_ipol

      mi_f=(stuff_MI && stuff_MI.length>0)
      mo_f=(stuff_MO && stuff_MO.length>0)

      if(mi_f || mo_f)
        tst=((mi_f ? stuff_MI.keys : [])+(mo_f ? stuff_MO.keys : [])).sort.uniq
        
        fn=dir+@tstamp+'.messages.csv'
        File::open(fn,'w') do |f|
          tst.each do |t|          
            stuff_MI[t].each do |s|
              f.puts("#{t},\"I\",#{s}")
            end if(mi_f && stuff_MI[t])
            
            if(mo_f && stuff_MO[t])
              stuff_MO[t].each do |s|
                a=s.split(',')
                a[1,0]="\"O\""
                f.puts(a.join(','))
              end
            end
          end
        end        
      end
    end

    #
    # Deal with the file containing times of image displays
    #

    inst=Pp_dg_video_stim_times::new(self)
    if(inst.exist)
      data=inst.collect_data(first_time)
      fn=dir+@tstamp+'.still_stim_times.csv'
      File::open(fn,'w') do |f|
        f.write("\"Req. time\",\"Displ. time\",\"Name\",\"Screen\"\n")
        data.each do |t1,t2,scn,name|
          f.printf("%d,%d,\"%s\",%d\n",t2,t1,name,scn)
        end
      end      
    end
    
    mysystem("cd #{dir} && zip #{@resfile} * ; chown #{NORMAL_USER} #{@base} ; chown #{NORMAL_USER} #{@resfile}",false)
    FileUtils::rm_rf(dir)

    true
  end
  
  def mail(addresses)
    h=read_info
    return h if(h.is_a?(String))
    
    subj="Moog run report"
    txt=<<-EOF
Results for Moog run type #{h[:type]}
ran from address #{h[:address]}
start time #{h[:time]}
Millisecond interval #{@millisec_interval}

Enjoy.
EOF

    begin
      mailtext(addresses,subj,txt,[['applica/zip',h[:time]+'.post_prod.zip',@resfile]])
    rescue => err
      return err.to_s
    end

    true
  end

  def read_info
    fn=@base+Session::INFO_FILE
    return "Info file #{fn} not found!" unless(File::file?(fn))

    h={}
    File::foreach(fn) do |l|
      next if(l[0,1]=='#')
      a=l.chomp.split('#')
      next unless(a.length==2)
      return "Info file #{fn}: tag #{a[0]} doubly specified!" if(h[a[0]])
      h[a[0].to_sym]=a[1]
    end    

    h
  end

  def cadenced_btns(d,ival,n_btns)
    dpos=0
    cursit=Array::new(n_btns,false)
    retarr={}
    time=0
    
    loop do
      while(dpos<d.length && d[dpos][0]<time)
        cursit[d[dpos][1][0]]=d[dpos][1][1]
        dpos+=1
      end
      break if(dpos>=d.length)
      retarr[time]=cursit.clone
      time+=ival
    end
    
    retarr
  end
  def cadenced_statuses(d1,d2,ival)
    d1pos=0
    d2pos=0
    retarr={}
    time=0

    loop do
      while(d1pos<d1.length && d1[d1pos][0]<time)
        d1pos+=1
      end if(d1)
      while(d2pos<d2.length && d2[d2pos][0]<time)
        d2pos+=1
      end if(d2)

      break if((!d1 || d1pos>=d1.length) && (!d2 || d2pos>=d2.length))

      retarr[time]=((d1 && d1pos>0) ? d1[d1pos-1][1] : [nil])+((d2 && d2pos>0) ? d2[d2pos-1][1] : [nil,nil])
      time+=ival
    end
    retarr
  end
end
