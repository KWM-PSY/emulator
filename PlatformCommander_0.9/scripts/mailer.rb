# mailer.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Some code to send out e-mails

=end

# mailer.rb

require 'net/smtp'
require 'xmlrpc/base64'

MAILSERVER='mailhub04.unibe.ch'
SENDER='donotreply@psy.unibe.ch'
BOUND='SEPAsepaSEPA123'
WDAY=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
MONTH=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

def time_to_emailformat(tm)
  gmt=Time.at(tm.to_i)
  gmt.gmtime
  offset=tm.to_i-Time.local(*gmt.to_a[0,6].reverse).to_i
  
  sprintf('%s, %s %s %d %02d:%02d:%02d %+.2d%.2d',
          WDAY[tm.wday], tm.mday, MONTH[tm.month-1],
          tm.year, tm.hour, tm.min, tm.sec,
          *(offset / 60).divmod(60))
end

def mailtext(receivers,subj,txt,parts)
  receivers.each do |r|
    msg=<<-EOF
Date: #{time_to_emailformat(Time::now)}+
From: <#{SENDER}>
To: <#{r}>
Subject: #{subj}
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="#{BOUND}"
Content-Disposition: inline
User-Agent: MOOGOMAT


--#{BOUND}
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline

#{txt}
EOF
    
    parts.each do |p|
      m=File::open(p[2],'r') do |f|
        f.read()
      end
      msg+=<<-EOF

--#{BOUND}
Content-Type: #{p[0]}
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename=#{p[1]}

#{XMLRPC::Base64::encode(m)}
EOF
    end if(parts)
    
    smtp=Net::SMTP.new(MAILSERVER,25)
    smtp.start do
      smtp.send_mail(msg,SENDER,r)
    end
  end
end

