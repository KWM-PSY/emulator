# activator.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Some Ruby code to supplement HW C code

=end

require('hw')

module Hw
  class Device
    attr_reader(:name,:cdev)
    
    @@devices={}

    def initialize(name,cdev)
      @name,@cdev=name,cdev
    end
    
    def set_mdev_id(id)
      raise "Doubly specified monitored id #{id}" if(@@devices[id])
      @mdev_id=id
      @@devices[@mdev_id]=self
    end

    def stop
      @cdev.stop_thread
    end

    def self::reset_devices
      @@devices={}
    end
    
    def self::dev_by_id(id)
      @@devices[id]
    end
  end

  class D_btn
    attr_reader(:code,:tag,:name)
    attr_accessor(:cond)
    
    def initialize(code,tag,name)
      @code,@tag,@name=code,tag,name
      @cond=false
    end
  end
  
  class D_gamepad < Device
    attr_reader(:active_btns)

    BUTTONS={
      fl1:D_btn::new(Hw::Gamepad::B_FL1,:fl1,'Front-left 1'),
      fl2:D_btn::new(Hw::Gamepad::B_FL2,:fl2,'Front-left 2'),
      fr1:D_btn::new(Hw::Gamepad::B_FR1,:fr1,'Front-right 1'),
      fr2:D_btn::new(Hw::Gamepad::B_FR2,:fr2,'Front-right 2'),
      select:D_btn::new(Hw::Gamepad::B_SELECT,:select,'Select'),
      start:D_btn::new(Hw::Gamepad::B_START,:start,'Start'),
      triangle:D_btn::new(Hw::Gamepad::B_TRIANGLE,:triangle,'Triangle'),
      square:D_btn::new(Hw::Gamepad::B_SQUARE,:square,'Square'),
      circle:D_btn::new(Hw::Gamepad::B_CIRCLE,:circle,'Circle'),
      cross:D_btn::new(Hw::Gamepad::B_CROSS,:cross,'Cross'),
    }
    
    def initialize(name,active_btns)
      raise "Gamepad: no buttons!" unless(active_btns)

      gp=nil
      Moo::GPAD_NAMES.each do |n|
        begin
          gp=Gamepad::new(n)
          break
        rescue
        end
      end

      raise "No available gamepad found." unless(gp)

      super(name,gp)
      
      @active_btns={}
      active_btns.each do |s|
        b=BUTTONS[s.to_sym]
        raise "Gamepad button #{s} does not exist!" unless(b)
        raise "Gamepad button #{s} doubly specified!" if(@active_btns[b.code])
        @active_btns[b.code]=b
      end
    end

    def feed_btn_act(bid,cond)
      brec=@active_btns[bid]
      return if(!brec || brec.cond==cond)
      brec.cond=cond
      yield(brec)
    end
  end
  
  class D_spnav < Device
    attr_reader(:active_btns)

    BUTTONS={
      left:D_btn::new(Hw::Spnav::B_LEFT,:left,'Left'),
      right:D_btn::new(Hw::Spnav::B_RIGHT,:right,'Right'),
    }
    
    def initialize(name,active_btns)
      hwe=Spnav::new('SpaceNavigator-event')
      unless(hwe)
        hwe=Spnav::new('SpaceNavigator_for_Notebooks-event')
        raise "Sorry! Space navigator not found!" unless(hwe)
      end
      super(name,hwe)
      
      @active_btns={}
      active_btns.each do |s|
        b=BUTTONS[s.to_sym]
        raise "Spnav button #{s} does not exist!" unless(b)
        raise "Spnav button #{s} doubly specified!" if(@active_btns[b.code])
        @active_btns[b.code]=b
      end
    end
    
    def feed_btn_act(bid,cond)
      brec=@active_btns[bid]
      return if(!brec || brec.cond==cond)
      brec.cond=cond
      yield(brec)
    end
  end

  class D_xsense < Device
    def initialize(name,*rest)
      super(name,Xsense::new('Xsens_USB'))
    end    
  end
  
  class Hw_user
    attr_reader(:d)
    
    DEVICES={gamepad:[Hw::DEV_GAMEPAD,D_gamepad],
             spnav:[Hw::DEV_SPNAV,D_spnav],
             xsense:[Hw::DEV_XSENSE,D_xsense]}

    def initialize(dev_array)
      Device::reset_devices()
      
      @d={}
      dev_array.each do |a|
        name,dev,data=a
        sym=dev.to_sym
        type,cls=DEVICES[sym]
        raise "Sorry: hw device #{dev} unknown!" unless(type)
        raise "Sorry: hw device #{dev} doubly specified!" if(@d[dev])
        @d[dev]=cls::new(name,data)
        @d[dev].set_mdev_id(add_monitored_device(type,@d[dev].cdev))
      end
    end

    def stop
      @d.values.each do |dev|
        dev.stop
      end
      stop_thread      
      Device::reset_devices()
    end

    def self::dotfile_parser(dir)
      fn=dir+'/'+Hw::LOG_NAME
      raise "#{fn} not found!" unless(File::file?(fn))
      recs=[]
      types=[]
      File::open(fn,'rb') do |f|
        loop do
          b=f.read(20)
          break unless(b)
          a=b.unpack('QQL')
          unless(types.include?(a[2]))
            types.push(a[2])
            types.sort!
          end
                 
          case a[2]
          when Hw::DEV_GAMEPAD
            b=f.read(24)
            break unless(b)
            t1,t2,type,code,value=b.unpack('QQSSl')
            recs.push([a[2],a[0,2].pack('QQ'),t1,t2,type,code,value])            
          when Hw::DEV_SPNAV
            b=f.read(12)
            break unless(b)
            type,prog,val=b.unpack('LLl')
            recs.push([a[2],a[0,2].pack('QQ'),type,prog,val])
          when Hw::DEV_XSENSE            
            b=f.read(8)
            break unless(b)
            msgtype,msglen=b.unpack('LL')
            msg=f.read(msglen)
            recs.push([a[2],a[0,2].pack('QQ'),msgtype,msg])
          else
            raise "DEVICE #{a[2]} NOT SUPPORTED"
          end
        end
      end
      recs.sort! do |a,b|
        Cg::time_diff(b[1],a[1])<=>0
      end
      [types,recs]
    end
  end
end
