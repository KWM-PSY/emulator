/* init.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Ruby extension to make use of the moogskt library - C code
 */

/* init.c */

#define __EXPORTED_HEADERS__

#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <sys/types.h>
#include <sys/time.h>

#define MOOG_H_TO_SRV_H ((float)0.465)
#define MAX_REM_ADDRS 4

typedef float hv2[2];
typedef float hv[3];
typedef float hv4[4];
typedef float hm4[4][4];

typedef double hv_d[3];

typedef struct
{
  char *loc_address,*rem_addresses[MAX_REM_ADDRS];
  int n_rem_addresses,recv_port,trans_port;
  uint8_t closed;
} moogskt_ruby_stc;

extern void moogskt_alive(char **address);
extern int moogskt_init(char *loc_address,char *rem_address1,char *rem_address2,char *rem_address3,char *rem_address4,int recv_port,int trans_port);
extern int moogskt_close(void);
extern int moogskt_send_pkt(int msg,char *payload);
extern int moogskt_recvport(void);
extern char *moogskt_recv_pkt(void);
extern int moogskt_recv_pkts(char ***pkts);
extern void lg(const char *fmt,...);

static VALUE new_moogskt_ruby(VALUE self,VALUE v_loc_address,VALUE v_rem_addresses,VALUE v_recv_port,VALUE v_trans_port);
static VALUE moogskt_ruby_recvport(VALUE self);
static VALUE moogskt_ruby_send_pkt(VALUE self,VALUE v_msg,VALUE v_pkt);
static VALUE moogskt_ruby_recv_pkt(VALUE self);
static VALUE moogskt_ruby_recv_pkts(VALUE self);
static VALUE moogskt_ruby_close(VALUE self);
static VALUE moogskt_ruby_alive(VALUE self);
static VALUE moogskt_ruby_moog_idea_to_server_idea(VALUE self,VALUE v_a);
static VALUE moogskt_ruby_server_idea_to_moog_idea(VALUE self,VALUE v_a);

static void do_close(moogskt_ruby_stc *s);
static void moogidea2serveridea(const float mi[6],hv sl,hv sa);
static void serveridea2moogidea(const hv sl,const hv sa,float mi[6]);

VALUE cls_moogskt_ruby;

void Init_moogskt_ruby(void)
{
  cls_moogskt_ruby=rb_define_class("Moogskt_ruby",rb_cObject);

  rb_define_const(cls_moogskt_ruby,"MOOG_H_TO_SRV_H",DBL2NUM(MOOG_H_TO_SRV_H));

  rb_define_singleton_method(cls_moogskt_ruby,"new",new_moogskt_ruby,4);
  rb_define_method(cls_moogskt_ruby,"recvport",moogskt_ruby_recvport,0);
  rb_define_method(cls_moogskt_ruby,"send_pkt",moogskt_ruby_send_pkt,2);
  rb_define_method(cls_moogskt_ruby,"recv_pkt",moogskt_ruby_recv_pkt,0);
  rb_define_method(cls_moogskt_ruby,"recv_pkts",moogskt_ruby_recv_pkts,0);
  rb_define_method(cls_moogskt_ruby,"close",moogskt_ruby_close,0);
  rb_define_singleton_method(cls_moogskt_ruby,"alive?",moogskt_ruby_alive,0);
  rb_define_singleton_method(cls_moogskt_ruby,"moog_idea_to_server_idea",moogskt_ruby_moog_idea_to_server_idea,1);
  rb_define_singleton_method(cls_moogskt_ruby,"server_idea_to_moog_idea",moogskt_ruby_server_idea_to_moog_idea,1);
}

static void free_moogskt_ruby(void *p);

static VALUE new_moogskt_ruby(VALUE self,VALUE v_loc_address,VALUE v_rem_addresses,VALUE v_recv_port,VALUE v_trans_port)
{
  moogskt_ruby_stc *s;
  VALUE sdata=Data_Make_Struct(cls_moogskt_ruby,moogskt_ruby_stc,NULL,free_moogskt_ruby,s),v;
  int i;
  
  bzero(s,sizeof(moogskt_ruby_stc));
  
  s->loc_address=strdup(RSTRING_PTR(v_loc_address));
  s->n_rem_addresses=RARRAY_LEN(v_rem_addresses);
  if(s->n_rem_addresses<1 || s->n_rem_addresses>MAX_REM_ADDRS)
    rb_raise(rb_eArgError,"%s: wrong number of addresses (%d)!",__func__,s->n_rem_addresses);
  
  for(i=0;i<s->n_rem_addresses;i++)
    s->rem_addresses[i]=strdup(RSTRING_PTR(rb_ary_entry(v_rem_addresses,i)));  
  for(i=s->n_rem_addresses;i<MAX_REM_ADDRS;i++)
  {
    s->rem_addresses[i]=malloc(1); // empty string
    s->rem_addresses[i][0]=0;
  }
  
  s->recv_port=FIX2INT(v_recv_port);
  s->trans_port=FIX2INT(v_trans_port);
  
  moogskt_init(s->loc_address,s->rem_addresses[0],s->rem_addresses[1],s->rem_addresses[2],s->rem_addresses[3],
	       s->recv_port,s->trans_port);
  
  return sdata;  
}

static void free_moogskt_ruby(void *p)
{
  moogskt_ruby_stc *s=(moogskt_ruby_stc *)p;
  int i;

  do_close(s);
  
  free(s->loc_address);

  for(i=0;i<MAX_REM_ADDRS;i++)
    free(s->rem_addresses[i]);

  free(p);
}

static VALUE moogskt_ruby_recvport(VALUE self)
{
  moogskt_ruby_stc *s;  
  Data_Get_Struct(self,moogskt_ruby_stc,s);

  return INT2FIX(moogskt_recvport());  
}

static VALUE moogskt_ruby_send_pkt(VALUE self,VALUE v_msg,VALUE v_pkt)
{
  moogskt_ruby_stc *s;  
  Data_Get_Struct(self,moogskt_ruby_stc,s);

  int msg=FIX2INT(v_msg);

//  lg("Sending msg %d",msg);
  
  moogskt_send_pkt(msg,v_pkt==Qnil ? NULL : RSTRING_PTR(v_pkt));
  
  return self;
}

static VALUE moogskt_ruby_recv_pkt(VALUE self)
{
  moogskt_ruby_stc *s;  
  Data_Get_Struct(self,moogskt_ruby_stc,s);

  char *p=moogskt_recv_pkt();

  if(!p)
    return Qnil;

  return rb_str_new_cstr(p);
}

static VALUE moogskt_ruby_recv_pkts(VALUE self)
{
  moogskt_ruby_stc *s;  
  Data_Get_Struct(self,moogskt_ruby_stc,s);
  
  char **pkts;
  int n_pkts=moogskt_recv_pkts(&pkts);
  
  if(n_pkts<=0)
    return Qnil;

  VALUE to_ret=rb_ary_new_capa(n_pkts);
  int i;

  for(i=0;i<n_pkts;i++)
  {
    rb_ary_store(to_ret,i,rb_str_new_cstr(pkts[i]));
    free(pkts[i]);
  }
  free(pkts);

  return to_ret;
}

static VALUE moogskt_ruby_close(VALUE self)
{
  moogskt_ruby_stc *s;  
  Data_Get_Struct(self,moogskt_ruby_stc,s);

  do_close(s);

  return self;
} 

static VALUE moogskt_ruby_alive(VALUE self)
{
  char *address=NULL;

  moogskt_alive(&address);

  if(!address)
    return Qnil;

  VALUE to_ret=rb_str_new_cstr(address);

  free(address);
  return to_ret;
}

static VALUE moogskt_ruby_moog_idea_to_server_idea(VALUE self,VALUE v_a)
{
  float mi[6],si[6];
  hv sl,sa;
  int i;

  for(i=0;i<6;i++)
    mi[i]=NUM2DBL(rb_ary_entry(v_a,i));

  moogidea2serveridea(mi,sl,sa);

  VALUE to_ret=rb_ary_new_capa(6);

  for(i=0;i<3;i++)
  {
    rb_ary_store(to_ret,i,DBL2NUM(sl[i]));
    rb_ary_store(to_ret,i+3,DBL2NUM(sa[i]));
  }

  return to_ret;
}

static VALUE moogskt_ruby_server_idea_to_moog_idea(VALUE self,VALUE v_a)
{
  hv sl,sa;
  float mi[6];
  int i;

  for(i=0;i<3;i++)
  {
    sl[i]=NUM2DBL(rb_ary_entry(v_a,i));
    sa[i]=NUM2DBL(rb_ary_entry(v_a,i+3));
  }

  serveridea2moogidea(sl,sa,mi);

  VALUE to_ret=rb_ary_new_capa(6);

  for(i=0;i<6;i++)
    rb_ary_store(to_ret,i,DBL2NUM(mi[i]));

  return to_ret;
}

static void do_close(moogskt_ruby_stc *s)
{
  if(!s->closed)
  {
    moogskt_close();
    s->closed=1;
  }
}

static void moogidea2serveridea(const float mi[6],hv sl,hv sa)
{
  sl[0]=mi[5]*1000.0; // lateral
  sl[1]=(-mi[2]+MOOG_H_TO_SRV_H)*1000.0; // heave
  sl[2]=mi[3]*1000.0; // surge
  
  sa[0]=mi[4]; // YAW
  sa[1]=-mi[1]; // PITCH
  sa[2]=-mi[0]; // ROLL
//  lg("=> mi %.3f %.3f %.3f %.3f %.3f %.3f sl %.3f %.3f %.3f sa %.3f %.3f %.3f",mi[0],mi[1],mi[2],mi[3],mi[4],mi[5],sl[0],sl[1],sl[2],sa[0],sa[1],sa[2]);
}

static void serveridea2moogidea(const hv sl,const hv sa,float mi[6])
{
  mi[0]=-sa[2]; // ROLL
  mi[1]=-sa[1]; // PITCH

  mi[2]=-(sl[1]/1000.0-MOOG_H_TO_SRV_H); // heave
  mi[3]=sl[2]/1000.0; // surge

  mi[4]=sa[0]; // YAW

  mi[5]=sl[0]/1000.0; // lateral
}
