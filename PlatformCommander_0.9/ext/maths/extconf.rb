# extconf.rb

=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Extension definition script

=end

require 'mkmf'

#$CFLAGS+=" -g -O6 -funsigned-char -fPIC -ffast-math -Werror -Wall -Wcast-align -Wno-declaration-after-statement \\
#-Wno-unused-function -Wno-unused-but-set-variable -Wno-unused-variable"
$CFLAGS+=" -g -O6 -funsigned-char -fPIC -ffast-math -Werror -Wall -Wno-cast-align"

#
# For address sanitizer, run as
#  LD_PRELOAD=/usr/lib/libasan.so ruby scripts/<script>
#

#$CFLAGS+=' -O1 -g -fsanitize=\'address\' -fsanitize=\'leak\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'address\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'leak\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'thread\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'undefined\' -fno-omit-frame-pointer'

#$LOCAL_LIBS=['m','gsl','gslcblas'].map do |s|
$LOCAL_LIBS=['m'].map do |s|
  '-l'+s
end.join(' ')

#$LOCAL_LIBS+=' -lasan'

create_makefile("maths")
