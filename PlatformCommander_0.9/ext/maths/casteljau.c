/* casteljau.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Casteljau spline algorithm
 */

#include "maths_include.h"

extern VALUE cls_maths_casteljau;

static void free_maths_casteljau(void *p);

VALUE new_maths_casteljau(VALUE self,VALUE v_array)
{
  maths_casteljau_stc *s;
  VALUE sdata=Data_Make_Struct(cls_maths_casteljau,maths_casteljau_stc,NULL,free_maths_casteljau,s);

  s->n_steps=RARRAY_LEN(v_array);
  s->steps=malloc(sizeof(double)*s->n_steps*2);

  int i;

  for(i=0;i<s->n_steps;i++)
    s->steps[i]=NUM2DBL(rb_ary_entry(v_array,i));
  
  return sdata;
}

static void free_maths_casteljau(void *p)
{
  maths_casteljau_stc *s=(maths_casteljau_stc *)p;

  free(s->steps);
  free(p);
}

VALUE maths_casteljau_calc(VALUE self,VALUE f_pos)
{
  maths_casteljau_stc *s;  
  Data_Get_Struct(self,maths_casteljau_stc,s);

  double pos=NUM2DBL(f_pos),res=casteljau_calc(s,pos);
      
  return DBL2NUM(res);
}

double casteljau_calc(maths_casteljau_stc *s,const double pos)
{
  double *fp=s->steps+s->n_steps;
  int i,j;

  memcpy(fp,s->steps,sizeof(double)*s->n_steps);
  for(i=1;i<s->n_steps;i++)
    for(j=0;j<s->n_steps-i;j++)
      fp[j]=(1.0-pos)*fp[j]+pos*fp[j+1];

  return fp[0];
}

#if 0
/*
 * Standalone, callable from c
 * Prev dot, dot from dot to, next dot
 * Extend direction to get dots 2 and 3
 * do cjau for dotfrom dot2 dot3 dotto
 */

#define CJAU_FD_PROP 0.3

void casteljau_four_dots(hv3 d[4],int *msecs,int n_msecs,hv3 *retv)
{
  float dist=vect_dist(d[1],d[2]);
  hv3 myd[4],work[4],ddir,v;

//  lg("CHIAMANO CON %d %.2f,%.2f,%.2f|%.2f,%.2f,%.2f|%.2f,%.2f,%.2f|%.2f,%.2f,%.2f",n_msecs,
//     d[0][0],d[0][1],d[0][2],d[1][0],d[1][1],d[1][2],d[2][0],d[2][1],d[2][2],d[3][0],d[3][1],d[3][2]);

  memcpy(myd[0],d[1],sizeof(hv3));
  vect_sub(d[1],d[0],ddir);
  vect_normalize(ddir);
  vect_mult_by_const(ddir,dist*CJAU_FD_PROP,v);
  vect_add(myd[0],v,myd[1]);

  memcpy(myd[3],d[2],sizeof(hv3));
  vect_sub(d[2],d[3],ddir);
  vect_normalize(ddir);
  vect_mult_by_const(ddir,dist*CJAU_FD_PROP,v);
  vect_add(myd[3],v,myd[2]);

/*
 * Now get the time points
 */

  int diff_msecs=msecs[n_msecs-1]-msecs[0],i,j,k,l;
  float pos;

  for(i=0;i<n_msecs;i++)
  {
//    pos=(msecs[i]-msecs[0])/(float)diff_msecs;

/*
 * DO NOT USE THE TIMES!!! Consider the intervals as equal
 */

    pos=(float)i/(n_msecs-1);    

    memcpy(work,myd,sizeof(hv3)*4);

    for(j=0;j<3;j++)
      for(k=1;k<4;k++)
	for(l=0;l<4-k;l++)
	  work[l][j]=(1.0-pos)*work[l][j]+pos*work[l+1][j];

    memcpy(retv+i,work,sizeof(hv3));
  }
}
#endif

