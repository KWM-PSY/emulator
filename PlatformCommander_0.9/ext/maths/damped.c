/* damped.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * damped harmonic oscillator
 *
 * Algorithm taken from
 * https://stackoverflow.com/questions/25938121/round-off-error-in-translated-javascript
 * that contains the translation to javascript of some fortan code,
 * with the fortan code included in comments.
 *
 * Apparently using VERLET INTEGRATION
 */

#include "maths_include.h"

extern VALUE cls_maths_dho;

static void free_maths_dho(void *p);
static size_t maths_dho_memsize(const void *p)
{
  return sizeof(maths_dho_stc);
}
const rb_data_type_t maths_dho_dtype={"maths_dho",
				      {0,free_maths_dho,maths_dho_memsize,},
				      0,0,
				      RUBY_TYPED_FREE_IMMEDIATELY,};

static int dho_calc(maths_dho_stc *s);
static void verlet(maths_dho_stc *s,float *pos,float *vel,float *t,float *a,float *e);
static float force(maths_dho_stc *s,float x,float v,float t);
static float energy(maths_dho_stc *s,float x,float v);

VALUE new_maths_dho(VALUE self,VALUE v_m,VALUE v_spring_c,VALUE v_damping,VALUE v_init_pos,VALUE v_init_vel,
		    VALUE v_t_step,VALUE v_t_max,VALUE v_energy_min)
{
  maths_dho_stc *s;
  VALUE sdata=TypedData_Make_Struct(cls_maths_dho,maths_dho_stc,&maths_dho_dtype,s);

  bzero(s,sizeof(maths_dho_stc));  

  s->m=NUM2DBL(v_m);
  s->spring_c=NUM2DBL(v_spring_c);
  s->damping=NUM2DBL(v_damping);
  s->init_pos=NUM2DBL(v_init_pos);
  s->init_vel=NUM2DBL(v_init_vel);
  s->t_step=NUM2DBL(v_t_step);
  s->t_max=NUM2DBL(v_t_max);
  s->energy_min=NUM2DBL(v_energy_min);
  
  s->damped_flg=dho_calc(s) ? 0 : 1;

  return sdata;
}

static void free_maths_dho(void *p)
{
  maths_dho_stc *s=(maths_dho_stc *)p;
  int i;
  
  for(i=0;i<s->n_steps;i++)
    free(s->res[i]);
  free(s->res);

  free(s);
}

VALUE maths_dho_damp_success(VALUE self)
{
  maths_dho_stc *s;  
  TypedData_Get_Struct(self,maths_dho_stc,&maths_dho_dtype,s);

  return (s->damped_flg) ? Qtrue : Qfalse;
}
  
VALUE maths_dho_duration(VALUE self)
{
  maths_dho_stc *s;  
  TypedData_Get_Struct(self,maths_dho_stc,&maths_dho_dtype,s);

  return DBL2NUM(s->t_step*s->n_steps);
}
    
VALUE maths_dho_arrays(VALUE self)
{
  maths_dho_stc *s;  
  TypedData_Get_Struct(self,maths_dho_stc,&maths_dho_dtype,s);

  VALUE to_ret=rb_ary_new_capa(6);

  rb_ary_store(to_ret,0,s->damped_flg ? Qtrue : Qfalse);
  rb_ary_store(to_ret,1,INT2FIX(s->n_steps));

  float *accum=malloc(sizeof(float)*s->n_steps);
  int i,j;

  for(i=0;i<5;i++)
  {
    for(j=0;j<s->n_steps;j++)
      accum[j]=s->res[j][i];
    rb_ary_store(to_ret,i+2,rb_str_new((char *)accum,sizeof(float)*s->n_steps));
  }

  free(accum);

  return to_ret;
}

VALUE maths_dho_calc_seq(VALUE self,VALUE v_fr_pos,VALUE v_to_pos)
{
  maths_dho_stc *s;  
  TypedData_Get_Struct(self,maths_dho_stc,&maths_dho_dtype,s);
  
  hv fr_pos_l,fr_pos_a,to_pos_l,to_pos_a,l_diff,a_diff;
  float cv;

  memcpy(fr_pos_l,RSTRING_PTR(v_fr_pos),sizeof(hv));
  memcpy(fr_pos_a,RSTRING_PTR(v_fr_pos)+sizeof(hv),sizeof(hv));
  memcpy(to_pos_l,RSTRING_PTR(v_to_pos),sizeof(hv));
  memcpy(to_pos_a,RSTRING_PTR(v_to_pos)+sizeof(hv),sizeof(hv));

  vect_sub(to_pos_l,fr_pos_l,l_diff);
  vect_sub(to_pos_a,fr_pos_a,a_diff);
  
  s->l.steps=realloc(s->l.steps,sizeof(hv)*s->n_steps);
  s->a.steps=realloc(s->a.steps,sizeof(hv)*s->n_steps);

  int i,j;
  
  for(i=0;i<s->n_steps;i++)
  {
    cv=s->init_pos-s->res[i][1]; // verlet result data goes from init_pos to 0
    
    for(j=0;j<3;j++)
    {
      s->l.steps[i][j]=fr_pos_l[j]+cv*(to_pos_l[j]-fr_pos_l[j])*cv;
      s->a.steps[i][j]=fr_pos_a[j]+cv*(to_pos_a[j]-fr_pos_a[j])*cv;
    }
  }

  return self;
}

VALUE maths_dho_pos_at(VALUE self,VALUE v_at)
{
  maths_dho_stc *s;  
  TypedData_Get_Struct(self,maths_dho_stc,&maths_dho_dtype,s);

  float pos=NUM2DBL(v_at);
  int ipos=(int)(s->n_steps*pos);

  if(ipos<0)
    ipos=0;
  else if(ipos>=s->n_steps)
    ipos=s->n_steps-1;

  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,rb_str_new((char *)s->l.steps[ipos],sizeof(hv)));
  rb_ary_store(to_ret,1,rb_str_new((char *)s->a.steps[ipos],sizeof(hv)));
  
  return to_ret;
}

VALUE maths_dho_end_pos_time(VALUE self)
{
  maths_dho_stc *s;  
  TypedData_Get_Struct(self,maths_dho_stc,&maths_dho_dtype,s);
  VALUE to_ret=rb_ary_new_capa(3);

  rb_ary_store(to_ret,0,rb_str_new((char *)s->l.steps[s->n_steps-1],sizeof(hv)));
  rb_ary_store(to_ret,1,rb_str_new((char *)s->a.steps[s->n_steps-1],sizeof(hv)));
  rb_ary_store(to_ret,2,DBL2NUM(s->t_step*s->n_steps));
  
  return to_ret;
}

static int dho_calc(maths_dho_stc *s)
{
  float pos=s->init_pos,vel=s->init_vel,t=0.0,a=0.0,e=0.0;

  while(1)
  {
//    lg("==> t %f",t);
    
    verlet(s,&pos,&vel,&t,&a,&e);
    s->res=realloc(s->res,sizeof(float *)*(s->n_steps+1));
    s->res[s->n_steps]=malloc(sizeof(float)*5);
    s->res[s->n_steps][0]=t;
    s->res[s->n_steps][1]=pos;
    s->res[s->n_steps][2]=vel;
    s->res[s->n_steps][3]=a;
    s->res[s->n_steps][4]=e;
    s->n_steps++;
    
    if(t>s->t_max || e<s->energy_min)
      break;
  }

//  lg("t %f e %f",t,e);
  
  return t>s->t_max ? -1 : 0;  
}

/*
 * Verlet integration:
 * position = position + velocity * timestep + Old Acceleration / 2 * timestep ^ 2
 * velocity = velocity + ((Acceleration + Old Acceleration) / 2) * timestep
 */

static void verlet(maths_dho_stc *s,float *pos,float *vel,float *time,float *acc,float *ene)
{
  float x=*pos,v=*vel,t=*time,f_osc,e,a,a2,next_x,next_v,next_t;

  f_osc=force(s,x,v,t);  
  e=energy(s,x,v);
  a=f_osc/s->m;
  next_x=x+v*s->t_step+0.5*a*s->t_step*s->t_step;  
  next_t=t+s->t_step;
  f_osc=force(s,next_x,v,next_t);
  a2=f_osc/s->m;  
  next_v=v+0.5*(a2+a)*s->t_step;

  (*pos)=next_x;
  (*vel)=next_v;
  (*time)=next_t;
  (*acc)=a;
  (*ene)=e;
}

static float force(maths_dho_stc *s,float x,float v,float t)
{
  return -s->damping*v-s->spring_c*x;
}

static float energy(maths_dho_stc *s,float x,float v)
{
  return 0.5*(v*v+s->spring_c*x*x);
}

