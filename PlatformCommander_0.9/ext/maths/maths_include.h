/* maths_include.h */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Defines the C maths functions, so they can be used in other C code in the whole project
 */

#ifndef MATHS_INCLUDE_H
#define MATHS_INCLUDE_H

#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <complex.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/time.h>
#include <linux/types.h>
//#include <gsl/gsl_integration.h>

#ifndef DBL_EPSILON
#define DBL_EPSILON ((double)2.22044604925031308085e-16L)
#endif

#define RAD2DEG (180.0/M_PI)
#define DEG2RAD (M_PI/180.0)

typedef int hv2_i[2];
typedef int hv_i[3];
typedef int hv4_i[4];

typedef float hv2[2];
typedef float hv[3];
typedef float hv4[4];

typedef double hv2_d[2];
typedef double hv_d[3];
typedef double hv4_d[4];

typedef float hm4[4][4];

typedef double hv_d[3];

typedef enum
{
  MATTYPE_UNIT,
  MATTYPE_TRASL,
  MATTYPE_ROTAXIS,
  MATTYPE_SCALE,
  MATTYPE_QUAT,
  MATTYPE_RAW,
} mattype_enum;

typedef enum
{
  ACCSIN_BOTH_LIN_ANG_ACC,
  ACCSIN_BOTH_LIN_ANG_DISP,
  ACCSIN_BOTH_ANG_LIN_ACC,
  ACCSIN_BOTH_ANG_LIN_DISP,
  ACCSIN_LIN_ACC_ANG_ACC_TIME,
  ACCSIN_LIN_ACC_ANG_DISP_TIME,
  ACCSIN_LIN_DISP_ANG_ACC_TIME,
  ACCSIN_LIN_DISP_ANG_DISP_TIME,
} accsin_type_enum;
  
typedef struct
{
  int msec;
  float *values;
} timepoint_stc;

typedef struct
{
  int n_values,n_points;
  timepoint_stc *tp;
} timepoints_stc;
  
typedef struct
{
  int n_steps;
  double *steps;
} maths_casteljau_stc;

typedef struct
{
  accsin_type_enum type;
  uint8_t loaded_flg,ang_or_lin_acc;
  float time;
  
  struct
  {
    hv from,dir,to,diff;
    float dist,acc;
  } a,l;
} maths_accsin_stc;

typedef struct
{
  float m,spring_c,damping,init_pos,init_vel,t_step,t_max,energy_min;
  int n_steps;
  uint8_t damped_flg;
  float **res;
  float time;

  struct
  {
    hv from,dir,to,diff,*steps;
    float dist,acc;
  } a,l;
} maths_dho_stc;

typedef struct
{
  float beta,smooth_data;
} maths_lowpass_stc;
  
typedef struct
{
  uint64_t cnt;
  int size;
  hv *data;
  double sum,sqsum;
} hv_averager_stc;
  
typedef struct
{
  uint64_t cnt;
  int size;
  float *data,stddev,avg;
  double sum,sqsum;
} flt_averager_stc;
  
typedef struct
{
  uint64_t cnt;
  int size;
  double *data,stddev,avg;
  double sum,sqsum;
} dbl_averager_stc;

extern void lg(const char *fmt,...);

extern const rb_data_type_t maths_dho_dtype;

extern void vect_negate(const hv c1,hv c2);
extern void vect_add(const hv c1,const hv c2,hv c3);
extern void vect_add_d(const hv_d c1,const hv_d c2,hv_d c3);
extern void vect_sub(const hv c1,const hv c2,hv c3);
extern void vect_sub_d(const hv_d c1,const hv_d c2,hv_d c3);
extern void vect_mult(const hv c1,const hv c2,hv c3);
extern void vect_mult_const(const hv v1,const float c,hv v2);
extern void vect_mult_const_d(const hv_d v1,const double c,hv_d v2);
extern float vect_normalize(const hv inv,hv outv);
extern float vect_modulo(const hv c);
extern float vect_dot_prod(const hv a,const hv b);
extern double vect_dot_prod_d(const hv_d a,const hv_d b);
extern void vect_cross_prod(const hv c1,const hv c2,hv c3);
extern void vect_center(const hv v1,const hv v2,hv vr);
extern float vect_max(const hv v);
extern void vect_three_points_normal(const hv p[3],hv norm);
extern void vect_unit_prop(const hv pin,hv pout);
extern float angle_btw_vects(const hv v1,const hv v2);
extern void mat4_unit(hm4 m);
extern void mat4_trasl(const hv v,hm4 m);
extern void mat4_scale(const hv v,hm4 m);
extern void mat4_shear(const hv v,hm4 m);
extern void mat4_transpose(const hm4 m1,hm4 m2);
extern void mat4_mult(const hm4 m1,const hm4 m2,hm4 m3);
extern void mat4_mult_only_rotscale(const hm4 m1,const hm4 m2,hm4 m3);
extern void mat4_rot_axis(const hv axis,const float ang,hm4 m);
extern void mat4_vect_mult(const hm4 m,const hv v1,hv v2);
extern void mat4_vect_mult_submat(const hm4 m,const hv v1,hv v2);
extern void mat4_vect_over_another(const hv v1,const hv v2,hm4 m);
extern void mat4_lookat(const hv eye,const hv center,const hv up,hm4 res);
extern void mat4_lookat_from_quat(const hv eye,const hv4 rot,hm4 res);
extern void mat4_refchange(const hv orig,const hv vx,const hv vy,const hv vz,hm4 res);
extern void mat4_perspective(const float fovy,const float aspect,const float near,const float far,hm4 res);
//extern void mat4_perspective_ohmd(const float fovy,const float aspect,const float near,const float far,hm4 res);
extern void mat4_tait_bryan(const hv ypr,hm4 m);
extern void mat4_print(char *title,hm4 m);

extern void quat_unit(hv4 quat);
extern float quat_dot_prod(const hv4 a,const hv4 b);
extern float quat_modulo(const hv4 quat);
extern void quat_invert(const hv4 qin,hv4 qout);
extern void quat_normalize(hv4 quat);
extern void quat_quat_mult(const hv4 q1,const hv4 q2,hv4 q3);
extern void tait_bryan_to_quat(const hv tb,hv4 q);
extern void quat_to_tait_bryan(const hv4 quat,hv tb);
extern void quat_to_hm4(const hv4 q,hm4 m);
extern void hm4_to_quat(const hm4 m,hv4 q);
extern void quat_from_vect_angle(const hv v,const float angle,hv4 q);
extern void rot_vect_by_quat(const hv4 q,const hv vin,hv vout);
extern void quat_vect_over_another(const hv v1,const hv v2,hv4 q);

extern float point_distance(const hv p1,const hv p2);
extern void three_points_to_plane(const hv p1,const hv p2,const hv p3,hv4 coef);
extern void lines_closest_points(const hv p1,const hv v1,const hv p2,const hv v2,hv cp1,hv cp2,float *scr,float *tcr);
extern int line_plane_intersect(const hv plane_point,const hv plane_normal,const hv line_point,const hv line_dir,hv intersect,float *fact);
extern void triangle_angles(const hv p1,const hv p2,const hv p3,float *a1,float *a2,float *a3);
extern float yaw_pitch_angle(const float yaw_from,const float pitch_from,const float yaw_to,const float pitch_to);

extern void velocity(const hv lin1,const hv ang1,const hv lin2,const hv ang2,const float time,float *lin_vel,float *ang_vel);
extern void interpolate_series(const timepoints_stc *tpin,const unsigned int interval,timepoints_stc *tpout);
extern void random_reorder(const int n,int reordered[n]);

extern double casteljau_calc(maths_casteljau_stc *s,const double pos);

extern hv_averager_stc *hv_averager_new(int size);
extern void free_hv_averager(hv_averager_stc *s);
extern void hv_averager_reset(hv_averager_stc *s);
extern void hv_averager_feed_sample(hv_averager_stc *s,const hv v,float *stddev);
extern void hv_averager_average(hv_averager_stc *s,hv avg);
extern void hv_averager_stats(hv_averager_stc *s,hv minv,hv maxv,hv ave,hv adev,hv sdev,hv svar,hv skew,hv kurt);

extern flt_averager_stc *flt_averager_new(int size);
extern void free_flt_averager(flt_averager_stc *s);
extern void flt_averager_reset(flt_averager_stc *s);
extern void flt_averager_reset_with_value(flt_averager_stc *s,const float rval);
extern void flt_averager_feed_sample(flt_averager_stc *s,const float v);
extern void flt_averager_subst_sample(flt_averager_stc *s,const int pos,const float v);
extern void flt_averager_average(flt_averager_stc *s,float *avg);
extern void flt_averager_stats(flt_averager_stc *s,float f[8]);

extern dbl_averager_stc *dbl_averager_new(int size);
extern void free_dbl_averager(dbl_averager_stc *s);
extern void dbl_averager_reset(dbl_averager_stc *s);
extern void dbl_averager_reset_with_value(dbl_averager_stc *s,const double rval);
extern void dbl_averager_feed_sample(dbl_averager_stc *s,const double v);
extern void dbl_averager_subst_sample(dbl_averager_stc *s,const int pos,const double v);

extern void sinacc_speed_pos(const float max_acc,const float period_len,const float cur_time,float *speed,float *pos);
extern void accsin_pos_at(maths_accsin_stc *s,const float at,hv l,hv a);

extern float lowpass_next(maths_lowpass_stc *s,float raw_value);

#endif

