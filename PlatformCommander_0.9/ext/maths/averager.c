/* averager.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Extension to compute averages of running data
 */

/* averager.c */

#include "maths_include.h"
#include <values.h>

extern void lg(const char *fmt,...);

hv_averager_stc *hv_averager_new(int size)
{
  hv_averager_stc *s=malloc(sizeof(hv_averager_stc));

  s->size=size;
  s->data=malloc(sizeof(hv)*s->size);

  hv_averager_reset(s);

  return s;
}
    
void free_hv_averager(hv_averager_stc *s)
{
  free(s->data);
  free(s);
}

void hv_averager_reset(hv_averager_stc *s)
{
  s->cnt=0;
  s->sum=s->sqsum=0.0;  
  bzero(s->data,sizeof(hv)*s->size);  
}

void hv_averager_feed_sample(hv_averager_stc *s,const hv v,float *stddev)
{
  s->cnt++;
  
  float m=vect_modulo(s->data[s->size-1]);

  s->sum-=m;
  s->sqsum-=m*m;  
    
  memmove(s->data+1,s->data,sizeof(hv)*(s->size-1));
  memcpy(s->data,v,sizeof(hv));

  m=vect_modulo(v);
  s->sum+=m;
  s->sqsum+=m*m;

  (*stddev)=sqrt(s->size*s->sqsum-(s->sum*s->sum))/s->size;  
}

void hv_averager_average(hv_averager_stc *s,hv avg)
{
  int i,j;
  
  bzero(avg,sizeof(hv));  
  for(i=0;i<s->size;i++)
    for(j=0;j<3;j++)
      avg[j]+=s->data[i][j];
  for(i=0;i<3;i++)
    avg[i]/=s->size;

//  for(i=0;i<s->size;i++)
//    printf("#%3d: %10.5f %10.5f %10.5f\n",i,s->data[i][0],s->data[i][1],s->data[i][2]);
//  printf("AVG:: %10.5f %10.5f %10.5f\n",avg[0],avg[1],avg[2]);
}

void hv_averager_stats(hv_averager_stc *s,hv minv,hv maxv,hv ave,hv adev,hv sdev,hv svar,hv skew,hv kurt)
{
  hv totv={0.0,0.0,0.0};
  int i,j;

  for(i=0;i<3;i++)
  {
    minv[i]=FLT_MAX;
    maxv[i]=FLT_MIN;
    adev[i]=svar[i]=skew[i]=kurt[i]=0.0;
  }

  for(i=0;i<s->size;i++)
    for(j=0;j<3;j++)
    {
      totv[j]+=s->data[i][j];
    
      if(s->data[i][j]<minv[j])
	minv[j]=s->data[i][j];
      if(s->data[i][j]>maxv[j])
	maxv[j]=s->data[i][j];
    }

  hv v1,v2;
  
  vect_mult_const(totv,1.0/s->size,ave);

  for(i=0;i<s->size;i++)
  {
    vect_sub(s->data[i],ave,v1);
    vect_mult(v1,v1,v2);
    
    for(j=0;j<3;j++)
      adev[j]+=fabsf(v1[j]);

    vect_add(svar,v2,svar);
    vect_mult(v2,v1,v2);
    vect_add(skew,v2,skew);
    vect_mult(v2,v1,v2);
    vect_add(kurt,v2,kurt);
  }

  vect_mult_const(adev,1.0/s->size,adev);
  vect_mult_const(svar,1.0/(s->size-1),adev);
  
  for(i=0;i<3;i++)
  {
    sdev[i]=sqrtf(svar[i]);
    if(svar[i])
    {
      skew[i]/=s->size*svar[i]*sdev[i];
      kurt[i]=(kurt[i]/(s->size*svar[i]*svar[i]))-3.0;
    }
    else
    {
      lg("%s: Comp %d: no skew/kurtosis when variance=0",__func__,i);
      skew[i]=kurt[i]=0.0;
    }
  }
}

/*
 * Averager for float
 */

flt_averager_stc *flt_averager_new(int size)
{
  flt_averager_stc *s=malloc(sizeof(flt_averager_stc));

  s->size=size;
  s->data=malloc(sizeof(float)*s->size);

  flt_averager_reset(s);

  return s;
}
    
void free_flt_averager(flt_averager_stc *s)
{
  free(s->data);
  free(s);
}

void flt_averager_reset(flt_averager_stc *s)
{
  s->cnt=0;
  s->sum=s->sqsum=0.0;  
  bzero(s->data,sizeof(float)*s->size);  
}

void flt_averager_reset_with_value(flt_averager_stc *s,const float rval)
{
  s->cnt=s->size;
  s->sum=rval*s->size;
  s->sqsum=rval*s->sum;

  int i;

  for(i=0;i<s->size;i++)
    s->data[i]=rval;
}

void flt_averager_feed_sample(flt_averager_stc *s,const float v)
{
//  fprintf(stderr,"{%f}",v);
  s->cnt++;
  
  float ov=s->data[s->size-1];

  s->sum-=ov;
  s->sqsum-=ov*ov;  
    
  memmove(s->data+1,s->data,sizeof(float)*(s->size-1));
  s->data[0]=v;  

  s->sum+=v;
  s->sqsum+=v*v;

  int cnt=s->size<s->cnt ? s->size : s->cnt;

  s->avg=s->sum/cnt;  
  s->stddev=sqrt(s->size*s->sqsum-(s->sum*s->sum))/cnt;
  if(isnan(s->stddev))
    s->stddev=0.0;
}

void flt_averager_subst_sample(flt_averager_stc *s,const int pos,const float v)
{
  if(pos<0 || pos>=s->size)
  {
    lg("%s: Sorry: bad position (%d)",pos);
    return;
  }

  if(s->cnt<=pos)
    s->cnt=pos+1;

  float ov=s->data[pos];
  
  s->sum-=ov;
  s->sqsum-=ov*ov;  
  
  s->data[pos]=v;

  s->sum+=v;
  s->sqsum+=v*v;
  
  int cnt=s->size<s->cnt ? s->size : s->cnt;

  s->avg=s->sum/cnt;  
  s->stddev=sqrt(s->size*s->sqsum-(s->sum*s->sum))/cnt;
  if(isnan(s->stddev))
    s->stddev=0.0;
}

void flt_averager_average(flt_averager_stc *s,float *a)
{
  int i;
  
  float avg=0.0;
  
  for(i=0;i<s->size;i++)
    avg+=s->data[i];
  avg/=s->size;

  (*a)=avg;

//  for(i=0;i<s->size;i++)
//    printf("#%3d: %10.5f\n",i,s->data[i]);
//  printf("AVG:: %10.5f\n",avg);
}

void flt_averager_stats(flt_averager_stc *s,float f[8])
{
  float totv=0.0,minv,maxv,ave,adev,sdev,svar,skew,kurt;
  int i;

  minv=FLT_MAX;
  maxv=FLT_MIN;
  adev=svar=skew=kurt=0.0;

  for(i=0;i<s->size;i++)
  {
    totv+=s->data[i];
    
    if(s->data[i]<minv)
      minv=s->data[i];
    if(s->data[i]>maxv)
      maxv=s->data[i];
  }

  float v1,v2;

  ave=totv/s->size;

  for(i=0;i<s->size;i++)
  {
    v1=s->data[i]-ave;
    v2=v1*v1;
    
    adev+=fabsf(v1);

    svar+=v2;
    v2*=v1;
    skew+=v2;
    v2*=v1;
    kurt+=v2;
  }

  adev/=s->size;
  svar/=s->size-1;

  sdev=sqrtf(svar);
  if(svar)
  {
    skew/=s->size*svar*sdev;
    kurt=(kurt/(s->size*svar*svar))-3.0;
  }
  else
  {
    lg("%s: Comp %d: no skew/kurtosis when variance=0",__func__,i);
    skew=kurt=0.0;
  }

  f[0]=minv;
  f[1]=maxv;
  f[2]=ave;
  f[3]=adev;
  f[4]=sdev;
  f[5]=svar;
  f[6]=skew;
  f[7]=kurt;
}

/*
 * Averager for double
 */

dbl_averager_stc *dbl_averager_new(int size)
{
  dbl_averager_stc *s=malloc(sizeof(dbl_averager_stc));

  s->size=size;
  s->data=malloc(sizeof(double)*s->size);

  dbl_averager_reset(s);

  return s;
}
    
void free_dbl_averager(dbl_averager_stc *s)
{
  free(s->data);
  free(s);
}

void dbl_averager_reset(dbl_averager_stc *s)
{
  s->cnt=0;
  s->sum=s->sqsum=0.0;  
  bzero(s->data,sizeof(double)*s->size);  
}

void dbl_averager_reset_with_value(dbl_averager_stc *s,const double rval)
{
  s->cnt=s->size;
  s->sum=rval*s->size;
  s->sqsum=rval*s->sum;

  int i;

  for(i=0;i<s->size;i++)
    s->data[i]=rval;
}

void dbl_averager_feed_sample(dbl_averager_stc *s,const double v)
{
//  fprintf(stderr,"{%f}",v);
  s->cnt++;
  
  double ov=s->data[s->size-1];

  s->sum-=ov;
  s->sqsum-=ov*ov;  
    
  memmove(s->data+1,s->data,sizeof(double)*(s->size-1));
  s->data[0]=v;  

  s->sum+=v;
  s->sqsum+=v*v;

  int cnt=s->size<s->cnt ? s->size : s->cnt;

  s->avg=s->sum/cnt;  
  s->stddev=sqrt(s->size*s->sqsum-(s->sum*s->sum))/cnt;
  if(isnan(s->stddev))
    s->stddev=0.0;
}

void dbl_averager_subst_sample(dbl_averager_stc *s,const int pos,const double v)
{
  if(pos<0 || pos>=s->size)
  {
    lg("%s: Sorry: bad position (%d)",pos);
    return;
  }

  if(s->cnt<=pos)
    s->cnt=pos+1;

  double ov=s->data[pos];
  
  s->sum-=ov;
  s->sqsum-=ov*ov;  
  
  s->data[pos]=v;

  s->sum+=v;
  s->sqsum+=v*v;
  
  int cnt=s->size<s->cnt ? s->size : s->cnt;

  s->avg=s->sum/cnt;  
  s->stddev=sqrt(s->size*s->sqsum-(s->sum*s->sum))/cnt;
  if(isnan(s->stddev))
    s->stddev=0.0;
}
