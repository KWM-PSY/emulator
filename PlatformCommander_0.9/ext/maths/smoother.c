/* smoother.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================\
 *
 * Simple data smoother
 */

#include "maths_include.h"

typedef struct
{
  int hist_length,cnt;
  float discard_thresh,*history,latest_avg;
} maths_smoother_stc;

extern VALUE cls_maths_smoother;

static void free_maths_smoother(void *p);

VALUE new_maths_smoother(VALUE self,VALUE v_hist_length,VALUE v_discard_thresh)
{
  maths_smoother_stc *s;
  VALUE sdata=Data_Make_Struct(cls_maths_smoother,maths_smoother_stc,NULL,free_maths_smoother,s);

  bzero(s,sizeof(maths_smoother_stc));  

  s->hist_length=FIX2INT(v_hist_length);
  s->discard_thresh=NUM2DBL(v_discard_thresh);
  s->history=malloc(sizeof(float)*s->hist_length);
  bzero(s->history,sizeof(float)*s->hist_length);  
  
  return sdata;
}

static void free_maths_smoother(void *p)
{
  maths_smoother_stc *s=(maths_smoother_stc *)p;

  free(s->history);

  free(p);  
}

VALUE maths_smoother_feed(VALUE self,VALUE v_new_value)
{
  maths_smoother_stc *s;  
  Data_Get_Struct(self,maths_smoother_stc,s);

  s->cnt++;

  float nv=NUM2DBL(v_new_value);

  if(s->cnt<s->hist_length || fabsf(s->latest_avg-nv)<=s->discard_thresh)
  {
    int i;    
    
    memcpy(s->history+1,s->history,sizeof(float)*(s->hist_length-1));
    s->history[0]=nv;

    for(s->latest_avg=0.0,i=0;i<s->hist_length;i++)
      s->latest_avg+=s->history[i];
    s->latest_avg/=s->hist_length;
  }

  return DBL2NUM(s->latest_avg);
}

