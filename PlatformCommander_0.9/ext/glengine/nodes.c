/* dist_nodes.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Gl engine: functions for creating and managing nodes
 */

#include "glengine.h"

/***************************************************************************************
 *                                   TETRAHEDRON                                       *
 *                                                                                     *
 ***************************************************************************************/

node_tria_stc *trias_tetrahedron(float size)
{
  node_tria_stc *nt=alloc_nt(12,4);
  float v1=sqrtf(8.0/9.0)/2.0,fact=size/sqrtf(8.0/3.0);

  hv coord[4]=
    {
      {v1*fact,-fact/3.0,0.0},
      {(-v1-sqrtf(2.0/9.0))*fact,-fact/3.0,sqrtf(2.0/3.0)*fact},
      {(-v1-sqrtf(2.0/9.0))*fact,-fact/3.0,-sqrtf(2.0/3.0)*fact},
      {-v1*fact,fact,0.0}
    };

  add_tria_with_normal(nt,0,0,coord[1],coord[0],coord[3]);
  add_tria_with_normal(nt,3,1,coord[2],coord[1],coord[3]);
  add_tria_with_normal(nt,6,2,coord[0],coord[2],coord[3]);
  add_tria_with_normal(nt,9,3,coord[0],coord[1],coord[2]);

  float t=tanf(30.0*M_PI/180.0)*0.5;  
  hv2 texcoords[4]={{0.0,0.5},{0.5,0.5+t},{0.5,0.5-t},{1.0,0.5}};
		    
  memcpy(nt->p[0].tp,texcoords[3],sizeof(hv2));
  memcpy(nt->p[1].tp,texcoords[2],sizeof(hv2));
  memcpy(nt->p[2].tp,texcoords[1],sizeof(hv2));

  memcpy(nt->p[3].tp,texcoords[0],sizeof(hv2));
  memcpy(nt->p[4].tp,texcoords[2],sizeof(hv2));
  memcpy(nt->p[5].tp,texcoords[1],sizeof(hv2));

  memcpy(nt->p[6].tp,texcoords[1],sizeof(hv2));
  memcpy(nt->p[7].tp,texcoords[2],sizeof(hv2));
  memcpy(nt->p[8].tp,texcoords[3],sizeof(hv2));
  
  memcpy(nt->p[9].tp,texcoords[1],sizeof(hv2));
  memcpy(nt->p[10].tp,texcoords[0],sizeof(hv2));
  memcpy(nt->p[11].tp,texcoords[2],sizeof(hv2));

  return nt;
}

/***************************************************************************************
 *                                   CYLINDER                                          *
 *                                                                                     *
 ***************************************************************************************/

node_tria_stc *trias_cylinder(float radius,float len,int n_refinements)
{
  int i,j,nr1=n_refinements+1;
  tria_stc *t;
  float step=M_PI*2.0/n_refinements;
  hv d,n,nleft={-1.0,0.0,0.0},nright={1.0,0.0,0.0};
  float cover_pct=radius/(len+radius*2);

  node_tria_stc *nt=alloc_nt(2+nr1*4,n_refinements*4);

  bzero(d,sizeof(hv));

  for(i=0;i<nr1;i++)
  {
    for(j=0;j<4;j++)      
      nt->p[i+nr1*j].prog=i+nr1*j;
    
    d[2]=sinf(step*i)*radius;
    d[1]=cosf(step*i)*radius;

    memcpy(nt->p[i].p,d,sizeof(hv));
    nt->p[i].p[0]=-len/2.0;    
    memcpy(nt->p[i].n,nleft,sizeof(hv));

    memcpy(nt->p[i+nr1].p,d,sizeof(hv));
    nt->p[i+nr1].p[0]=-len/2.0;
    vect_normalize(d,nt->p[i+nr1].n);

    memcpy(nt->p[i+nr1*2].p,d,sizeof(hv));
    nt->p[i+nr1*2].p[0]=len/2.0;
    vect_normalize(d,nt->p[i+nr1*2].n);

    memcpy(nt->p[i+nr1*3].p,d,sizeof(hv));
    nt->p[i+nr1*3].p[0]=len/2.0;    
    memcpy(nt->p[i+nr1*3].n,nright,sizeof(hv));

    d[1]=(1.0/n_refinements)*i;

    d[0]=cover_pct;
    memcpy(nt->p[i].tp,d,sizeof(hv2));
    memcpy(nt->p[i+nr1].tp,d,sizeof(hv2));
    d[0]=1.0-cover_pct;
    memcpy(nt->p[i+nr1*2].tp,d,sizeof(hv2));
    memcpy(nt->p[i+nr1*3].tp,d,sizeof(hv2));    
  }
  nt->p[nr1*4].prog=nr1*4;
  nt->p[nr1*4].p[0]=-len/2.0;
  memcpy(nt->p[nr1*4].n,nleft,sizeof(hv));
  nt->p[nr1*4].tp[0]=0.0;
  nt->p[nr1*4].tp[1]=0.5;
  
  nt->p[nr1*4+1].prog=nr1*4+1;
  nt->p[nr1*4+1].p[0]=len/2.0;
  memcpy(nt->p[nr1*4+1].n,nright,sizeof(hv));
  nt->p[nr1*4+1].tp[0]=1.0;
  nt->p[nr1*4+1].tp[1]=0.5;  

  for(i=0;i<n_refinements;i++)
  {
    for(j=0;j<4;j++)      
      nt->t[i+n_refinements*j].prog=i+n_refinements*j;

    j=i+1;
    
    t=nt->t+i;
    t->v[0]=nr1*4;
    t->v[1]=j;
    t->v[2]=i;
    
    t=nt->t+(i+n_refinements);
    t->v[0]=i+nr1;
    t->v[1]=j+nr1;
    t->v[2]=i+nr1*2;
    
    t=nt->t+(i+n_refinements*2);
    t->v[0]=j+nr1;
    t->v[1]=j+nr1*2;
    t->v[2]=i+nr1*2;
    
    t=nt->t+(i+n_refinements*3);    
    t->v[0]=nr1*4+1;
    t->v[1]=i+nr1*3;
    t->v[2]=j+nr1*3;
  }
  
  return nt;
}

/***************************************************************************************
 *                                   CONE                                              *
 *                                                                                     *
 ***************************************************************************************/

node_tria_stc *trias_cone(float radius,float len,int n_refinements)
{
  int i,j,nr1=n_refinements+1;

  float angle=atanf(radius/len);
    
  tria_stc *t;
  float step=M_PI*2.0/n_refinements;
  hv d,n,nright={1.0,0.0,0.0};
  float cover_pct=radius/(len+radius),ang;

  node_tria_stc *nt=alloc_nt(1+nr1*3,n_refinements*2);

  bzero(d,sizeof(hv));

  for(i=0;i<nr1;i++)
  {
    for(j=0;j<3;j++)      
      nt->p[i+nr1*j].prog=i+nr1*j;

    ang=step*i;

    d[2]=sinf(ang)*radius;
    d[1]=cosf(ang)*radius;
    vect_normalize(d,n);

    nt->p[i].p[0]=-len/2.0;
    nt->p[i].p[1]=nt->p[i].p[2]=0.0;
    memcpy(nt->p[i].n,n,sizeof(hv));    

    memcpy(nt->p[i+nr1].p,d,sizeof(hv));
    nt->p[i+nr1].p[0]=len/2.0;
    memcpy(nt->p[i+nr1].n,n,sizeof(hv));
    
    memcpy(nt->p[i+nr1*2].p,d,sizeof(hv));
    nt->p[i+nr1*2].p[0]=len/2.0;    
    memcpy(nt->p[i+nr1*2].n,nright,sizeof(hv));

    nt->p[i].tp[0]=0.0;
    nt->p[i].tp[1]=(1.0/n_refinements)*(i+0.5);

    d[1]=(1.0/n_refinements)*i;
    d[0]=1.0-cover_pct;
    memcpy(nt->p[i+nr1].tp,d,sizeof(hv2));
    memcpy(nt->p[i+nr1*2].tp,d,sizeof(hv2));
  }

  nt->p[nr1*3].prog=nr1*3;
  nt->p[nr1*3].p[0]=len/2.0;
  memcpy(nt->p[nr1*3].n,nright,sizeof(hv));
  nt->p[nr1*3].tp[0]=1.0;
  nt->p[nr1*3].tp[1]=0.5;  

  for(i=0;i<n_refinements;i++)
  {
    for(j=0;j<2;j++)      
      nt->t[i+n_refinements*j].prog=i+n_refinements*j;

    j=i+1;

    t=nt->t+i;
    t->v[0]=i;
    t->v[1]=j+nr1;
    t->v[2]=i+nr1;
    
    t=nt->t+(i+n_refinements);
    t->v[0]=nr1*3;
    t->v[1]=i+nr1*2;
    t->v[2]=j+nr1*2;
  }
  
  return nt;
}

/***************************************************************************************
 *                                   SPHERE                                            *
 *                                                                                     *
 ***************************************************************************************/

/*
 * build an icosphere
 * inspired by http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
 */

static void recurse_accumulate_sphere(node_tria_stc *nt,float radius,const hv_i faces,int remaining_refinements)
{
  if(remaining_refinements>0)
  {
    int i,j;

/*
 * get middle points of sides, extend them to radius
 */

    int newp[3]={nt->n_p,nt->n_p+1,nt->n_p+2};

    nt->n_p+=3;
    nt->p=realloc(nt->p,sizeof(point_stc)*nt->n_p);

    point_stc *vert[3]={nt->p+faces[0],nt->p+faces[1],nt->p+faces[2]},*pp;

    for(i=0;i<3;i++)
    {
      int refp[2]={i,(i+1)%3};	
	
      pp=nt->p+newp[i];
      pp->prog=newp[i];
      for(j=0;j<3;j++)
	pp->p[j]=vert[refp[0]]->p[j]+(vert[refp[1]]->p[j]-vert[refp[0]]->p[j])/2.0;
      
      vect_normalize(pp->p,pp->n);
      vect_mult_const(pp->n,radius,pp->p);      
//      vect_mult_const(pp->n,-1.0,pp->n); /////////////////// here to flip the normal to inside!
      pp->tp[0]=atan2f(pp->n[0],-pp->n[2])/(2.0*M_PI)+0.5; // for 360degree proj
      pp->tp[1]=0.5-asinf(pp->n[1])/M_PI;
    }
    
    {
      hv_i vi={newp[0],faces[1],newp[1]};      
      recurse_accumulate_sphere(nt,radius,vi,remaining_refinements-1);
    }
    {
      hv_i vi={newp[1],faces[2],newp[2]};      
      recurse_accumulate_sphere(nt,radius,vi,remaining_refinements-1);
    }
    {
      hv_i vi={newp[2],faces[0],newp[0]};      
      recurse_accumulate_sphere(nt,radius,vi,remaining_refinements-1);
    }
    {
      hv_i vi={newp[0],newp[1],newp[2]};      
      recurse_accumulate_sphere(nt,radius,vi,remaining_refinements-1);
    }
  }
  else
  {
    tria_stc *tp;
    
    nt->t=realloc(nt->t,sizeof(tria_stc)*(nt->n_t+1));
    tp=nt->t+(nt->n_t);
    tp->prog=nt->n_t++;
    if((nt->n_t%1000000)==0)
      lg("Allocated %d triangles",nt->n_t);

    tp->v[0]=faces[0];
    tp->v[1]=faces[1];
    tp->v[2]=faces[2];
  }
}

node_tria_stc *trias_sphere(float radius,int n_refinements)
{
  float t=(1.0+sqrtf(5.0))/2.0,fact=1.0/sqrtf(t*t+1.0);

  fact*=radius;  
  t*=fact;
  
  hv vertices[12]=
    {
      {-fact,t,0.0},
      {fact,t,0.0},
      {-fact,-t,0.0},
      {fact,-t,0.0},

      {0.0,-fact,t},
      {0.0,fact,t},
      {0.0,-fact,-t},
      {0.0,fact,-t},
	
      {t,0.0,-fact},
      {t,0.0,fact},
      {-t,0.0,-fact},
      {-t,0.0,fact}
    };

  static const hv_i faces[20]=
    {
      {0,11,5},
      {0,5,1},
      {0,1,7},
      {0,7,10},
      {0,10,11},
      
      {1,5,9},
      {5,11,4},
      {11,10,2},
      {10,7,6},
      {7,1,8},
      
      {3,9,4},
      {3,4,2},
      {3,2,6},
      {3,6,8},
      {3,8,9},
      
      {4,9,5},
      {2,4,11},
      {6,2,10},
      {8,6,7},
      {9,8,1}
    };

//  hv basev[20][3];
  int i,j;

//  for(i=0;i<20;i++)
//    for(j=0;j<3;j++)
//      memcpy(basev[i][j],vertices[faces[i][j]],sizeof(hv));

  node_tria_stc *nt=alloc_nt(12,0);
  point_stc *p;

  for(p=nt->p,i=0;i<12;i++,p++)
  {
    p->prog=i;
    memcpy(p->p,vertices[i],sizeof(hv));
    vect_normalize(p->p,p->n);

    p->tp[0]=atan2f(p->n[0],-p->n[2])/(2.0*M_PI)+0.5; // for 360degree proj
    p->tp[1]=0.5-asinf(p->n[1])/M_PI;
  }

  for(i=0;i<20;i++)  
    recurse_accumulate_sphere(nt,radius,faces[i],n_refinements);

//  for(p=nt->p,i=0;i<nt->n_p;i++,p++)
//    lg("Point%d(%d/%p): %.2f,%.2f,%.2f N %.2f,%.2f,%.2f",i,p->prog,p,p->p[0],p->p[1],p->p[2],p->n[0],p->n[1],p->n[2]);
  
  return nt;
}

/***************************************************************************************
 *                                   MOOG PLATE                                        *
 *                                                                                     *
 ***************************************************************************************/

node_tria_stc *trias_moog_plate(float len1,float len2,float thick,float rotang)
{
  node_tria_stc *nt=alloc_nt(36,20);
  
  float hthick=thick/2.0,tria_side=len1+len2*2,tria_h=tria_side*sqrtf(3.0)/2.0,small_tria_h=len2*sqrtf(3.0)/2.0,
    tria_rad=tria_side*2.0/3.0,shortrad=tria_rad-small_tria_h,ang=atanf((len2/2.0)/shortrad),extlen=sqrtf(len2*len2/4.0+shortrad*shortrad),a,si,co;
  
  lg("l1 %f l2 %f th %f Angle is %f",len1,len2,thick,ang*180.0/M_PI);
  
  hv2 main_coord[6];

  a=-ang;
  main_coord[0][0]=sinf(a)*extlen;
  main_coord[0][1]=cosf(a)*extlen;
  a=ang;
  main_coord[1][0]=sinf(a)*extlen;
  main_coord[1][1]=cosf(a)*extlen;
  a=M_PI*2.0/3.0-ang;
  main_coord[2][0]=sinf(a)*extlen;
  main_coord[2][1]=cosf(a)*extlen;
  a=M_PI*2.0/3.0+ang;
  main_coord[3][0]=sinf(a)*extlen;
  main_coord[3][1]=cosf(a)*extlen;
  a=M_PI*4.0/3.0-ang;
  main_coord[4][0]=sinf(a)*extlen;
  main_coord[4][1]=cosf(a)*extlen;
  a=M_PI*4.0/3.0+ang;
  main_coord[5][0]=sinf(a)*extlen;
  main_coord[5][1]=cosf(a)*extlen;

  /* lg("MAICO %.2f,%.2f | %.2f,%.2f | %.2f,%.2f | %.2f,%.2f | %.2f,%.2f | %.2f,%.2f", */
  /*    main_coord[0][0],main_coord[0][1], */
  /*    main_coord[1][0],main_coord[1][1], */
  /*    main_coord[2][0],main_coord[2][1], */
  /*    main_coord[3][0],main_coord[3][1], */
  /*    main_coord[4][0],main_coord[4][1], */
  /*    main_coord[5][0],main_coord[5][1]); */

  point_stc *p;
  int i,j;
  float lcomp=sqrtf(0.75);  
  hv nup={0.0,1.0,0.0},ndown={0.0,-1.0,0.0},
    nlongsides[3]={{-lcomp,0.0,-0.5},{lcomp,0.0,-0.5},{0.0,0.0,1.0}},
    nshortsides[3]={{0.0,0.0,-1.0},{lcomp,0.0,0.5},{-lcomp,0.0,0.5}};      

  for(p=nt->p,i=0;i<6;i++,p++) // top
  {
    p->prog=i;
    p->p[0]=main_coord[i][0];
    p->p[1]=hthick;
    p->p[2]=main_coord[i][1];
    memcpy(p->n,nup,sizeof(hv));
  }

  for(p=nt->p+6,i=0;i<6;i++,p++) // top, long sides
  {
    p->prog=i+6;
    p->p[0]=main_coord[i][0];
    p->p[1]=hthick;
    p->p[2]=main_coord[i][1];
    memcpy(p->n,nlongsides[((i+1)>>1)%3],sizeof(hv));
  }

  for(p=nt->p+12,i=0;i<6;i++,p++) // top, short sides
  {
    p->prog=i+12;
    p->p[0]=main_coord[i][0];
    p->p[1]=hthick;
    p->p[2]=main_coord[i][1];
    memcpy(p->n,nshortsides[((i+4)>>1)%3],sizeof(hv));
  }

  for(p=nt->p+18,i=0;i<6;i++,p++) // bottom, long sides
  {
    p->prog=i+18;
    p->p[0]=main_coord[i][0];
    p->p[1]=-hthick;
    p->p[2]=main_coord[i][1];
    memcpy(p->n,nlongsides[((i+1)>>1)%3],sizeof(hv));
  }

  for(p=nt->p+24,i=0;i<6;i++,p++) // bottom, short sides
  {
    p->prog=i+24;
    p->p[0]=main_coord[i][0];
    p->p[1]=-hthick;
    p->p[2]=main_coord[i][1];
    memcpy(p->n,nshortsides[((i+4)>>1)%3],sizeof(hv));
  }

  for(p=nt->p+30,i=0;i<6;i++,p++) // bottom
  {
    p->prog=i+30;
    p->p[0]=main_coord[i][0];
    p->p[1]=-hthick;
    p->p[2]=main_coord[i][1];
    memcpy(p->n,ndown,sizeof(hv));
  }

  if(rotang!=0)
  {
    hv rotaxis={0.0,1.0,0.0};      
    hm4 m;

    mat4_rot_axis(rotaxis,rotang,m);
    
    for(p=nt->p,i=0;i<nt->n_p;i++,p++)
      mat4_vect_mult(m,p->p,p->p);
  }
  
/*
 * Top
 */
  
  nt->t[0].v[0]=0;
  nt->t[0].v[1]=1;
  nt->t[0].v[2]=5;
  
  nt->t[1].v[0]=1;
  nt->t[1].v[1]=2;
  nt->t[1].v[2]=5;

  nt->t[2].v[0]=4;
  nt->t[2].v[1]=5;
  nt->t[2].v[2]=3;

  nt->t[3].v[0]=3;
  nt->t[3].v[1]=5;
  nt->t[3].v[2]=2;

/*
 * sides
 */

  tria_stc *t;
  
  for(t=nt->t+4,i=0;i<6;i++,t+=2)
  {
    j=(i+5)%6;
    if((i%2)==0) // long side
    {      
      t[0].v[0]=i+6;
      t[0].v[1]=j+6;
      t[0].v[2]=i+18;
    
      t[1].v[0]=i+18;
      t[1].v[1]=j+6;
      t[1].v[2]=j+18;
    }
    else
    {
      t[0].v[0]=i+12;
      t[0].v[1]=j+12;
      t[0].v[2]=i+24;
    
      t[1].v[0]=i+24;
      t[1].v[1]=j+12;
      t[1].v[2]=j+24;
    }
  }  

/*
 * bottom
 */
  
  nt->t[16].v[0]=0+30;
  nt->t[16].v[1]=5+30;
  nt->t[16].v[2]=1+30;
  
  nt->t[17].v[0]=1+30;
  nt->t[17].v[1]=5+30;
  nt->t[17].v[2]=2+30;

  nt->t[18].v[0]=4+30;
  nt->t[18].v[1]=3+30;
  nt->t[18].v[2]=5+30;

  nt->t[19].v[0]=3+30;
  nt->t[19].v[1]=2+30;
  nt->t[19].v[2]=5+30;    

  return nt;
}

/***************************************************************************************
 *                                   RANDOM TRIAS                                      *
 *                                                                                     *
 ***************************************************************************************/
  
static int random_trias_action(node_stc *n)
{
  random_trias_stc *rt=(random_trias_stc *)n->nt->actiondata;
  
  float *fp;
  hv *hvp;
  tria_stc *t;
  int i,j,k;

  for(t=n->nt->t,fp=n->nt->coord_p,hvp=rt->vects,i=0;i<n->nt->n_t;i++,hvp++,t++)
    for(j=0;j<3;j++)
      for(k=0;k<3;k++,fp++)
	(*fp)=n->nt->p[t->v[j]].p[k]+(*hvp)[k]*rt->cur_pos;

  return ACTION_RETURN_FILL_UNIF_ARRAYS|ACTION_RETURN_REQUEST_NODE_SHADER_DATA_UPDATE|ACTION_RETURN_REFRESH; // update stuff
}

static void random_trias_config(node_tria_stc *nt,float triasize,hv boxdim,hv norm)
{
  random_trias_stc *rt=(random_trias_stc *)nt->actiondata;

  memcpy(rt->normal,norm,sizeof(hv));

  rt->triadim=triasize;
  rt->halfside=rt->triadim/2.0;
  rt->circumcircle_rad=rt->triadim/sqrtf(3.0);
  
/*
 * build zero-centered tria
 */

  static hv up={0.0,1.0,0.0};
  
  hm4 m;
  hv v;
  int i,j,k;

  bzero(rt->tria_vertices,sizeof(hv)*3);
  rt->tria_vertices[0][0]=rt->halfside;
  rt->tria_vertices[0][2]=rt->tria_vertices[2][2]=rt->circumcircle_rad/2.0;
  rt->tria_vertices[1][2]=-rt->circumcircle_rad;
  rt->tria_vertices[2][0]=-rt->halfside;

  mat4_vect_over_another(rt->normal,up,m);

  for(i=0;i<3;i++)
    mat4_vect_mult(m,rt->tria_vertices[i],rt->tria_vertices[i]);
		      
  point_stc *p;
  tria_stc *t;
  hv box={boxdim[0]-rt->halfside*2,boxdim[1]-rt->halfside*2,boxdim[2]-rt->halfside*2},offs;
  
  for(p=nt->p,t=nt->t,i=0;i<nt->n_t;i++,t++,p+=3)
  {
    t->prog=i;
    
    for(k=0;k<3;k++) // calc offset in box
      v[k]=rt->halfside+drand48()*box[k];
    
    for(j=0;j<3;j++) // three vertices
    {
      p[j].prog=i*3+j;
      for(k=0;k<3;k++) // xyz
	p[j].p[k]=rt->tria_vertices[j][k]+v[k];
      
      memcpy(p[j].n,rt->normal,sizeof(hv));

      t->v[j]=p[j].prog;
    }
    /* lg("TRIA %d: %.2f,%.2f,%.2f | %.2f,%.2f,%.2f | %.2f,%.2f,%.2f",i, */
    /*    p[0].p[0],p[0].p[1],p[0].p[2], */
    /*    p[1].p[0],p[1].p[1],p[1].p[2], */
    /*    p[2].p[0],p[2].p[1],p[2].p[2]);       */
  }
}

node_tria_stc *trias_random_trias(hv boxdim,int n_tria,float triadim,hv normal)
{
  node_tria_stc *nt=alloc_nt(n_tria*3,n_tria);

  nt->actionfunc=random_trias_action;
  nt->actiondata=malloc(sizeof(random_trias_stc));
  bzero(nt->actiondata,sizeof(random_trias_stc));  

  random_trias_stc *rt=(random_trias_stc *)nt->actiondata;

  rt->vects=malloc(sizeof(hv)*nt->n_t);

  random_trias_config(nt,triadim,boxdim,normal);

  return nt;
}

void random_trias_regenerate_trias(node_stc *n,float triasize,hv boxdim,hv norm)
{
  random_trias_config(n->nt,triasize,boxdim,norm);
}

void random_trias_load_run(node_stc *n,hv mainmotion,float randpct,hv randmotion[2])
{
  random_trias_stc *rt=(random_trias_stc *)n->nt->actiondata;  
  int nrand=(int)(n->nt->n_t*randpct),*locb=malloc(sizeof(int)*n->nt->n_t),i,j;
  
  random_reorder(n->nt->n_t,locb);

  for(i=0;i<n->nt->n_t;i++)
  {
    if(i<nrand)
      for(j=0;j<3;j++)
	rt->vects[locb[i]][j]=randmotion[0][j]+drand48()*(randmotion[1][j]-randmotion[0][j]);
    else
      memcpy(rt->vects[locb[i]],mainmotion,sizeof(hv));
  }
  
//for(i=0;i<rt->n_randmov;i++)
//  for(j=0;j<3;j++)
//    rt->randmov_vects[i][j]=drand48()*(randmove_per_sec*2.0)-randmove_per_sec;

  free(locb);
}

/***************************************************************************************
 *                                   TORUS                                             *
 *                                                                                     *
 ***************************************************************************************/

node_tria_stc *trias_torus(float bigradius,float smallradius,int n_refinements)
{
  int i,j,nr1=n_refinements+1,refsq=n_refinements*n_refinements,r1,r2,cnt,ncur1,ncur2,qp[4];
  float step=M_PI*2.0/n_refinements,a1,a2;
  node_tria_stc *nt=alloc_nt(refsq,refsq*2);
  hv v,vc={0.0,0.0,0.0},vv={0.0,1.0,0.0},vh={0.0,0.0,1.0},v1,v2;
  hm4 mt1,mt2,mr1,mr2,m1,m2,m3;
  point_stc *p;
  tria_stc *t;

  v[0]=bigradius;v[1]=0.0;v[2]=0.0;
  mat4_trasl(v,mt1);
  v[0]=0.0;v[1]=smallradius;v[2]=0.0;
  mat4_trasl(v,mt2);

  for(cnt=0,p=nt->p,t=nt->t,r1=0;r1<=n_refinements;r1++)
  {    
    ncur1=(r1+n_refinements)%n_refinements;
    
    mat4_rot_axis(vv,r1*step,mr1);
    mat4_mult(mt1,mr1,m1);    

    mat4_vect_mult(m1,vc,v1);
    
    for(r2=0;r2<=n_refinements;r2++)
    {
      ncur2=(r2+n_refinements)%n_refinements;
      
      if(r1<n_refinements && r2<n_refinements)
      {
	p->prog=cnt++;
	
	mat4_rot_axis(vh,r2*step,mr2);
	mat4_mult(mt2,mr2,m2);
	mat4_vect_mult(m2,vc,v2);
	
	mat4_mult(m2,m1,m3);
	mat4_vect_mult(m3,vc,p->p);
	
	vect_normalize(v2,v);
	mat4_vect_mult(mr1,v,p->n);

	p++;
      }
      
      if(r1>0 && r2>0)
      {
	qp[0]=ncur1*n_refinements+ncur2;
	qp[1]=(r1-1)*n_refinements+ncur2;
	qp[2]=(r1-1)*n_refinements+(r2-1);
	qp[3]=ncur1*n_refinements+(r2-1);

//	lg("%d/%d -> %d %d %d %d. About to tria %d",r1,r2,qp[0],qp[1],qp[2],qp[3],t-nt->t);
	
	t->prog=t-nt->t;
	t->v[0]=qp[0];
	t->v[1]=qp[1];
	t->v[2]=qp[2];
	t++;
	t->prog=t-nt->t;
	t->v[0]=qp[0];
	t->v[1]=qp[2];
	t->v[2]=qp[3];
	t++;
      }
//      lg("%d/%d: %.3f,%.3f,%.3f",r1,r2,p->p[0],p->p[1],p->p[2]);
    }
  }
//  lg("At end: %d points (%d created) %d trias (%d created)",p-nt->p,refsq,t-nt->t,refsq*2);

  return nt;
}

/***************************************************************************************
 *                                   SCREEN                                            *
 *                                                                                     *
 ***************************************************************************************/

/*
 * Two-sided, with texture & colour on both sides
 */

#define SCREEN_SIDE_DISTANCE 0.001

node_tria_stc *trias_screen(float w,float h)
{
  node_tria_stc *nt=alloc_nt(8,4);
  point_stc *p;
  tria_stc *t;
  int i;
  float wh=w/2.0,hh=h/2.0;
  hv nup={0.0,1.0,0.0},ndown={0.0,-1.0,0.0};

  for(p=nt->p,i=0;i<4;i++,p++)
  {
    p->prog=i;
    (p+4)->prog=i+4;
    
    p->p[1]=SCREEN_SIDE_DISTANCE;
    (p+4)->p[1]=-SCREEN_SIDE_DISTANCE;
    
    memcpy(p->n,nup,sizeof(hv));
    memcpy((p+4)->n,ndown,sizeof(hv));    
  }

  p=nt->p;
  p[0].p[0]=p[1].p[0]=p[4].p[0]=p[5].p[0]=-wh;
  p[2].p[0]=p[3].p[0]=p[6].p[0]=p[7].p[0]=wh;
  p[1].p[2]=p[2].p[2]=p[5].p[2]=p[6].p[2]=-hh;
  p[0].p[2]=p[3].p[2]=p[4].p[2]=p[7].p[2]=hh;

  p[2].tp[0]=p[3].tp[0]=p[0].tp[1]=p[3].tp[1]=1.0;
  p[4].tp[0]=p[5].tp[0]=p[4].tp[1]=p[7].tp[1]=1.0;
  
  t=nt->t;

  t->prog=0;
  t->v[0]=0;t->v[1]=2;t->v[2]=1;
  t++;

  t->prog=1;
  t->v[0]=0;t->v[1]=3;t->v[2]=2;
  t++;

  t->prog=2;
  t->v[0]=4;t->v[1]=5;t->v[2]=6;
  t++;

  t->prog=3;
  t->v[0]=4;t->v[1]=6;t->v[2]=7;

  return nt;
}

/***************************************************************************************
 *                          SPHERICAL SCREEN for 360deg movies                         *
 *                                                                                     *
 ***************************************************************************************/

static void fix_spheretria(node_tria_stc *ta,tria_stc *ts,
			   tria_stc **newtrias,int *n_newtrias,point_stc **newpoints,int *n_newpoints,uint8_t use_internal_surface)
{
//  ts->v[0]=ts->v[1]=ts->v[2]=0;
  int sums[3]={0,0,0},promemoria[3][3],cond,i;
  float v;

  for(i=0;i<3;i++)
  {
    v=ta->p[ts->v[i]].tp[0];
    if(v<=0.0)
    {
      lg("%s: NEVER SAW THIS HAPPENING ONCE IN MY TESTS!!!",__func__);
      abort();
    }
    if(v<0.5)
      cond=0;
    else if(v<1)
      cond=1;
    else
      cond=2;

    promemoria[cond][sums[cond]]=i;
    sums[cond]++;
  }

  if(sums[2]<1)
  {
    lg("%s: NEVER SAW THIS HAPPENING ONCE IN MY TESTS!!! [2]",__func__);
    abort();
  }

/*
 * Here we duplicate the point with sums 2, setting the texture pointer to 0.0
 */
  
  if(sums[0]==2 || (sums[0]==1 && sums[2]==2))
  {
    for(i=0;i<sums[2];i++)
    {
      (*newpoints)=realloc(*newpoints,sizeof(point_stc)*((*n_newpoints)+1));
      memcpy((*newpoints)+(*n_newpoints),ta->p+ts->v[promemoria[2][i]],sizeof(point_stc));
      ((*newpoints)+(*n_newpoints))->prog=(*n_newpoints);
      ((*newpoints)+(*n_newpoints))->tp[0]=0.0;
      ts->v[promemoria[2][i]]=ta->n_p+(*n_newpoints);
      (*n_newpoints)++;
    }
  }
  else if(sums[0]==1 && sums[1]==1)
  {
    hv centerpoint;

    vect_center(ta->p[ts->v[promemoria[0][0]]].p,ta->p[ts->v[promemoria[1][0]]].p,centerpoint);
    
    point_stc *c1,*c2,*othervertex;
    
    (*newpoints)=realloc(*newpoints,sizeof(point_stc)*((*n_newpoints)+3));

    c1=(*newpoints)+(*n_newpoints);
    c1->prog=ta->n_p+(*n_newpoints);
    c2=(*newpoints)+(*n_newpoints)+1;
    c2->prog=ta->n_p+(*n_newpoints)+1;
    othervertex=(*newpoints)+(*n_newpoints)+2;
    memcpy(othervertex,ta->p+ts->v[promemoria[2][0]],sizeof(point_stc));
    othervertex->prog=ta->n_p+(*n_newpoints)+2;
    (*n_newpoints)+=3;     

    memcpy(c1->p,centerpoint,sizeof(hv));
    vect_normalize(c1->p,c1->n);
    memcpy(c2->p,centerpoint,sizeof(hv));
    vect_normalize(c2->p,c2->n);
    if(use_internal_surface)
    {
      vect_negate(c1->n,c1->n);
      vect_negate(c2->n,c2->n);
    }
    
    c1->tp[0]=0.0;
    c2->tp[0]=1.0;    
    c1->tp[1]=c2->tp[1]=ta->p[ts->v[promemoria[0][0]]].tp[1];
    
    othervertex->tp[0]=0.0;
    
/*
 * Now create the new triangle
 */

    tria_stc *tria;
    
    (*newtrias)=realloc(*newtrias,sizeof(tria_stc)*((*n_newtrias)+1));
    tria=(*newtrias)+(*n_newtrias);
    tria->prog=ta->n_t+(*n_newtrias);    
    (*n_newtrias)++;

/*
 * New tria at the leftmost of texture.
 */

    tria->v[promemoria[0][0]]=ts->v[promemoria[0][0]];
    tria->v[promemoria[1][0]]=c1->prog;
    tria->v[promemoria[2][0]]=othervertex->prog;
    
    ts->v[promemoria[0][0]]=c2->prog;

//    lg("Tria 1: p 
  }
//  else
//    ts->v[0]=ts->v[1]=ts->v[2]=0;   
}

node_tria_stc *trias_sphscreen(float radius,int n_refinements,uint8_t use_internal_surface)
{
  node_tria_stc *ta=trias_sphere(radius,n_refinements);
  tria_stc *ts;
  point_stc *ps;
  int i,j;

/*
 * If internal surface required, invert the triangles' point order and the normals
 */

  if(use_internal_surface)
  {
    uint32_t tmp;
    
    for(ps=ta->p,i=0;i<ta->n_p;i++,ps++)
      vect_negate(ps->n,ps->n);
    for(ts=ta->t,i=0;i<ta->n_t;i++,ts++)
    {
      tmp=ts->v[1];
      ts->v[1]=ts->v[2];
      ts->v[2]=tmp;
    }
  }

/*
 * Fix triangles that span vertical margin
 */

  float phi[3],theta[3];
  tria_stc *newtrias=NULL;
  point_stc *newpoints=NULL;
  int n_newtrias=0,n_newpoints=0;
    
  for(ts=ta->t,i=0;i<ta->n_t;i++,ts++)
  {
//    for(j=0;j<3;j++)
//    {
//      ps=ta->p+ts->v[j];
//      phi[j]=(fabsf(ps->p[0])<0.00001) ? 0.0 : atanf(ps->p[2]/ps->p[0]);
//      theta[j]=acosf(ps->p[1]/radius);
//    }

//    if(phi[0]==0.0 || phi[1]==0.0 || phi[2]==0.0)
    if(fabsf(ta->p[ts->v[0]].tp[0]-ta->p[ts->v[1]].tp[0])+
       fabsf(ta->p[ts->v[0]].tp[0]-ta->p[ts->v[2]].tp[0])+
       fabsf(ta->p[ts->v[1]].tp[0]-ta->p[ts->v[2]].tp[0])>0.8)
      fix_spheretria(ta,ts,&newtrias,&n_newtrias,&newpoints,&n_newpoints,use_internal_surface);
    
/*     { */
/*       lg("Triangle #%d: phi %6.2f,%6.2f,%6.2f theta %6.2f,%6.2f,%6.2f xcoo %6.2f,%6.2f,%6.2f txx %6.2f,%6.2f,%6.2f",i, */
/* 	 phi[0]*180.0/M_PI,phi[1]*180.0/M_PI,phi[2]*180.0/M_PI, */
/* 	 theta[0]*180.0/M_PI,theta[1]*180.0/M_PI,theta[2]*180.0/M_PI, */
/* 	 ta->p[ts->v[0]].p[0]/radius,ta->p[ts->v[1]].p[0]/radius,ta->p[ts->v[2]].p[0]/radius, */
/* 	 ta->p[ts->v[0]].tp[0],ta->p[ts->v[1]].tp[0],ta->p[ts->v[2]].tp[0]); */
/* //      ta->p[ts->v[0]].tp[0]=ta->p[ts->v[0]].tp[1]= */
/* //      ta->p[ts->v[1]].tp[0]=ta->p[ts->v[1]].tp[1]= */
/* //	ta->p[ts->v[2]].tp[0]=ta->p[ts->v[2]].tp[1]=0.0; */
/*       ts->v[0]=ts->v[1]=ts->v[2]=0; */
/*     } */
  }
  if(n_newpoints>0)
  {
    ta->p=realloc(ta->p,sizeof(point_stc)*(ta->n_p+n_newpoints));
    memcpy(ta->p+ta->n_p,newpoints,sizeof(point_stc)*n_newpoints);
    ta->n_p+=n_newpoints;
    free(newpoints);
  }
  if(n_newtrias>0)
  {
    ta->t=realloc(ta->t,sizeof(tria_stc)*(ta->n_t+n_newtrias));
    memcpy(ta->t+ta->n_t,newtrias,sizeof(tria_stc)*n_newtrias);
    ta->n_t+=n_newtrias;
    free(newtrias);
  }

  return ta;
}

/***************************************************************************************
 *                                    misc                                             *
 *                                                                                     *
 ***************************************************************************************/

node_tria_stc *alloc_nt(int n_p,int n_t)
{
  node_tria_stc *nt=malloc(sizeof(node_tria_stc));

  bzero(nt,sizeof(node_tria_stc));

  nt->n_p=n_p;
  nt->n_t=n_t;

  nt->p=malloc(sizeof(point_stc)*nt->n_p);
  bzero(nt->p,sizeof(point_stc)*nt->n_p);
  nt->t=malloc(sizeof(tria_stc)*nt->n_t);
  bzero(nt->t,sizeof(tria_stc)*nt->n_t);
  
  return nt;
}

void add_tria_with_normal(node_tria_stc *nt,int n_p,int n_t,hv v1,hv v2,hv v3)
{
  hv a[3],normal;
  point_stc *p;
  int i;

//  lg("TVN parte con %.2f,%.2f,%.2f %.2f,%.2f,%.2f %.2f,%.2f,%.2f",
//     v1[0],v1[1],v1[2],
//     v2[0],v2[1],v2[2],
//     v3[0],v3[1],v3[2]);
  
  memcpy(a[0],v1,sizeof(hv));
  memcpy(a[1],v2,sizeof(hv));
  memcpy(a[2],v3,sizeof(hv));
  
  vect_three_points_normal(a,normal);

  for(p=nt->p+n_p,i=0;i<3;i++,p++)
  {
    p->prog=i+n_p;
    memcpy(p->p,a[i],sizeof(hv));
    
    memcpy(p->n,normal,sizeof(hv));
//    lg("P%d prende N  %.2f,%.2f,%.2f",p-nt->p,p->n[0],p->n[1],p->n[2]);
  }  
  
  tria_stc *t=nt->t+n_t;

  t->prog=n_t;
  t->v[0]=n_p;
  t->v[1]=n_p+1;
  t->v[2]=n_p+2;
}

static void free_single_node(node_stc *n)
{
  free(n->name);

  if(n->nt)
  {
    free(n->nt->p);
    free(n->nt->t);
    free(n->nt);
  }
  free(n->subnodes);
  free(n->disabled_flgs);
  free(n);
}
  
void free_node_data(glengine_stc *s)
{
  node_stc *n;
  int i;

  for(i=0;i<s->n_nodes;i++)
    free_single_node(s->nodes[i]);
  free(s->nodes);
  free(s->root_nodes);  
}

static node_stc *new_node(glengine_stc *s)
{
  node_stc *n=malloc(sizeof(node_stc));
  
  bzero(n,sizeof(node_stc));
  n->n_texture=-1;
  n->prog=s->n_nodes;
  mat4_unit(n->mat);
  
  s->nodes=realloc(s->nodes,sizeof(node_stc *)*(s->n_nodes+1));
  s->nodes[s->n_nodes++]=n;
  
  return n;
}
  
node_stc *new_node_as_subnode(glengine_stc *s,node_stc *parent)
{
  node_stc *n=new_node(s);
  
  n->parent=parent;
  parent->subnodes=realloc(parent->subnodes,sizeof(node_stc *)*(parent->n_subnodes+1));
  parent->subnodes[parent->n_subnodes++]=n;

  return n;
}

node_stc *new_node_as_root_node(glengine_stc *s)
{
  node_stc *n=new_node(s);
  
  s->root_nodes=realloc(s->root_nodes,sizeof(node_stc *)*(s->n_root_nodes+1));
  s->root_nodes[s->n_root_nodes++]=n;

  return n;
}

