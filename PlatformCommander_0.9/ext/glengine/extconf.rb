# extconf.rb


=begin

***--MANAGED--***

==========================================================================
Copyright (C) 2020 Carlo Prelz, University of Bern

carlo.prelz@humdek.unibe.ch

This file is part of Platform Commander.

Platform Commander is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Platform Commander is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
==========================================================================

Extension builder

=end

require 'mkmf'

$CFLAGS+=" -g -O6 -funsigned-char -fPIC -ffast-math -Werror -Wall -Wcast-align "+
         "-Wno-unused-variable -Wno-unused-but-set-variable -Wno-unused-function "+
         "-Wno-stringop-overflow -Wno-unused-result -Wno-int-to-pointer-cast"

#
# For address sanitizer, run as
# LD_PRELOAD=/usr/lib/gcc/x86_64-linux-gnu/9/libasan.so ruby scripts/<script>
#

#$CFLAGS+=' -O1 -g -fsanitize=\'address\' -fsanitize=\'undefined\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'address\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'leak\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'thread\' -fno-omit-frame-pointer'
#$CFLAGS+=' -O1 -g -fsanitize=\'undefined\' -fno-omit-frame-pointer'

$LOCAL_LIBS=['xcb','X11-xcb','xcb-ewmh','GLEW','GLU','freetype','cairo','GLX','X11','hidapi-libusb'].map do |s|
  '-l'+s
end.join(' ')

#$LOCAL_LIBS='/usr/lib/gcc/x86_64-linux-gnu/9/libasan.so '+$LOCAL_LIBS

$INCFLAGS+=' '+['../carlobase/','../maths/','/usr/include/cairo/','/usr/include/freetype2/','/usr/local/include/hidapi/'].map do |s|
  '-I'+s
end.join(' ')

create_makefile("glengine")
