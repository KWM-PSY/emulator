/* glengine.h */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Gl engine class include file
 */

/* glengine.h */

#ifndef GLENGINE_H
#define GLENGINE_H

#define __EXPORTED_HEADERS__

#include <maths_include.h>
#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <complex.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/time.h>
#include <linux/types.h>
#include <GL/glew.h>
#include <GL/glx.h>
#include <pthread.h>

#define ACTION_RETURN_REBUILD_NODE_MATS 1
#define ACTION_RETURN_FILL_UNIF_ARRAYS 2
#define ACTION_RETURN_REQUEST_NODE_SHADER_DATA_UPDATE 4
#define ACTION_RETURN_REFRESH 8

typedef enum
{
  GLT_COMPAT=0xFFF0,
  GLT_MOREMODERN,
} engine_type_enum;
  
typedef enum
{
  GLN_PLACEHOLDER,
  GLN_LIGHT,
  GLN_TETRAHEDRON,
  GLN_CYLINDER,
  GLN_CONE,
  GLN_SPHERE,
  GLN_PLY,
  GLN_MOOG_PLATE,
  GLN_RANDOM_TRIAS,
  GLN_TORUS,
  GLN_SCREEN,
  GLN_SPHSCREEN,
  GLN_MAX // MUST BE LAST!
} node_type_enum;

typedef struct
{
  int prog,w,h,size;
  uint32_t *lpixbfr,*rpixbfr;
  uint8_t request_shader_data_update;
} texture_stc;
  
typedef struct
{
  uint32_t prog;
  hv p,n; // point, normal
  hv2 tp; // texture pos
} point_stc;

typedef struct
{
  uint32_t col[3]; // amb,spec,diff
  float shine;
} col_stc;

typedef struct
{
  uint32_t prog,v[3];
} tria_stc;

typedef struct node
{
  node_type_enum type;
  
  uint32_t prog;
  char *name;
  uint8_t *disabled_flgs,request_shader_data_update;
  
  struct node_tria *nt;
  int32_t tp; // texture prog - neg=col
  col_stc c;
  
  int n_subnodes,n_texture;
  struct node *parent,**subnodes;

  hv attach_offset;
  hm4 mat_at_start,mat,accmat;
} node_stc;

typedef struct node_tria
{
  int n_p,n_t;
  point_stc *p;
  tria_stc *t;

  int (*actionfunc)(node_stc *n);
  void *actiondata;

  float *coord_p,*normal_p,*texture_p,*node_p;
} node_tria_stc;

typedef struct
{
  struct camera *c;
  uint8_t right_flg;
  GLuint warp_program,fbo,color_tex,depth_tex,quad_data;
  GLint attr_coord,attr_tex,unif_warp_factor,unif_scale_factor;
} hmd_per_cam_stc;

/*
 * Important to remember: this structure is used to save data to pass to modern shaders.
 * It has nothing to do with my higher-level nodes (node_stc)
 * Or rather - one of these is allocated for each of my higher-level nodes, but
 * it is used only to prepare data for the shaders
 */

typedef struct
{
  int32_t valid,texture,g1,g2;
  hm4 m;
  hv4 amb,diff,spec,shine;
} moremodern_node_stc;
  
typedef struct camera
{
  char *name,*dpy_name;
  int progr,x,y,w,h,vers[2];
  uint8_t decor,exposed,shader_loaded;
  hv position,look,up;
  hm4 vp_mat[2];
  float ratio,fovy,near,far;

  struct hmd *hmd_p;
  hmd_per_cam_stc *hmdpc_p;
  
  Display *d;
  Window wnd;
  GLXContext ctx;

  int *node_valid;

  GLuint program,triangle_data,*textures,ssbo_nodes,index_ssbo_nodes;
  GLint unif_vp_mat,unif_light_pos,unif_light_col,unif_light_range,
    unif_node_valid,unif_node_m,unif_node_texture,unif_node_amb,unif_node_diff,unif_node_spec,unif_node_shine,*unif_textures, // for compat
    attr_point_coord,attr_point_normal,attr_point_texture,attr_point_node;

  pthread_mutex_t mtx;
} camera_stc;

typedef struct
{
  engine_type_enum type;
  
  char *name,*logbase;

  unsigned long long int poll_interval;
  struct timespec next_poll;

  hv bkg_col,position;  
  hv4 cur_quat;
  uint8_t cameras_uploaded,request_redraw;
  
  int n_textures,n_hmds,n_cameras,n_lights,n_nodes,n_root_nodes,n_tot_trias;
  texture_stc **textures;
  camera_stc **cameras;
  node_stc **nodes,**root_nodes;

  struct pollfd *pfds;
  
  unsigned long stuff_size;
  void *stuff;
  float *coord_p,*normal_p,*texture_p,*node_p;
  uint8_t *must_update_texture;

  moremodern_node_stc *moremodern_nodes;

  hv *light_pos,*light_col;
  float *light_range;

  hm4 *node_m;
  int *node_texture;
  hv *node_amb,*node_diff,*node_spec;
  float *node_shine;

  void *refresh_log_tag;  
  int refresh_logname_unit,refresh_log_tag_len;
  
  uint8_t exit_thread;
  pthread_t thr;
  pthread_mutex_t mtx;
} glengine_stc;

/***************************************************************************
 * PER NODE TYPE                                                           *
 ***************************************************************************/

/*
 * Random trias
 */

typedef struct
{
  hv normal,tria_vertices[3],*vects;
  float triadim,halfside,circumcircle_rad,cur_pos;
} random_trias_stc;

extern void hexprint(char *ptr,int len);
extern void timeadd(struct timespec *t1,unsigned long long int usecs);
extern long long int timediff(struct timespec *t1,struct timespec *t2);
extern uint8_t timepast(struct timespec *t1,struct timespec *t2);
extern void lg(const char *fmt,...);

extern node_tria_stc *trias_tetrahedron(float size);
extern node_tria_stc *trias_cylinder(float radius,float len,int n_refinements);
extern node_tria_stc *trias_cone(float radius,float len,int n_refinements);
extern node_tria_stc *trias_sphere(float radius,int n_refinements);
extern node_tria_stc *trias_moog_plate(float len1,float len2,float thick,float rotang);
extern node_tria_stc *trias_random_trias(hv boxdim,int n_tria,float triadim,hv normal);
extern node_tria_stc *trias_torus(float bigradius,float smallradius,int n_refinements);
extern node_tria_stc *trias_screen(float w,float h);
extern node_tria_stc *trias_sphscreen(float radius,int n_refinements,uint8_t use_internal_surface);
extern node_tria_stc *alloc_nt(int n_p,int n_t);
extern void add_tria_with_normal(node_tria_stc *nt,int n_p,int n_t,hv v1,hv v2,hv v3);
extern void free_node_data(glengine_stc *s);
extern node_stc *new_node_as_subnode(glengine_stc *s,node_stc *parent);
extern node_stc *new_node_as_root_node(glengine_stc *s);

extern void random_trias_regenerate_trias(node_stc *n,float triasize,hv boxdim,hv norm);
extern void random_trias_load_run(node_stc *n,hv mainmotion,float randpct,hv randmotion[2]);

#endif

