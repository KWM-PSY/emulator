/* init.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Gl engine class: function declarations and extension initialization
 */

#include "glengine.h"
#include <ctype.h>

VALUE cls_glengine;

extern VALUE glengine_img_24to32(VALUE self,VALUE v_pixels);
extern VALUE glengine_img_vflip(VALUE self,VALUE v_pixels,VALUE v_cols);

extern VALUE new_glengine(VALUE self,VALUE v_type,VALUE v_bkg_col,VALUE v_lights,VALUE v_refresh_logname);
extern VALUE glengine_stop_thr(VALUE self);
extern VALUE glengine_redraw(VALUE self);
extern VALUE glengine_set_bkg_col(VALUE self,VALUE v_rgb);
extern VALUE glengine_start_logging(VALUE self,VALUE v_logbase);
extern VALUE glengine_add_texture(VALUE self,VALUE v_w,VALUE v_h,VALUE v_lpixels,VALUE v_rpixels);
extern VALUE glengine_add_camera(VALUE self,VALUE v_name,VALUE v_dpy_name,VALUE v_coords,VALUE v_decor,
				 VALUE v_fovy,VALUE v_near,VALUE v_far,VALUE v_last_camera);
extern VALUE glengine_add_node(VALUE self,VALUE v_type,VALUE v_node_attach,VALUE v_h);
extern VALUE glengine_node_set_texture(VALUE self,VALUE v_node_id,VALUE v_txt_id);
extern VALUE glengine_node_set_col(VALUE self,VALUE v_node_id,VALUE v_col);
extern VALUE glengine_wait_for_expose(VALUE self);
extern VALUE glengine_load_shaders(VALUE self,VALUE v_sh_v,VALUE v_sh_f);
extern VALUE glengine_orient_camera(VALUE self,VALUE v_camera_id,VALUE v_pos,VALUE v_quat,VALUE v_distance);
extern VALUE glengine_orient_camera_mat(VALUE self,VALUE v_camera_id,VALUE v_pos,VALUE v_mat,VALUE v_distance);
extern VALUE glengine_activate_node(VALUE self,VALUE v_camera_id,VALUE v_node_id,VALUE v_active_flg);
extern VALUE glengine_position_node(VALUE self,VALUE v_node_id,VALUE v_mat);
extern VALUE glengine_movrot_node(VALUE self,VALUE v_node_id,VALUE v_x,VALUE v_y,VALUE v_z,VALUE v_yank,VALUE v_pitch,VALUE v_roll);
extern VALUE glengine_posquat_node(VALUE self,VALUE v_node_id,VALUE v_pos,VALUE v_quat);
extern VALUE glengine_random_trias_regenerate_trias(VALUE self,VALUE v_node_id,VALUE v_boxdim,VALUE v_triasize,VALUE v_norm,VALUE v_colors,VALUE v_shininess);
extern VALUE glengine_random_trias_load_run(VALUE self,VALUE v_node_id,VALUE v_mainmotion,VALUE v_randpct,VALUE v_randmotion);
extern VALUE glengine_random_trias_position_run(VALUE self,VALUE v_node_id,VALUE v_pos);
extern VALUE glengine_request_redraw(VALUE self);
extern VALUE glengine_add_log_tag(VALUE self,VALUE v_tag);
extern VALUE glengine_texture_update_pixels(VALUE self,VALUE v_txt_id,VALUE v_lptr,VALUE v_rptr);

void Init_glengine(void)
{
  VALUE v;
  char **ptr;
  
  cls_glengine=rb_define_class("Glengine",rb_cObject);

  rb_define_const(cls_glengine,"T_COMPAT",INT2FIX(GLT_COMPAT));
  rb_define_const(cls_glengine,"T_MOREMODERN",INT2FIX(GLT_MOREMODERN));
  
  rb_define_const(cls_glengine,"N_PLACEHOLDER",INT2FIX(GLN_PLACEHOLDER));
  rb_define_const(cls_glengine,"N_TETRAHEDRON",INT2FIX(GLN_TETRAHEDRON));
  rb_define_const(cls_glengine,"N_CYLINDER",INT2FIX(GLN_CYLINDER));
  rb_define_const(cls_glengine,"N_CONE",INT2FIX(GLN_CONE));
  rb_define_const(cls_glengine,"N_SPHERE",INT2FIX(GLN_SPHERE));
  rb_define_const(cls_glengine,"N_PLY",INT2FIX(GLN_PLY));
  rb_define_const(cls_glengine,"N_MOOG_PLATE",INT2FIX(GLN_MOOG_PLATE));
  rb_define_const(cls_glengine,"N_RANDOM_TRIAS",INT2FIX(GLN_RANDOM_TRIAS));
  rb_define_const(cls_glengine,"N_TORUS",INT2FIX(GLN_TORUS));
  rb_define_const(cls_glengine,"N_SCREEN",INT2FIX(GLN_SCREEN));
  rb_define_const(cls_glengine,"N_SPHSCREEN",INT2FIX(GLN_SPHSCREEN));
  
  rb_define_singleton_method(cls_glengine,"img_24to32",glengine_img_24to32,1);
  rb_define_singleton_method(cls_glengine,"img_vflip",glengine_img_vflip,2);
  
  rb_define_singleton_method(cls_glengine,"new",new_glengine,4);
  rb_define_method(cls_glengine,"stop_thr",glengine_stop_thr,0);
  rb_define_method(cls_glengine,"redraw",glengine_redraw,0);
  rb_define_method(cls_glengine,"set_bkg_col",glengine_set_bkg_col,1);
  rb_define_method(cls_glengine,"start_logging",glengine_start_logging,1);
  rb_define_method(cls_glengine,"add_texture",glengine_add_texture,4);
  rb_define_method(cls_glengine,"add_camera",glengine_add_camera,8);
  rb_define_method(cls_glengine,"add_node",glengine_add_node,3);
  rb_define_method(cls_glengine,"node_set_texture",glengine_node_set_texture,2);
  rb_define_method(cls_glengine,"node_set_col",glengine_node_set_col,2);
  rb_define_method(cls_glengine,"wait_for_expose",glengine_wait_for_expose,0);
  rb_define_method(cls_glengine,"load_shaders",glengine_load_shaders,2);
  rb_define_method(cls_glengine,"orient_camera",glengine_orient_camera,4);
  rb_define_method(cls_glengine,"orient_camera_mat",glengine_orient_camera_mat,4);
  rb_define_method(cls_glengine,"activate_node",glengine_activate_node,3);
  rb_define_method(cls_glengine,"position_node",glengine_position_node,2);
  rb_define_method(cls_glengine,"movrot_node",glengine_movrot_node,7);
  rb_define_method(cls_glengine,"posquat_node",glengine_posquat_node,3);
  rb_define_method(cls_glengine,"random_trias_regenerate_trias",glengine_random_trias_regenerate_trias,6);
  rb_define_method(cls_glengine,"random_trias_load_run",glengine_random_trias_load_run,4);
  rb_define_method(cls_glengine,"random_trias_position_run",glengine_random_trias_position_run,2);
  rb_define_method(cls_glengine,"request_redraw",glengine_request_redraw,0);
  rb_define_method(cls_glengine,"add_log_tag",glengine_add_log_tag,1);
  rb_define_method(cls_glengine,"texture_update_pixels",glengine_texture_update_pixels,3);
}
