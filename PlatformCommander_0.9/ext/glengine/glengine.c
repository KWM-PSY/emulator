/* dist_glengine.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Gl engine: bulk of the code
 */

#include "glengine.h"

#include <poll.h>
#include <X11/Xlib.h>
#include <GL/gl.h>
#include <GL/glu.h>

#define NAME "Glengine"

extern VALUE cls_glengine;

static void free_glengine(void *p);

static size_t glengine_memsize(const void *p)
{
  return sizeof(glengine_stc);
}

const rb_data_type_t glengine_dtype={"glengine",
				     {0,free_glengine,glengine_memsize,},
				     0,0,
				     RUBY_TYPED_FREE_IMMEDIATELY,};

static void stop_thr(glengine_stc *s);
static void compute_matrices(glengine_stc *s,camera_stc *c);
static void add_camera(glengine_stc *s,camera_stc *c,char *name,char *dpy_name,
		       int x,int y,int w,int h,uint8_t decor);
static void *displayer(void *arg);
static void gler(char *file,int line);
static char *shader_log(GLuint object);
static void process_event(glengine_stc *s);
static void prepare_stuff(glengine_stc *s);
static void fill_node_stuff(node_stc *n);
static void fill_unif_arrays(glengine_stc *s);
static void fill_node_unif_arrays(glengine_stc *s,node_stc *n);
static void update_enabled_node_sitn(glengine_stc *s);
static void update_node_accmats(glengine_stc *s);
static void calc_accmat_recurse(node_stc *n,hm4 in_mat);
static void propagate_node_cond(glengine_stc *s,node_stc *n,int camera_id,uint8_t cond);
static GLuint compile_glsl_program(camera_stc *c,char *sh_v,char *sh_f);
static void request_node_shader_data_update(glengine_stc *s,node_stc *n);

static const hv up_vector={0.0,1.0,0.0},forw_lookvector={0.0,0.0,-1.0},def_viewp={0.0,0.0,200.0},def_lookat={0.0,0.0,0.0};

VALUE new_glengine(VALUE self,VALUE v_type,VALUE v_bkg_col,VALUE v_lights,VALUE v_refresh_logname)
{
  glengine_stc *s;
  VALUE sdata=TypedData_Make_Struct(cls_glengine,glengine_stc,&glengine_dtype,s),v;
  int i,sts;
  
  bzero(s,sizeof(glengine_stc));

  if(v_refresh_logname!=Qnil)
  {
    s->refresh_logname_unit=open(RSTRING_PTR(v_refresh_logname),O_WRONLY|O_TRUNC|O_CREAT,0666);
    if(s->refresh_logname_unit<0)
      rb_raise(rb_eArgError,"%s: could not open refresh log [%s] (%s)",__func__,RSTRING_PTR(v_refresh_logname),strerror(errno));
  }
  else
    s->refresh_logname_unit=-1;
  
  lg("XInitTHreads returns %d",XInitThreads());

  s->type=FIX2INT(v_type);  

  for(i=0;i<3;i++)
    s->bkg_col[i]=FIX2INT(rb_ary_entry(v_bkg_col,i))/255.0;

  s->n_lights=RARRAY_LEN(v_lights);

  s->light_pos=malloc(sizeof(hv)*s->n_lights);
  s->light_col=malloc(sizeof(hv)*s->n_lights);
  s->light_range=malloc(sizeof(float)*s->n_lights);
  
  for(i=0;i<s->n_lights;i++)
  {
    v=rb_ary_entry(v_lights,i);
    s->light_pos[i][0]=NUM2DBL(rb_ary_entry(v,0));
    s->light_pos[i][1]=NUM2DBL(rb_ary_entry(v,1));
    s->light_pos[i][2]=NUM2DBL(rb_ary_entry(v,2));
    s->light_col[i][0]=FIX2INT(rb_ary_entry(v,3))/255.0;
    s->light_col[i][1]=FIX2INT(rb_ary_entry(v,4))/255.0;
    s->light_col[i][2]=FIX2INT(rb_ary_entry(v,5))/255.0;
    s->light_range[i]=NUM2DBL(rb_ary_entry(v,6));
    /* lg("LI#%d: pos %.2f,%.2f,%.2f col %.2f,%.2f,%.2f ra %.2f",i, */
    /*    s->light_pos[i][0],s->light_pos[i][1],s->light_pos[i][2], */
    /*    s->light_col[i][0],s->light_col[i][1],s->light_col[i][2],s->light_range[i]); */
  }

  s->exit_thread=0;

  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  sts=pthread_create(&s->thr,NULL,displayer,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting display thread (%s)",__func__,sts,strerror(errno));

  return sdata;
}

static void free_glengine(void *p)
{
  glengine_stc *s=(glengine_stc *)p;

  stop_thr(s);

  free(s);
}

VALUE glengine_redraw(VALUE self)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);
  
  s->request_redraw=1;
  return self;
}

VALUE glengine_set_bkg_col(VALUE self,VALUE v_rgb)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int i;

  for(i=0;i<3;i++)
    s->bkg_col[i]=FIX2INT(rb_ary_entry(v_rgb,i))/255.0;

  s->request_redraw=1;
  return self;
}

VALUE glengine_stop_thr(VALUE self)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  stop_thr(s);

  return self;
}

VALUE glengine_start_logging(VALUE self,VALUE v_logbase)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  s->logbase=strdup(RSTRING_PTR(v_logbase));
  
  return self;
}

VALUE glengine_add_texture(VALUE self,VALUE v_w,VALUE v_h,VALUE v_lpixels,VALUE v_rpixels)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  s->textures=realloc(s->textures,sizeof(texture_stc *)*(s->n_textures+1));
  s->textures[s->n_textures]=malloc(sizeof(texture_stc));

  s->must_update_texture=realloc(s->must_update_texture,sizeof(uint8_t)*s->n_textures+1);  

  texture_stc *t=s->textures[s->n_textures];

  t->prog=s->n_textures++;

  t->w=FIX2INT(v_w);
  t->h=FIX2INT(v_h);
  t->size=sizeof(uint32_t)*t->w*t->h;

  if(v_lpixels==Qnil)
    t->lpixbfr=NULL;
  else
  {
    if(RSTRING_LEN(v_lpixels)!=t->size)
      rb_raise(rb_eArgError,"%s: bad lpixbfr size (is %ld, should be %d)",__func__,RSTRING_LEN(v_lpixels),t->size);

    t->lpixbfr=malloc(t->size);
    memcpy(t->lpixbfr,RSTRING_PTR(v_lpixels),t->size);
  }
  if(v_rpixels==Qnil)
    t->rpixbfr=NULL;
  else
  {
    if(RSTRING_LEN(v_rpixels)!=t->size)
      rb_raise(rb_eArgError,"%s: bad rpixbfr size (is %ld, should be %d)",__func__,RSTRING_LEN(v_rpixels),t->size);

    t->rpixbfr=malloc(t->size);
    memcpy(t->rpixbfr,RSTRING_PTR(v_rpixels),t->size);
  }
  
  return INT2FIX(t->prog);
}
  
VALUE glengine_add_camera(VALUE self,VALUE v_name,VALUE v_dpy_name,VALUE v_coords,VALUE v_decor,
			  VALUE v_fovy,VALUE v_near,VALUE v_far,VALUE v_last_camera)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  if(s->cameras_uploaded)
    rb_raise(rb_eArgError,"%s: trying to create camera when running!",__func__);

  s->cameras=realloc(s->cameras,sizeof(camera_stc *)*(s->n_cameras+1));  
  s->pfds=realloc(s->pfds,sizeof(struct pollfd)*(s->n_cameras+1));

  camera_stc *c=malloc(sizeof(camera_stc));
  bzero(c,sizeof(camera_stc));
  c->progr=s->n_cameras++;
  s->cameras[c->progr]=c;

  add_camera(s,c,RSTRING_PTR(v_name),RSTRING_PTR(v_dpy_name),
	     FIX2INT(rb_ary_entry(v_coords,0)),
	     FIX2INT(rb_ary_entry(v_coords,1)),
	     FIX2INT(rb_ary_entry(v_coords,2)),
	     FIX2INT(rb_ary_entry(v_coords,3)),
	     (v_decor==Qnil ||  v_decor==Qfalse) ? 0 : 1);

  c->fovy=NUM2DBL(v_fovy);
  c->near=NUM2DBL(v_near);
  c->far=NUM2DBL(v_far);

  memcpy(c->up,up_vector,sizeof(hv));
  memcpy(c->position,def_viewp,sizeof(hv));
  memcpy(c->look,def_lookat,sizeof(hv));

  compute_matrices(s,c);

  if(v_last_camera==Qtrue)
  {
    lg("==> ENDED UPLOADING CAMERAS");
    prepare_stuff(s);
    s->cameras_uploaded=1;
  }

  return INT2FIX(c->progr);
}

VALUE glengine_add_node(VALUE self,VALUE v_type,VALUE v_node_attach,VALUE v_h)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  if(s->cameras_uploaded)
    rb_raise(rb_eArgError,"%s: Nodes must be loaded before cameras!",__func__);    
  
  node_type_enum t=FIX2INT(v_type);
  int i;

  if(t<0 || t>=GLN_MAX)
    rb_raise(rb_eArgError,"%s: Bad node type (%d)",__func__,t);
  
  node_stc *n;

  if(v_node_attach==Qnil)
    n=new_node_as_root_node(s);
  else
  {
    int node_attach=FIX2INT(v_node_attach);

    if(node_attach<0 || node_attach>=s->n_nodes)
      rb_raise(rb_eArgError,"%s: Bad attach node ID (%d)",__func__,node_attach);
    n=new_node_as_subnode(s,s->nodes[node_attach]);
  }

  VALUE v=rb_hash_aref(v_h,ID2SYM(rb_intern("name")));
  n->name=strdup(RSTRING_PTR(v));

  v=rb_hash_aref(v_h,ID2SYM(rb_intern("mat")));
  if(v!=Qnil)
    memcpy(n->mat,RSTRING_PTR(v),sizeof(hm4));
  else
    mat4_unit(n->mat);  

  v=rb_hash_aref(v_h,ID2SYM(rb_intern("offset")));
  for(i=0;i<3;i++)
    n->attach_offset[i]=NUM2DBL(rb_ary_entry(v,i));  

  n->type=t;
  
  switch(n->type)
  {
  case GLN_PLACEHOLDER:
    break;
  case GLN_TETRAHEDRON:
    v=rb_hash_aref(v_h,ID2SYM(rb_intern("size")));
    n->nt=trias_tetrahedron(NUM2DBL(v));
    break;
  case GLN_CYLINDER:
  {
    float radius=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("radius"))));
    float len=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("length"))));
    int n_refi=FIX2INT(rb_hash_aref(v_h,ID2SYM(rb_intern("refinements"))));
    n->nt=trias_cylinder(radius,len,n_refi);
  }
  break;    
  case GLN_CONE:
  {
    float radius=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("radius"))));
    float len=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("length"))));
    int n_refi=FIX2INT(rb_hash_aref(v_h,ID2SYM(rb_intern("refinements"))));
    n->nt=trias_cone(radius,len,n_refi);
  }
  break;    
  case GLN_SPHERE:
  {
    float radius=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("radius"))));
    int n_refi=FIX2INT(rb_hash_aref(v_h,ID2SYM(rb_intern("refinements"))));
    n->nt=trias_sphere(radius,n_refi);
  }
  break;    
  case GLN_MOOG_PLATE:
  {
    float len1=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("len1"))));
    float len2=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("len2"))));
    float thick=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("thickness"))));

    v=rb_hash_aref(v_h,ID2SYM(rb_intern("rot_angle")));
    
    float rotang=v==Qnil ? 0.0 : NUM2DBL(v);    

    n->nt=trias_moog_plate(len1,len2,thick,rotang);
  }
  break;    
  case GLN_RANDOM_TRIAS:
  {
    int n_tria=FIX2INT(rb_hash_aref(v_h,ID2SYM(rb_intern("n_tria"))));
    float triadim=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("tria_dimension"))));
    hv boxdim,normal;

    memcpy(boxdim,RSTRING_PTR(rb_hash_aref(v_h,ID2SYM(rb_intern("box_dimension")))),sizeof(hv));
    memcpy(normal,RSTRING_PTR(rb_hash_aref(v_h,ID2SYM(rb_intern("normal")))),sizeof(hv));

    n->nt=trias_random_trias(boxdim,n_tria,triadim,normal);
  }
  break;    
  case GLN_TORUS:
  {
    float bradius=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("bigradius"))));
    float sradius=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("smallradius"))));
    int n_refi=FIX2INT(rb_hash_aref(v_h,ID2SYM(rb_intern("refinements"))));
    n->nt=trias_torus(bradius,sradius,n_refi);
  }
  break;    
  case GLN_SCREEN:
  {
    float w=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("w"))));
    float h=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("h"))));
    n->nt=trias_screen(w,h);
  }
  break;    
  case GLN_SPHSCREEN:
  {
    float radius=NUM2DBL(rb_hash_aref(v_h,ID2SYM(rb_intern("radius"))));
    int n_refi=FIX2INT(rb_hash_aref(v_h,ID2SYM(rb_intern("refinements"))));
    uint8_t use_int_surf=rb_hash_aref(v_h,ID2SYM(rb_intern("use_internal_surface")))==Qtrue ? 1 : 0;

    n->nt=trias_sphscreen(radius,n_refi,use_int_surf);
  }
  break;    
  default:
    rb_raise(rb_eArgError,"%s: Type %d is not handled!",__func__,t);
  }

  if(n->nt && n->nt->n_t>0)
  {
    tria_stc *tp;
    
    v=rb_hash_aref(v_h,ID2SYM(rb_intern("col")));
    if(v!=Qnil)
    {
      n->n_texture=-1;

      for(i=0;i<3;i++)
	n->c.col[i]=FIX2INT(rb_ary_entry(v,i));
//      lg("Name %s: col %6.6x %6.6x %6.6x",n->name,n->c.col[0],n->c.col[1],n->c.col[2]);
      n->c.shine=NUM2DBL(rb_ary_entry(v,3));
    }
    else
    {
      v=rb_hash_aref(v_h,ID2SYM(rb_intern("texture")));
      if(v!=Qnil)
      {
	i=FIX2INT(v);
	if(i<0 || i>=s->n_textures)
	  rb_raise(rb_eArgError,"%s: bad texture id (%d)",__func__,i);

	n->n_texture=i;
      }
      else
	rb_raise(rb_eArgError,"%s: Node %s: 'col' or 'texture' must be defined!",__func__,n->name);
    }

#if 0
    for(tp=n->nt->t,i=0;i<n->nt->n_t;i++,tp++)
      lg("N %s tr %d: %.2f,%.2f,%.2f | %.2f,%.2f,%.2f | %.2f,%.2f,%.2f",
	 n->name,i,
	 n->nt->p[tp->v[0]].p[0],n->nt->p[tp->v[0]].p[1],n->nt->p[tp->v[0]].p[2],
	 n->nt->p[tp->v[1]].p[0],n->nt->p[tp->v[1]].p[1],n->nt->p[tp->v[1]].p[2],
	 n->nt->p[tp->v[2]].p[0],n->nt->p[tp->v[2]].p[1],n->nt->p[tp->v[2]].p[2]);
#endif    
  }
  
  return INT2FIX(n->prog);
}

VALUE glengine_node_set_texture(VALUE self,VALUE v_node_id,VALUE v_txt_id)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int node_id=FIX2INT(v_node_id);
  
  if(node_id<0 || node_id>=s->n_nodes)
    rb_raise(rb_eArgError,"%s: bad node id (%d)",__func__,node_id);

  node_stc *n=s->nodes[node_id];  
  int i=FIX2INT(v_txt_id);
  
  if(i<0 || i>=s->n_textures)
    rb_raise(rb_eArgError,"%s: bad texture id (%d)",__func__,i);
  
  n->n_texture=i;
  fill_node_unif_arrays(s,n);
  request_node_shader_data_update(s,n);

  return self;
}

VALUE glengine_node_set_col(VALUE self,VALUE v_node_id,VALUE v_col)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int node_id=FIX2INT(v_node_id);
  
  if(node_id<0 || node_id>=s->n_nodes)
    rb_raise(rb_eArgError,"%s: bad node id (%d)",__func__,node_id);

  node_stc *n=s->nodes[node_id];
  int i;

  n->n_texture=-1;
  for(i=0;i<3;i++)
    n->c.col[i]=FIX2INT(rb_ary_entry(v_col,i));
  n->c.shine=NUM2DBL(rb_ary_entry(v_col,3));
  fill_node_unif_arrays(s,n);
  request_node_shader_data_update(s,n);
  
  return self;
}

VALUE glengine_wait_for_expose(VALUE self)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int tot_p=0,tot_t=0,i;
  node_stc *n;

/*
 * Wait for cameras to be exposed
 */

  camera_stc *c;
  uint8_t flg;
  
  while(1)
  {
    flg=1;
    for(i=0;i<s->n_cameras;i++)
      if(!s->cameras[i]->exposed)
	flg=0;
    if(flg==1)
      break;
    usleep(10000);
  }

  for(i=0;i<s->n_nodes;i++)
  {
    n=s->nodes[i];
    if(n->nt)
    {      
      tot_p+=n->nt->n_p;
      tot_t+=n->nt->n_t;
    }
  }    
  
  VALUE to_ret=rb_ary_new_capa(4);

  rb_ary_store(to_ret,0,INT2FIX(s->n_nodes));
  rb_ary_store(to_ret,1,INT2FIX(tot_p));
  rb_ary_store(to_ret,2,INT2FIX(tot_t));
  rb_ary_store(to_ret,3,INT2FIX(s->n_textures));

  return to_ret;
}

VALUE glengine_load_shaders(VALUE self,VALUE v_sh_v,VALUE v_sh_f)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  char *sh_v=RSTRING_PTR(v_sh_v),*sh_f=RSTRING_PTR(v_sh_f);
//  lg("V shader:\n%s",sh_v);
//  lg("F shader:\n%s",sh_f);
  
  texture_stc *t;
  camera_stc *c;
  int i,j;
  char bfr[256];
  uint32_t *pixbfr;
  
  for(i=0;i<s->n_cameras;i++)
  {
    c=s->cameras[i];

    if(!c->exposed)
      rb_raise(rb_eArgError,"%s: Trying to set shader for cam. %s, but is not exposed!",__func__,c->name);

    gler(__FILE__,__LINE__);
    glXMakeCurrent(c->d,None,NULL);
    gler(__FILE__,__LINE__);

    c->program=compile_glsl_program(c,sh_v,sh_f);
    
    pthread_mutex_lock(&c->mtx);   
    
    glXMakeCurrent(c->d,c->wnd,c->ctx);
    
    c->unif_vp_mat=glGetUniformLocation(c->program,"vp_mat");

    c->unif_light_pos=glGetUniformLocation(c->program,"light_pos");
    c->unif_light_col=glGetUniformLocation(c->program,"light_col");
    c->unif_light_range=glGetUniformLocation(c->program,"light_range");
      
    c->attr_point_node=glGetAttribLocation(c->program,"point_node");
    c->attr_point_coord=glGetAttribLocation(c->program,"point_coord");
    c->attr_point_normal=glGetAttribLocation(c->program,"point_normal");
    if(s->n_textures>0)
      c->attr_point_texture=glGetAttribLocation(c->program,"point_texture");
    
    c->unif_textures=malloc(sizeof(GLint)*s->n_textures);
    c->textures=malloc(sizeof(GLuint)*s->n_textures);
    for(j=0;j<s->n_textures;j++)
    {
      t=s->textures[j];

      pixbfr=(c->hmdpc_p && c->hmdpc_p->right_flg) ? t->rpixbfr : t->lpixbfr;
      if(!pixbfr)
	continue;      
      
      glActiveTexture(GL_TEXTURE0+j);
      
      sprintf(bfr,"texture%3.3d",j+1);      
      c->unif_textures[j]=glGetUniformLocation(c->program,bfr);
      
      glGenTextures(1,c->textures+j);
      glBindTexture(GL_TEXTURE_2D,c->textures[j]);
      glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
      glTexImage2D(GL_TEXTURE_2D, // target
		   0,  // level, 0 = base, no minimap,
		   GL_RGBA, // internalformat
		   t->w,  // width
		   t->h,  // height
		   0,  // border, always 0 in OpenGL ES
		   GL_RGBA,  // format
		   GL_UNSIGNED_BYTE, // type
		   pixbfr);  
    }

    switch(s->type)
    {
    case GLT_COMPAT:      
      c->unif_node_valid=glGetUniformLocation(c->program,"node_valid");
      c->unif_node_m=glGetUniformLocation(c->program,"node_m");
      c->unif_node_texture=glGetUniformLocation(c->program,"node_texture");
      c->unif_node_amb=glGetUniformLocation(c->program,"node_amb");
      c->unif_node_diff=glGetUniformLocation(c->program,"node_diff");
      c->unif_node_spec=glGetUniformLocation(c->program,"node_spec");
      c->unif_node_shine=glGetUniformLocation(c->program,"node_shine");
      break;
    case GLT_MOREMODERN:
      glGenBuffers(1,&c->ssbo_nodes);
      glBindBufferBase(GL_SHADER_STORAGE_BUFFER,0,c->ssbo_nodes);
      glBufferData(GL_SHADER_STORAGE_BUFFER,sizeof(moremodern_node_stc)*s->n_nodes,s->moremodern_nodes,GL_DYNAMIC_COPY);

      c->index_ssbo_nodes=glGetProgramResourceIndex(c->program,GL_SHADER_STORAGE_BLOCK,"nodes");
      glShaderStorageBlockBinding(c->program,c->index_ssbo_nodes,0);
      break;
    }

    glXMakeCurrent(c->d,None,NULL);
    c->shader_loaded=1;
    pthread_mutex_unlock(&c->mtx);
    lg("Cam. %s: shaders loaded & linked",c->name);
  }  
  update_enabled_node_sitn(s);
  s->request_redraw=1;

  return self;
}


VALUE glengine_orient_camera(VALUE self,VALUE v_camera_id,VALUE v_pos,VALUE v_quat,VALUE v_distance)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int camera_id=FIX2INT(v_camera_id);

  if(camera_id<0 || camera_id>=s->n_cameras)
    rb_raise(rb_eArgError,"%s: bad camera id (%d)",__func__,camera_id);

  camera_stc *c=s->cameras[camera_id];

//  pthread_mutex_lock(&c->mtx);
  
  memcpy(c->position,RSTRING_PTR(v_pos),sizeof(hv));

  hv v1,v2;
  hv4 quat;

  memcpy(quat,RSTRING_PTR(v_quat),sizeof(hv4));
  rot_vect_by_quat(quat,up_vector,c->up);
  rot_vect_by_quat(quat,forw_lookvector,v1);
  
  vect_mult_const(v1,NUM2DBL(v_distance),v2);
  vect_add(v2,c->position,c->look);
  /* lg("AAYYAA pos %.3f,%.3f,%.3f look %.3f,%.3f,%.3f up %.3f,%.3f,%.3f v1 %.3f,%.3f,%.3f", */
  /*    c->position[0],c->position[1],c->position[2], */
  /*    c->look[0],c->look[1],c->look[2], */
  /*    c->up[0],c->up[1],c->up[2], */
  /*    v1[0],v1[1],v1[2]); */
  
  compute_matrices(s,c);
  
  s->request_redraw=1;
//  pthread_mutex_unlock(&c->mtx);
  
  return self;
}

VALUE glengine_orient_camera_mat(VALUE self,VALUE v_camera_id,VALUE v_pos,VALUE v_mat,VALUE v_distance)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int camera_id=FIX2INT(v_camera_id);

  if(camera_id<0 || camera_id>=s->n_cameras)
    rb_raise(rb_eArgError,"%s: bad camera id (%d)",__func__,camera_id);

  camera_stc *c=s->cameras[camera_id];

  pthread_mutex_lock(&c->mtx);

  memcpy(c->position,RSTRING_PTR(v_pos),sizeof(hv));

  hv v1,v2;
  hm4 mat;

  memcpy(mat,RSTRING_PTR(v_mat),sizeof(hm4));
  mat4_vect_mult(mat,up_vector,c->up);
  mat4_vect_mult(mat,forw_lookvector,v1);
  vect_mult_const(v1,NUM2DBL(v_distance),v2);
  vect_add(v2,c->position,c->look);

  /* lg("ZZYYZZ pos %.3f,%.3f,%.3f look %.3f,%.3f,%.3f up %.3f,%.3f,%.3f v1 %.3f,%.3f,%.3f", */
  /*    c->position[0],c->position[1],c->position[2], */
  /*    c->look[0],c->look[1],c->look[2], */
  /*    c->up[0],c->up[1],c->up[2], */
  /*    v1[0],v1[1],v1[2]); */
  
  pthread_mutex_lock(&s->mtx);
  compute_matrices(s,c);

  pthread_mutex_unlock(&s->mtx);
  pthread_mutex_unlock(&c->mtx);

  s->request_redraw=1;
  return self;
}

VALUE glengine_activate_node(VALUE self,VALUE v_camera_ids,VALUE v_node_id,VALUE v_active_flg)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);
  
  int node_id=FIX2INT(v_node_id),camera_id,i,cond=(v_active_flg==Qtrue) ? 0 : 1;
  
  if(node_id<0 || node_id>=s->n_nodes)
    rb_raise(rb_eArgError,"%s: bad node id (%d)",__func__,node_id);

  if(v_camera_ids==Qnil)    
    propagate_node_cond(s,s->nodes[node_id],-1,cond);
  else
    for(i=0;i<RARRAY_LEN(v_camera_ids);i++)
    {
      camera_id=FIX2INT(rb_ary_entry(v_camera_ids,i));
      if(camera_id<0 || camera_id>=s->n_cameras)
	rb_raise(rb_eArgError,"%s: bad camera id (%d)",__func__,camera_id);
      propagate_node_cond(s,s->nodes[node_id],camera_id,cond);    
    }
  
  update_enabled_node_sitn(s);
  s->request_redraw=1;
    
  return self;
}

VALUE glengine_position_node(VALUE self,VALUE v_node_id,VALUE v_mat)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);
  
  int node_id=FIX2INT(v_node_id);
  
  if(node_id<0 || node_id>=s->n_nodes)
    rb_raise(rb_eArgError,"%s: bad node id (%d)",__func__,node_id);

//  pthread_mutex_lock(&s->mtx);
  memcpy(s->nodes[node_id]->mat,RSTRING_PTR(v_mat),sizeof(hm4));

//  mat4_print(s->nodes[node_id]->name,s->nodes[node_id]->mat);

  update_node_accmats(s);  
  s->request_redraw=1;
//  pthread_mutex_unlock(&s->mtx);

  return self;
}

VALUE glengine_movrot_node(VALUE self,VALUE v_node_id,VALUE v_x,VALUE v_y,VALUE v_z,VALUE v_yank,VALUE v_pitch,VALUE v_roll)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);
  
  int node_id=FIX2INT(v_node_id);
  
  if(node_id<0 || node_id>=s->n_nodes)
    rb_raise(rb_eArgError,"%s: bad node id (%d)",__func__,node_id);

  node_stc *n=s->nodes[node_id];

  float x=v_x!=Qnil ? NUM2DBL(v_x) : n->mat[3][0],
    y=v_y!=Qnil ? NUM2DBL(v_y) : n->mat[3][1],
    z=v_z!=Qnil ? NUM2DBL(v_z) : n->mat[3][2],
    yank=NUM2DBL(v_yank),pitch=NUM2DBL(v_pitch),roll=NUM2DBL(v_roll);
  
  hv ypr={yank,pitch,roll};
  
//  pthread_mutex_lock(&s->mtx);
  mat4_tait_bryan(ypr,n->mat);
  n->mat[3][0]=x;
  n->mat[3][1]=y;
  n->mat[3][2]=z;

  update_node_accmats(s);  
  s->request_redraw=1;
//  pthread_mutex_unlock(&s->mtx);

  return self;
}

VALUE glengine_posquat_node(VALUE self,VALUE v_node_id,VALUE v_pos,VALUE v_quat)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int node_id=FIX2INT(v_node_id);

  if(node_id<0 || node_id>=s->n_nodes)    
    rb_raise(rb_eArgError,"%s: Bad node id! (%d).",__func__,node_id);

  node_stc *n=s->nodes[node_id];
  hv4 quat;

  memcpy(quat,RSTRING_PTR(v_quat),sizeof(hv4));
  quat_to_hm4(quat,n->mat);
  memcpy(n->mat[3],RSTRING_PTR(v_pos),sizeof(hv));

  update_node_accmats(s);  
  s->request_redraw=1;
  return self;
}

VALUE glengine_random_trias_regenerate_trias(VALUE self,VALUE v_node_id,VALUE v_boxdim,VALUE v_triasize,VALUE v_norm,VALUE v_colors,VALUE v_shininess)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int node_id=FIX2INT(v_node_id);

  if(node_id<0 || node_id>=s->n_nodes)    
    rb_raise(rb_eArgError,"%s: Bad node id! (%d).",__func__,node_id);

  node_stc *n=s->nodes[node_id];
  int i,j;
  hv boxdim,norm;
  float triasize=NUM2DBL(v_triasize);

  memcpy(boxdim,RSTRING_PTR(v_boxdim),sizeof(hv));
  memcpy(norm,RSTRING_PTR(v_norm),sizeof(hv));

  random_trias_regenerate_trias(n,triasize,boxdim,norm);

  for(i=0;i<3;i++)
    n->c.col[i]=FIX2INT(rb_ary_entry(v_colors,i));
  n->c.shine=NUM2DBL(v_shininess);  

  fill_node_stuff(n);
  fill_node_unif_arrays(s,n);
  request_node_shader_data_update(s,n);  
  
  return self;
}

VALUE glengine_random_trias_load_run(VALUE self,VALUE v_node_id,VALUE v_mainmotion,VALUE v_randpct,VALUE v_randmotion)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);
  
  int node_id=FIX2INT(v_node_id);
  
  if(node_id<0 || node_id>=s->n_nodes || s->nodes[node_id]->type!=GLN_RANDOM_TRIAS)
    rb_raise(rb_eArgError,"%s: bad node id (%d)",__func__,node_id);

  node_stc *n=s->nodes[node_id];
  random_trias_stc *rt=(random_trias_stc *)n->nt->actiondata;

  float randpct=NUM2DBL(v_randpct);
  hv mainmotion,randmotion[2];
  
  memcpy(mainmotion,RSTRING_PTR(v_mainmotion),sizeof(hv));
  memcpy(randmotion[0],RSTRING_PTR(rb_ary_entry(v_randmotion,0)),sizeof(hv));
  memcpy(randmotion[1],RSTRING_PTR(rb_ary_entry(v_randmotion,1)),sizeof(hv));

  random_trias_load_run(n,mainmotion,randpct,randmotion);

  return self;
}
  
VALUE glengine_random_trias_position_run(VALUE self,VALUE v_node_id,VALUE v_pos)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);
  
  int node_id=FIX2INT(v_node_id);
  
  if(node_id<0 || node_id>=s->n_nodes || s->nodes[node_id]->type!=GLN_RANDOM_TRIAS)
    rb_raise(rb_eArgError,"%s: bad node id (%d)",__func__,node_id);

  node_stc *n=s->nodes[node_id];
  random_trias_stc *rt=(random_trias_stc *)n->nt->actiondata;

  rt->cur_pos=NUM2DBL(v_pos);

  return self;
}

VALUE glengine_request_redraw(VALUE self)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  s->request_redraw=1;
  return self;
}

VALUE glengine_add_log_tag(VALUE self,VALUE v_tag)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  if(s->refresh_logname_unit<=0)
  {
    lg("%s Warning: you are trying to add a log tag, but you did not pass a log file name when initializing!",__func__);
    return Qfalse;
  }  

  pthread_mutex_lock(&s->mtx);
  s->refresh_log_tag_len=RSTRING_LEN(v_tag);
  s->refresh_log_tag=realloc(s->refresh_log_tag,s->refresh_log_tag_len);
  memcpy(s->refresh_log_tag,RSTRING_PTR(v_tag),s->refresh_log_tag_len);
  pthread_mutex_unlock(&s->mtx);
//  hexprint(s->refresh_log_tag,s->refresh_log_tag_len);

  return self;
}

VALUE glengine_texture_update_pixels(VALUE self,VALUE v_txt_id,VALUE v_lptr,VALUE v_rptr)
{
  glengine_stc *s;  
  TypedData_Get_Struct(self,glengine_stc,&glengine_dtype,s);

  int txt_id=FIX2INT(v_txt_id),i;
  
  if(txt_id<0 || txt_id>=s->n_textures)
    rb_raise(rb_eArgError,"%s: bad texture id (%d)",__func__,txt_id);

  texture_stc *t=s->textures[txt_id];
  uint64_t ptr;
  uint8_t *bfrptr;
  
  if(v_lptr!=Qnil && t->lpixbfr)
  {
    memcpy((char *)&ptr,RSTRING_PTR(v_lptr),sizeof(uint64_t));
    bfrptr=(uint8_t *)ptr;
    memcpy(t->lpixbfr,bfrptr,t->size);
  }
  if(v_rptr!=Qnil && t->rpixbfr)
  {
    memcpy((char *)&ptr,RSTRING_PTR(v_rptr),sizeof(uint64_t));
    bfrptr=(uint8_t *)ptr;
    memcpy(t->rpixbfr,bfrptr,t->size);
  }
  
  t->request_shader_data_update=1;
  s->request_redraw=1;

  return self;
}

VALUE glengine_img_24to32(VALUE self,VALUE v_pixels)
{
  int n_pixels=RSTRING_LEN(v_pixels)/3,i;
  uint8_t *bfr=malloc(n_pixels*4),*ptr2,*ptr1;

  bzero(bfr,n_pixels*4);
  
  for(ptr1=(uint8_t *)RSTRING_PTR(v_pixels),ptr2=bfr,i=0;i<n_pixels;i++,ptr2+=4,ptr1+=3)
    memcpy(ptr2,ptr1,3);
  
  VALUE to_ret=rb_str_new((char *)bfr,n_pixels*4);

  free(bfr);
  return to_ret;  
}

VALUE glengine_img_vflip(VALUE self,VALUE v_pixels,VALUE v_w)
{
  int w=FIX2INT(v_w),linesize=w*3,h=RSTRING_LEN(v_pixels)/linesize,i;
  uint8_t *pix=(uint8_t *)RSTRING_PTR(v_pixels),*pout,*new_pix=malloc(linesize*w);

  for(pout=new_pix,i=0;i<h;i++,pout+=linesize)
    memcpy(pout,pix+(linesize*(h-i-1)),linesize);

  VALUE to_ret=rb_str_new((char *)new_pix,linesize*w);

  free(new_pix);
  return to_ret;
}
  
static void stop_thr(glengine_stc *s)
{
  if(s->exit_thread)
    return;

  if(s->refresh_logname_unit>0)
  {
    close(s->refresh_logname_unit);
    s->refresh_logname_unit=-1;
  }  

  texture_stc *t;
  camera_stc *c;
  node_stc *n;
  int i;

  s->exit_thread=1;
  
  pthread_join(s->thr,NULL);

  pthread_mutex_lock(&s->mtx);
  for(i=0;i<s->n_textures;i++)
  {
    t=s->textures[i];

    free(t->rpixbfr);
    free(t->lpixbfr);
    free(t);
  }
  free(s->textures);
  free(s->must_update_texture);
  
  for(i=0;i<s->n_cameras;i++)
  {
    c=s->cameras[i];

    pthread_mutex_lock(&c->mtx);
    glXMakeCurrent(c->d,None,NULL);
    glXDestroyContext(c->d,c->ctx);
    XDestroyWindow(c->d,c->wnd);
    XCloseDisplay(c->d);
    pthread_mutex_unlock(&c->mtx);

    free(c->name);
    free(c->dpy_name);
    free(c->node_valid);
    free(c);
  }
  free(s->cameras);

  free_node_data(s);
  
  free(s->pfds);
  free(s->stuff);

  free(s->node_m);
  free(s->node_texture);
  free(s->node_amb);
  free(s->node_diff);
  free(s->node_spec);
  free(s->node_shine);

  free(s->moremodern_nodes);

  free(s->logbase);
  free(s->name);
  
  pthread_mutex_unlock(&s->mtx);
}

static void compute_matrices(glengine_stc *s,camera_stc *c)
{
  mat4_lookat(c->position,c->look,c->up,c->vp_mat[0]);
  mat4_perspective(c->fovy,c->ratio,c->near,c->far,c->vp_mat[1]);
}


static void add_camera(glengine_stc *s,camera_stc *c,char *name,char *dpy_name,
		       int x,int y,int w,int h,uint8_t decor)
{
  c->name=strdup(name);
  c->dpy_name=strdup(dpy_name);
    
  c->x=x;
  c->y=y;
  c->w=w;
  c->h=h;
  c->decor=decor;
  
  c->ratio=(float)c->w/(float)c->h;

  pthread_mutex_init(&c->mtx,NULL);

  c->d=XOpenDisplay(c->dpy_name);
  if(!c->d)
  {
    lg("%s: Cannot open X display %s!",__func__,c->dpy_name);
    abort();
  }

  int attribs[64],n_attribs=0;

  attribs[n_attribs++]=GLX_RGBA;
  attribs[n_attribs++]=GLX_DOUBLEBUFFER;
  attribs[n_attribs++]=GLX_RED_SIZE;
  attribs[n_attribs++]=1;
  attribs[n_attribs++]=GLX_GREEN_SIZE;
  attribs[n_attribs++]=1;
  attribs[n_attribs++]=GLX_BLUE_SIZE;
  attribs[n_attribs++]=1;
  attribs[n_attribs++]=GLX_DEPTH_SIZE;
  attribs[n_attribs++]=1;
  attribs[n_attribs++]=None;

  int scrnum=DefaultScreen(c->d);
  Window root=RootWindow(c->d,scrnum);
  XVisualInfo *visinfo=glXChooseVisual(c->d,scrnum,attribs);

  if(!visinfo)
  {
    lg("%s: glXChooseVisual failed (scrnum %d, root %d)",__func__,scrnum,root);
    abort();
  }

  XSetWindowAttributes attr;
  
  attr.override_redirect=True; // only used without decor
  attr.background_pixel=0;
  attr.border_pixel=0;
  attr.colormap=XCreateColormap(c->d,root,visinfo->visual,AllocNone);
  attr.event_mask=StructureNotifyMask|ExposureMask|KeyPressMask;

  unsigned long mask=CWBackPixel|CWBorderPixel|CWColormap|CWEventMask;

  if(!c->decor)
    mask|=CWOverrideRedirect;
  c->wnd=XCreateWindow(c->d,root,c->x,c->y,c->w,c->h,0,visinfo->depth,InputOutput,visinfo->visual,mask,&attr);

  {
    XSizeHints size_hints;
    char name[256];
    sprintf(name,"%s_%d",c->name,getpid());
	  
    size_hints.x=c->x;
    size_hints.y=c->y;
    size_hints.width=c->w;
    size_hints.height=c->h;
    size_hints.flags=USSize|USPosition;
    XSetNormalHints(c->d,c->wnd,&size_hints);
    XSetStandardProperties(c->d,c->wnd,name,name,None,(char **)NULL,0,&size_hints);
  }
  
  c->ctx=glXCreateContext(c->d,visinfo,NULL,True);
  if(!c->ctx)
  {
    lg("%s: Error creating window context!",__func__);
    abort();
  }
  
  XFree(visinfo);
  XMapWindow(c->d,c->wnd);

  s->pfds[c->progr].fd=XConnectionNumber(c->d);
  s->pfds[c->progr].events=POLLIN;
  s->pfds[c->progr].revents=0;

  c->program=-1;
}

static void *displayer(void *arg)
{
  glengine_stc *s=(glengine_stc *)arg;
  texture_stc *t;
  camera_stc *c;
  node_stc *n;
  GLenum glew_status=-1;
  int i,j,k;
  char *log;
  float *fptr;
  struct timespec now;
  uint32_t *pixbfr;

  while(!s->exit_thread)
  {    
    if(!s->cameras_uploaded)
    {
      usleep(3000);
      continue;
    }
    if(s->cameras_uploaded==1)
    {
      GLuint sn;
      GLint res;
      
      for(i=0;i<s->n_cameras;i++)
      {
	c=s->cameras[i];
	lg("Exposing camera %s",c->name);

	if(c->exposed)
	{
	  lg("Strange: doubly exposing camera %s",c->name);
	  continue;
	}

      	pthread_mutex_lock(&c->mtx);

	glXMakeCurrent(c->d,c->wnd,c->ctx);
	gler(__FILE__,__LINE__);

	glViewport(0,0,c->w,c->h);

	gler(__FILE__,__LINE__);
	if(glew_status!=GLEW_OK)
	{
	  glewExperimental=GL_TRUE;
	  glew_status=glewInit();
	  if(glew_status!=GLEW_OK)
	  {
	    lg("%s: Error accessing glew (%s)!",__func__,glewGetErrorString(glew_status));
	    abort();
	  }
	}
//     	glGetIntegerv(GL_MAJOR_VERSION,&c->vers[0]);
//	glGetIntegerv(GL_MINOR_VERSION,&c->vers[1]);
	gler(__FILE__,__LINE__);
	
	glGenBuffers(1,&(c->triangle_data));
//	lg("<%s> running on GL version %d.%d. Tridata at %d",c->name,c->vers[0],c->vers[1],c->triangle_data);
	
	glBindBuffer(GL_ARRAY_BUFFER,c->triangle_data);
	glBufferData(GL_ARRAY_BUFFER,s->stuff_size,s->stuff,GL_DYNAMIC_DRAW);

	c->exposed=1;
	gler(__FILE__,__LINE__);
	glXMakeCurrent(c->d,None,NULL);
	pthread_mutex_unlock(&c->mtx);
      }	  
      
      s->cameras_uploaded=2;
    }

    int lres,res=0;
    
    for(i=0;i<s->n_nodes;i++)
    {
      n=s->nodes[i];
      
      if(n->nt && n->nt->actionfunc)
      {
	lres=n->nt->actionfunc(n);
	if(lres&ACTION_RETURN_REBUILD_NODE_MATS)
	  update_node_accmats(s);  
	if(lres&ACTION_RETURN_FILL_UNIF_ARRAYS)
	  fill_node_unif_arrays(s,n);
	if(lres&ACTION_RETURN_REQUEST_NODE_SHADER_DATA_UPDATE)
	  request_node_shader_data_update(s,n);
	res|=lres;
      }      
    }
    if(res&ACTION_RETURN_REFRESH)
      s->request_redraw=1;
    
    process_event(s);
    
    clock_gettime(CLOCK_MONOTONIC,&now);

    if(s->request_redraw)
    {
      gler(__FILE__,__LINE__);
      for(i=0;i<s->n_textures;i++)
      {
	s->must_update_texture[i]=s->textures[i]->request_shader_data_update;	
	s->textures[i]->request_shader_data_update=0;
      }
      
      pthread_mutex_lock(&s->mtx);
      s->request_redraw=0;
      
      GLvoid *p=NULL;
      
      for(i=0;i<s->n_cameras;i++)
      {
	gler(__FILE__,__LINE__);
	c=s->cameras[i];
	
	if(!c->shader_loaded)
	  continue;

	pthread_mutex_lock(&c->mtx);

	glXMakeCurrent(c->d,c->wnd,c->ctx);	
	gler(__FILE__,__LINE__);

/*
 * If any shader data sync was requested, do that here
 */
    
	glBindBuffer(GL_UNIFORM_BUFFER,c->triangle_data);
	for(j=0;j<s->n_nodes;j++)
	{
	  n=s->nodes[j];
	  if(n->request_shader_data_update)
	  {
	    int datasizes[4]={sizeof(float)*n->nt->n_p*3,sizeof(float)*n->nt->n_p*3,sizeof(float)*n->nt->n_p*2,sizeof(float)*n->nt->n_p};
	    float *pointers[4]={n->nt->coord_p,n->nt->normal_p,n->nt->texture_p,n->nt->node_p};  

	    for(k=0;k<4;k++)
	      glBufferSubData(GL_UNIFORM_BUFFER,(void *)pointers[k]-s->stuff,datasizes[k],pointers[k]);
	  }
	}
	glBindBuffer(GL_UNIFORM_BUFFER,0);
	
	if(c->hmdpc_p)
	  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT,c->hmdpc_p->fbo);

	glClearColor(s->bkg_col[0],s->bkg_col[1],s->bkg_col[2],1.0);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	gler(__FILE__,__LINE__);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);  
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	gler(__FILE__,__LINE__);
	glUseProgram(c->program);

#if 0	
	if(s->n_textures>0)
	{
	  glEnable(GL_TEXTURE_2D);
	  for(j=0;j<s->n_textures;j++)
	  {
	    t=s->textures[j];	    
	    glActiveTexture(GL_TEXTURE0+j);

	    glBindTexture(GL_TEXTURE_2D,c->textures[j]);
	  }
	}
#else	
#endif	
	
	glUniformMatrix4fv(c->unif_vp_mat,2,GL_FALSE,(const float *)c->vp_mat);
	
	glUniform3fv(c->unif_light_pos,s->n_lights,(const GLfloat *)s->light_pos);
	glUniform3fv(c->unif_light_col,s->n_lights,(const GLfloat *)s->light_col);	
	glUniform1fv(c->unif_light_range,s->n_lights,s->light_range);

	gler(__FILE__,__LINE__);
	switch(s->type)
	{
	case GLT_COMPAT:      
	  glUniform1iv(c->unif_node_valid,s->n_nodes,(const GLint *)c->node_valid);
	  glUniformMatrix4fv(c->unif_node_m,s->n_nodes,GL_FALSE,(const GLfloat *)s->node_m);
	  glUniform1iv(c->unif_node_texture,s->n_nodes,(const GLint *)s->node_texture);
	  glUniform3fv(c->unif_node_amb,s->n_nodes,(const GLfloat *)s->node_amb);
	  glUniform3fv(c->unif_node_diff,s->n_nodes,(const GLfloat *)s->node_diff);
	  glUniform3fv(c->unif_node_spec,s->n_nodes,(const GLfloat *)s->node_spec);
	  glUniform1fv(c->unif_node_shine,s->n_nodes,(const GLfloat *)s->node_shine);
	  break;
	case GLT_MOREMODERN:
	  for(j=0;j<s->n_nodes;j++)
	    s->moremodern_nodes[j].valid=c->node_valid[j];
	  
	  gler(__FILE__,__LINE__);
	  glBindBuffer(GL_SHADER_STORAGE_BUFFER,c->ssbo_nodes);

	  memcpy(glMapBuffer(GL_SHADER_STORAGE_BUFFER,GL_WRITE_ONLY),s->moremodern_nodes,sizeof(moremodern_node_stc)*s->n_nodes);	  
	  glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

	  gler(__FILE__,__LINE__);
//	  glShaderStorageBlockBinding(c->program,c->index_ssbo_nodes,/***NUMERO IN SHADER binding= - quindi,*/2);
	  break;
	}
	
	gler(__FILE__,__LINE__);

	/* for(fptr=s->coord_p,j=0;j<s->n_tot_trias;j++,fptr+=9) */
	/*   lg("%s-> tria %d %.2f,%.2f,%.2f | %.2f,%.2f,%.2f | %.2f,%.2f,%.2f",c->name,j, */
	/*      fptr[0],fptr[1],fptr[2], */
	/*      fptr[3],fptr[4],fptr[5], */
	/*      fptr[6],fptr[7],fptr[8]); */
	
	glBindBuffer(GL_ARRAY_BUFFER,c->triangle_data);
//	glBufferData(GL_ARRAY_BUFFER,s->stuff_size,s->stuff,GL_DYNAMIC_DRAW);
	
	gler(__FILE__,__LINE__);
	glEnableVertexAttribArray(c->attr_point_coord);
	glVertexAttribPointer(c->attr_point_coord,3,GL_FLOAT,GL_FALSE,sizeof(float)*3,(GLvoid *)((void *)s->coord_p-s->stuff));
	gler(__FILE__,__LINE__);
	if(c->attr_point_normal>=0)
	{
	  glEnableVertexAttribArray(c->attr_point_normal);
	  glVertexAttribPointer(c->attr_point_normal,3,GL_FLOAT,GL_FALSE,sizeof(float)*3,(GLvoid *)((void *)s->normal_p-s->stuff));
	}	

	gler(__FILE__,__LINE__);

	if(s->n_textures>0)
	{
	  for(j=0;j<s->n_textures;j++)
	  {
	    t=s->textures[j];
	    
	    pixbfr=(c->hmdpc_p && c->hmdpc_p->right_flg) ? t->rpixbfr : t->lpixbfr;
	    if(!pixbfr)
	      continue;
	    
	    glActiveTexture(GL_TEXTURE0+j);
//	    lg("Cam %s: Assigning tex %3.3d to unif %d -> %d",c->name,j+1,c->unif_textures[j],c->textures[j]);
	    
	    glBindTexture(GL_TEXTURE_2D,c->textures[j]);
	    if(s->must_update_texture[j])
	    {
	      glTexImage2D(GL_TEXTURE_2D, // target
			   0,  // level, 0 = base, no minimap,
			   GL_RGBA, // internalformat
			   t->w,  // width
			   t->h,  // height
			   0,  // border, always 0 in OpenGL ES
			   GL_RGBA,  // format
			   GL_UNSIGNED_BYTE, // type
			   pixbfr);  
	    }
	    glUniform1i(c->unif_textures[j],j /*c->textures[j]*/);	    
	  }
	  gler(__FILE__,__LINE__);

	  glEnableVertexAttribArray(c->attr_point_texture);
	  glVertexAttribPointer(c->attr_point_texture,2,GL_FLOAT,GL_FALSE,sizeof(float)*2,(GLvoid *)((void *)s->texture_p-s->stuff));
	  gler(__FILE__,__LINE__);
	}
	if(c->attr_point_node>=0)
	{
	  glEnableVertexAttribArray(c->attr_point_node);
	  glVertexAttribPointer(c->attr_point_node,1,GL_FLOAT,GL_FALSE,sizeof(float),(GLvoid *)((void *)s->node_p-s->stuff));
	}
	gler(__FILE__,__LINE__);


//	for(j=0;j<s->n_tot_trias*3;j++)
//	  fprintf(stderr,"DOT%d -> node %d\n",j,s->node_p[j]);

	glDrawArrays(GL_TRIANGLES,0,s->n_tot_trias*3);

	/* if(s->type==GLT_MOREMODERN) */
	/* { */
	/*   lg("AFT: %p",p); */

	/*   if(p!=NULL)	     */
	/*     hexprint((char *)p,sizeof(moremodern_node_stc)*s->n_nodes); */
	/*   glUnmapBuffer(GL_SHADER_STORAGE_BUFFER); */
	/* } */
	
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_TEXTURE_2D);
	glUseProgram(0);
	
	glXSwapBuffers(c->d,(GLXDrawable)c->wnd);
	glFinish();
//	lg("REDR %s",c->name);

	glDisableVertexAttribArray(c->attr_point_node);
	if(s->n_textures>0)
	  glDisableVertexAttribArray(c->attr_point_texture);
	glDisableVertexAttribArray(c->attr_point_normal);
	glDisableVertexAttribArray(c->attr_point_coord);
	glXMakeCurrent(c->d,None,NULL);
	pthread_mutex_unlock(&c->mtx);
      }

      for(i=0;i<s->n_nodes;i++)
	s->nodes[i]->request_shader_data_update=0;
      
      pthread_mutex_unlock(&s->mtx);

      if(s->refresh_log_tag_len)
      {
	struct timespec now;
	
	clock_gettime(CLOCK_MONOTONIC,&now);
	write(s->refresh_logname_unit,&now,sizeof(struct timespec));

	pthread_mutex_lock(&s->mtx);   
	write(s->refresh_logname_unit,&s->refresh_log_tag_len,sizeof(int));
	int res=write(s->refresh_logname_unit,s->refresh_log_tag,s->refresh_log_tag_len);

//	hexprint(s->refresh_log_tag,s->refresh_log_tag_len);

	free(s->refresh_log_tag);
	s->refresh_log_tag=NULL;
	s->refresh_log_tag_len=0;
	pthread_mutex_unlock(&s->mtx);   
      }      
    }
  else
    usleep(3000);
  }
  
  return NULL;
}

static void gler(char *file,int line)
{
  int e=glGetError();
  if(e)
  {
    lg("** GL ERROR 0x%x at %s/%d (%s)",e,file,line,gluErrorString(e));
    abort();
  }  
}

static char *shader_log(GLuint object)
{
  GLint log_length;
  
  if(glIsShader(object))
    glGetShaderiv(object,GL_INFO_LOG_LENGTH,&log_length);
  else if(glIsProgram(object)) 
    glGetProgramiv(object,GL_INFO_LOG_LENGTH,&log_length);
  else
  {
    lg("Object is not a shader or program!");
    exit(1);
  }
  
  if(log_length<=0)
    return NULL;  
  
  char *log=malloc(log_length);
  
  if(glIsShader(object))
    glGetShaderInfoLog(object,log_length,NULL,log);
  else if(glIsProgram(object))
    glGetProgramInfoLog(object,log_length,NULL,log);
  
  return log;
}

static void process_event(glengine_stc *s)
{
  int res=poll(s->pfds,s->n_cameras,0);

  if(res<1)
    return;

  camera_stc *c;
  XEvent event;
  int i;

  for(i=0;i<s->n_cameras;i++)
  {
    c=s->cameras[i];
    
    if(s->pfds[i].revents>0)
    {
      pthread_mutex_lock(&c->mtx);
      if(XCheckMaskEvent(c->d,ExposureMask,&event))
	s->request_redraw=1;
      pthread_mutex_unlock(&c->mtx);
    }
  }
}

static void prepare_stuff(glengine_stc *s)
{
  node_stc *n;
  camera_stc *c;
  int i,j,k;

/*
 * Prepare node uniform arrays
 */
  
  switch(s->type)
  {
  case GLT_COMPAT:      
    s->node_m=malloc(sizeof(hm4)*s->n_nodes);
    s->node_texture=malloc(sizeof(int)*s->n_nodes);
    s->node_amb=malloc(sizeof(hv)*s->n_nodes);
    s->node_diff=malloc(sizeof(hv)*s->n_nodes);
    s->node_spec=malloc(sizeof(hv)*s->n_nodes);
    s->node_shine=malloc(sizeof(float)*s->n_nodes);
    break;
  case GLT_MOREMODERN:
    s->moremodern_nodes=malloc(sizeof(moremodern_node_stc)*s->n_nodes);
    break;
  }
  
  for(i=0;i<s->n_cameras;i++)
    s->cameras[i]->node_valid=malloc(sizeof(int)*s->n_nodes);

  fill_unif_arrays(s);  
  
/*
 * see total nr. of trias
 */

  for(s->n_tot_trias=0,i=0;i<s->n_nodes;i++)
    if(s->nodes[i]->nt)
    {
//      lg("%s: adding %d",s->nodes[i]->name,s->nodes[i]->nt->n_t);
      s->n_tot_trias+=s->nodes[i]->nt->n_t;
    }  

  s->stuff_size=s->n_tot_trias*3*(sizeof(float)*9);
  s->stuff=realloc(s->stuff,s->stuff_size);
  lg("STUFF: allocated %d bytes: %p",s->stuff_size,s->stuff);
  s->coord_p=s->stuff;
  s->normal_p=((void *)s->coord_p)+s->n_tot_trias*3*(sizeof(float)*3);
  s->texture_p=((void *)s->normal_p)+s->n_tot_trias*3*(sizeof(float)*3);
  s->node_p=((void *)s->texture_p)+s->n_tot_trias*3*(sizeof(float)*2);

  tria_stc *t;
  point_stc *p;  
  float *coord_ptr,*normal_ptr,*texture_ptr,*node_ptr;
  
/*
 * in this loop also save matrix at start
 */
    
  for(coord_ptr=s->coord_p,normal_ptr=s->normal_p,texture_ptr=s->texture_p,node_ptr=s->node_p,
	i=0;i<s->n_nodes;i++)
  {
    n=s->nodes[i];

    memcpy(n->mat_at_start,n->mat,sizeof(hm4));
    
    n->disabled_flgs=malloc(sizeof(uint8_t)*s->n_cameras);
    bzero(n->disabled_flgs,sizeof(uint8_t)*s->n_cameras);
    
    if(!n->nt)
      continue;
    
    n->nt->coord_p=coord_ptr;
    n->nt->normal_p=normal_ptr;
    n->nt->texture_p=texture_ptr;
    n->nt->node_p=node_ptr;
    
    fill_node_stuff(n);
    
    coord_ptr+=n->nt->n_t*9;
    normal_ptr+=n->nt->n_t*9;
    texture_ptr+=n->nt->n_t*6;
    node_ptr+=n->nt->n_t*3;    
  }
  
//  hexprint(s->stuff,s->stuff_size);
//  lg("Offsetto %p",(void *)s->node_p-s->stuff);
  
}

static void fill_node_stuff(node_stc *n)
{
  tria_stc *t;
  float *coord_ptr,*normal_ptr,*texture_ptr,*node_ptr;
  int i,j;
  point_stc *p;

  for(coord_ptr=n->nt->coord_p,normal_ptr=n->nt->normal_p,texture_ptr=n->nt->texture_p,node_ptr=n->nt->node_p,t=n->nt->t,i=0;
      i<n->nt->n_t;
      i++,t++)
    for(j=0;j<3;j++,coord_ptr+=3,normal_ptr+=3,texture_ptr+=2,node_ptr++)
    {
      p=n->nt->p+t->v[j];
      memcpy(coord_ptr,p->p,sizeof(hv));
      
      memcpy(normal_ptr,p->n,sizeof(hv));
      memcpy(texture_ptr,p->tp,sizeof(hv2));
      (*node_ptr)=n->prog;
    }
}
    
static void fill_unif_arrays(glengine_stc *s)
{
  update_node_accmats(s);
  
  int i;

  for(i=0;i<s->n_nodes;i++)
    fill_node_unif_arrays(s,s->nodes[i]);
}

static void fill_node_unif_arrays(glengine_stc *s,node_stc *n)
{
  int i;

  switch(s->type)
  {
  case GLT_COMPAT:    
    s->node_texture[n->prog]=n->n_texture;
    for(i=0;i<3;i++)
    {
      s->node_amb[n->prog][i]=((uint8_t *)(n->c.col))[2-i]/256.0;
      s->node_diff[n->prog][i]=((uint8_t *)(n->c.col+1))[2-i]/256.0;
      s->node_spec[n->prog][i]=((uint8_t *)(n->c.col+2))[2-i]/256.0;
    }
    s->node_shine[n->prog]=n->c.shine;
    break;
  case GLT_MOREMODERN:
  {
    moremodern_node_stc *mn=s->moremodern_nodes+n->prog;

    mn->texture=n->n_texture;

    for(i=0;i<3;i++)
    {
      mn->amb[i]=((uint8_t *)(n->c.col))[2-i]/256.0;
      mn->diff[i]=((uint8_t *)(n->c.col+1))[2-i]/256.0;
      mn->spec[i]=((uint8_t *)(n->c.col+2))[2-i]/256.0;
    }
    mn->shine[0]=n->c.shine;
  }
  break;
  }  
}

static void update_enabled_node_sitn(glengine_stc *s)
{
  camera_stc *c;
  int i,j;

  for(i=0;i<s->n_cameras;i++)
  {
    c=s->cameras[i];    
    for(j=0;j<s->n_nodes;j++)
      c->node_valid[j]=s->nodes[j]->disabled_flgs[i] ? 0 : 1;
  }
}

static void update_node_accmats(glengine_stc *s)
{
  hm4 um;
  node_stc *n;
  int i;  
  
  mat4_unit(um);

  for(i=0;i<s->n_root_nodes;i++)    
    calc_accmat_recurse(s->root_nodes[i],um);

  switch(s->type)
  {
  case GLT_COMPAT:      
    for(i=0;i<s->n_nodes;i++)
      memcpy(s->node_m[i],s->nodes[i]->accmat,sizeof(hm4));
    break;
  case GLT_MOREMODERN:
  {
    moremodern_node_stc *ms;
    for(ms=s->moremodern_nodes,i=0;i<s->n_nodes;i++,ms++)
      memcpy(ms->m,s->nodes[i]->accmat,sizeof(hm4));
  }  
  break;
  }
}

static void calc_accmat_recurse(node_stc *n,hm4 in_mat)
{
  mat4_mult(n->mat,in_mat,n->accmat);

  int i;
  
  for(i=0;i<n->n_subnodes;i++)
    calc_accmat_recurse(n->subnodes[i],n->accmat);
}

static void propagate_node_cond(glengine_stc *s,node_stc *n,int camera_id,uint8_t cond)
{
  int i;
  
  if(camera_id<0)
    for(i=0;i<s->n_cameras;i++)
      n->disabled_flgs[i]=cond;
  else    
    n->disabled_flgs[camera_id]=cond;

  for(i=0;i<n->n_subnodes;i++)
    propagate_node_cond(s,n->subnodes[i],camera_id,cond);
}
      
static GLuint compile_glsl_program(camera_stc *c,char *sh_v,char *sh_f)
{
  char *log=NULL;
  GLuint sn;
  GLint res;

  pthread_mutex_lock(&c->mtx);

  gler(__FILE__,__LINE__);
  glXMakeCurrent(c->d,c->wnd,c->ctx);
  gler(__FILE__,__LINE__);

  GLuint program=glCreateProgram();
  
  if(!program)
    rb_raise(rb_eArgError,"%s: Error creating program  for cam. %s",__func__,c->name);
  
  gler(__FILE__,__LINE__);
  sn=glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(sn,1,(const GLchar **)&sh_v,NULL);
  glCompileShader(sn);

  gler(__FILE__,__LINE__);
  glGetShaderiv(sn,GL_COMPILE_STATUS,&res);	
  if(!res)
  {
    log=shader_log(sn);
    rb_raise(rb_eArgError,"%s: in shader compilation (vertex) for cam. %s:\n%s",__func__,c->name,log);
  }	
  glAttachShader(program,sn);
  glDeleteShader(sn);	

  gler(__FILE__,__LINE__);
  sn=glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(sn,1,(const GLchar **)&sh_f,NULL);
  glCompileShader(sn);
  
  glGetShaderiv(sn,GL_COMPILE_STATUS,&res);	
  if(!res)
  {
    log=shader_log(sn);
    rb_raise(rb_eArgError,"%s: in shader compilation (fragment) for cam. %s:\n%s",__func__,c->name,log);
  }	
  glAttachShader(program,sn);
  glDeleteShader(sn);	
  
  glLinkProgram(program);
  
  gler(__FILE__,__LINE__);
  glGetProgramiv(program,GL_LINK_STATUS,&res);
  if(!res)
  {
    log=shader_log(program);
    rb_raise(rb_eArgError,"%s: in shader linking for cam. %s:\n%s\n[V]\n%s\n[F]\n%s",__func__,c->name,log,sh_v,sh_f);
  }
  
  glXMakeCurrent(c->d,None,NULL);
  pthread_mutex_unlock(&c->mtx);

  return program;
}

static void request_node_shader_data_update(glengine_stc *s,node_stc *n)
{
  n->request_shader_data_update=1;
  s->request_redraw=1;
}
