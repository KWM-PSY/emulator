/* dist_hw.h */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Generic hardware extensions - include file
 */

#ifndef HW_H
#define HW_H

/*
 * This one to stop compiler from complaining that
 * we include kernel headers directly
 */

#define __EXPORTED_HEADERS__

#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <complex.h>
#include <stdarg.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <linux/types.h>
#include <linux/input.h>

#define HWUSER_LOG_NAME ".hwuser"

#define HW_SP_N_RELS 6
#define HW_SP_N_BUTTONS 2

typedef enum
{
  HW_DEV_GAMEPAD,
  HW_DEV_SPNAV,
  HW_DEV_XSENSE,
} hw_dev_enum;

typedef enum
{
  HW_GP_B_FL1=294,
  HW_GP_B_FL2=292,
  HW_GP_B_FR1=295,
  HW_GP_B_FR2=293,
  HW_GP_B_SELECT=296,
  HW_GP_B_START=297,
  HW_GP_B_TRIANGLE=288,
  HW_GP_B_SQUARE=291,
  HW_GP_B_CIRCLE=289,
  HW_GP_B_CROSS=290,
  HW_GP_B_LEFTJOY=298, 
  HW_GP_B_RIGHTJOY=299
} hw_gp_btns_enum;

typedef enum
{
  HW_SP_B_LEFT,
  HW_SP_B_RIGHT,
} hw_sp_btns_enum;

typedef enum
{
  HW_SP_REL_X,
  HW_SP_REL_Y,
  HW_SP_REL_Z,
  HW_SP_REL_YANK,
  HW_SP_REL_PITCH,
  HW_SP_REL_ROLL,
} hw_sp_rel_enum;

typedef enum
{
  HW_SP_EVENT_REL,
  HW_SP_EVENT_BTN,
} hw_sp_event_type;

typedef struct gp_event
{
  struct input_event ie;
  struct timespec reftime;  
  struct gp_event *next;
} gp_event_stc;

typedef struct
{
  char *devname,*idname;
  int unit;

  gp_event_stc *events;

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;
} hw_gamepad_stc;

typedef struct
{
  hw_sp_event_type type;
  int prog,val;
} sp_evdata_stc;
  
typedef struct sp_event
{
  sp_evdata_stc d;
  
  struct timespec reftime;  
  struct sp_event *next;
} sp_event_stc;

typedef struct
{
  char *devname,*idname;
  int unit;

  uint8_t btn_cond[HW_SP_N_BUTTONS];
  sp_event_stc *events;

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;
} hw_spnav_stc;

typedef struct
{
  int msgtype,msglen;
  uint8_t *msg;
} xs_evdata_stc;

typedef struct xsense_event
{
  xs_evdata_stc d;
  
  struct timespec reftime;  
  struct xsense_event *next;
} xsense_event_stc;

typedef struct
{
  char *devname,*idname;
  struct termios orig_tios,tios;
  int unit;

  xsense_event_stc *events;

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;
} hw_xsense_stc;

typedef struct
{
  uint32_t *data,len,n_samples,cur_pos;
  uint8_t immediate_flg;
  int repeat_count;
} sampleblock_stc;

typedef struct queued
{
  sampleblock_stc *s;
  struct queued *next;
} queued_stc;

typedef struct
{
  hw_dev_enum type;
  union
  {
    hw_gamepad_stc *gp;
    hw_spnav_stc *sp;
    hw_xsense_stc *xsense;
  } devdata;
} hw_device_stc;

const static uint16_t hw_sp_rel[HW_SP_N_RELS]={0,2,1,4,3,5};
const static char *hw_sp_rel_lbls[]={"x","y","z","yank","pitch","roll",NULL};

const static uint16_t hw_sp_buttons[HW_SP_N_BUTTONS]={256,257};
const static char *hw_sp_btn_lbls[]={"LEFT","RIGHT",NULL};

extern const rb_data_type_t hw_gamepad_dtype,hw_spnav_dtype,hw_xsense_dtype;

extern void lg(const char *fmt,...);
extern void hexprint(char *ptr,int len);

#endif
