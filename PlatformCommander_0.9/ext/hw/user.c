/* user.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * C code for the thread that manages several hardware devices
 */

#include "hw.h"
#include <pthread.h>

typedef struct user_event
{
  int devid;
  hw_dev_enum type;
  void *ev;
  
  struct user_event *next;
} user_event_stc;

typedef struct
{
  int n_devices,log_unit;

  hw_device_stc *devices;

  user_event_stc *events;
  
  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;  
} hw_user_stc;

extern VALUE mod_hw,cls_hw_user;

static void free_hw_user(void *p);
static void stop_thread(hw_user_stc *s);
static void *thr(void *arg);
static void append_user_event(hw_user_stc *s,int devid,struct timespec *t,hw_dev_enum type,void *d);
static void get_datainfo(user_event_stc *ev,struct timespec **t,void **data,int *len);

static size_t hw_user_memsize(const void *p)
{
  return sizeof(hw_user_stc);
}

static const rb_data_type_t hw_user_dtype=
{
  "hw_user",
  {0,free_hw_user,hw_user_memsize,},
  0,0,
  RUBY_TYPED_FREE_IMMEDIATELY,
};

VALUE new_hw_user(VALUE self,VALUE v_dev_array,VALUE v_log_dir)
{
  hw_user_stc *s;
  VALUE sdata=TypedData_Make_Struct(cls_hw_user,hw_user_stc,&hw_user_dtype,s);
  int sts;

  bzero(s,sizeof(hw_user_stc));

  char bfr[RSTRING_LEN(v_log_dir)+strlen(HWUSER_LOG_NAME)+2];

  sprintf(bfr,"%s/%s",RSTRING_PTR(v_log_dir),HWUSER_LOG_NAME);
  s->log_unit=open(bfr,O_WRONLY|O_CREAT|O_TRUNC,0660);

  if(s->log_unit<=0)
    rb_raise(rb_eArgError,"%s: error opening logfile %s (%s)",__func__,bfr,strerror(errno));    

  rb_funcall(sdata,rb_intern("initialize"),1,v_dev_array);
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  
  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;
}

static void free_hw_user(void *p)
{
  hw_user_stc *s=(hw_user_stc *)p;
  int i;

  stop_thread(s);
  close(s->log_unit);

  free(p);
}

VALUE hw_user_add_monitored_device(VALUE self,VALUE v_type,VALUE v_dev)
{
  hw_user_stc *s;  
  TypedData_Get_Struct(self,hw_user_stc,&hw_user_dtype,s);

  hw_device_stc hd;

  hd.type=FIX2INT(v_type);

  switch(hd.type)
  {
  case HW_DEV_GAMEPAD:
    TypedData_Get_Struct(v_dev,hw_gamepad_stc,&hw_gamepad_dtype,hd.devdata.gp);
    break;
  case HW_DEV_SPNAV:
    TypedData_Get_Struct(v_dev,hw_spnav_stc,&hw_spnav_dtype,hd.devdata.sp);
    break;
  case HW_DEV_XSENSE:
    TypedData_Get_Struct(v_dev,hw_xsense_stc,&hw_xsense_dtype,hd.devdata.xsense);
    break;
  default:
    lg("%s Sorry! Device type %d unknown!",__func__,hd.type);
    abort();
  }
  
  s->devices=realloc(s->devices,sizeof(hw_device_stc)*(s->n_devices+1));
  memcpy(s->devices+s->n_devices,&hd,sizeof(hw_device_stc));

  VALUE to_ret=INT2FIX(s->n_devices);
  
  s->n_devices++;
  return to_ret;
}

VALUE hw_user_poll(VALUE self,VALUE v_objrec,VALUE v_func)
{
  hw_user_stc *s;  
  TypedData_Get_Struct(self,hw_user_stc,&hw_user_dtype,s);

  if(!s->events)
    return Qfalse;

  user_event_stc *evq,*evp1,*evp2;
  struct timespec *t;
  void *dptr=NULL;
  int dlen=0;

  pthread_mutex_lock(&s->mtx);
  evq=s->events;
  s->events=NULL;
  pthread_mutex_unlock(&s->mtx);

  for(evp1=evq;evp1;)
  {
    evp2=evp1;
    evp1=evp2->next;

    get_datainfo(evp2,&t,&dptr,&dlen);
    
    rb_funcall(v_objrec,rb_to_id(v_func),4,INT2FIX(evp2->devid),rb_str_new((char *)t,sizeof(struct timespec)),INT2FIX(evp2->type),rb_str_new(dptr,dlen));
    
    free(evp2->ev);
    free(evp2);
  }    

  return self;
}

VALUE hw_user_stop_thread(VALUE self)
{
  hw_user_stc *s;  
  TypedData_Get_Struct(self,hw_user_stc,&hw_user_dtype,s);

  stop_thread(s);

  return self;
}

static void stop_thread(hw_user_stc *s)
{
  if(s->thr && !s->leave_thread)
  {
    s->leave_thread=1;
    pthread_join(s->thr,NULL);
    s->thr=(pthread_t)0;
  }
}
  
static void *thr(void *arg)
{
  hw_user_stc *s=(hw_user_stc *)arg;
  hw_device_stc *hd;
  int i;
  uint8_t found;

  hw_gamepad_stc *gp;
  gp_event_stc *gp_evq,*gp_evp1,*gp_evp2;
  hw_spnav_stc *sp;
  sp_event_stc *sp_evq,*sp_evp1,*sp_evp2;
  hw_xsense_stc *xs;
  xsense_event_stc *xs_evq,*xs_evp1,*xs_evp2;

  while(!s->leave_thread)
  {
    for(found=0,hd=s->devices,i=0;i<s->n_devices;i++,hd++)
    {
      switch(hd->type)
      {
      case HW_DEV_GAMEPAD:
	gp=hd->devdata.gp;	
	if(gp->events)
	{
	  found=1;
	  
	  pthread_mutex_lock(&gp->mtx);
	  gp_evq=gp->events;
	  gp->events=NULL;
	  pthread_mutex_unlock(&gp->mtx);
	  
	  for(gp_evp1=gp_evq;gp_evp1;)
	  {
	    gp_evp2=gp_evp1;
	    gp_evp1=gp_evp2->next;
	    
	    write(s->log_unit,&gp_evp2->reftime,sizeof(struct timespec));
	    write(s->log_unit,&hd->type,sizeof(hw_dev_enum));
	    write(s->log_unit,&gp_evp2->ie,sizeof(struct input_event));

	    append_user_event(s,i,&gp_evp2->reftime,hd->type,gp_evp2);
	  }
	}	
	break;
      case HW_DEV_SPNAV:
	sp=hd->devdata.sp;	
	if(sp->events)
	{
	  found=1;
	  
	  pthread_mutex_lock(&sp->mtx);
	  sp_evq=sp->events;
	  sp->events=NULL;
	  pthread_mutex_unlock(&sp->mtx);
	  
	  for(sp_evp1=sp_evq;sp_evp1;)
	  {
	    sp_evp2=sp_evp1;
	    sp_evp1=sp_evp2->next;
	    
	    write(s->log_unit,&sp_evp2->reftime,sizeof(struct timespec));
	    write(s->log_unit,&hd->type,sizeof(hw_dev_enum));
	    write(s->log_unit,&sp_evp2->d.type,sizeof(hw_sp_event_type));
	    write(s->log_unit,&sp_evp2->d.prog,sizeof(int));
	    write(s->log_unit,&sp_evp2->d.val,sizeof(int));

	    append_user_event(s,i,&sp_evp2->reftime,hd->type,sp_evp2);
	  }
	}	
	break;
      case HW_DEV_XSENSE:
	xs=hd->devdata.xsense;
	if(xs->events)
	{
	  found=1;
	  
	  pthread_mutex_lock(&xs->mtx);
	  xs_evq=xs->events;
	  xs->events=NULL;
	  pthread_mutex_unlock(&xs->mtx);

	  for(xs_evp1=xs_evq;xs_evp1;)
	  {
	    xs_evp2=xs_evp1;
	    xs_evp1=xs_evp2->next;

	    write(s->log_unit,&xs_evp2->reftime,sizeof(struct timespec));
	    write(s->log_unit,&hd->type,sizeof(hw_dev_enum));
	    write(s->log_unit,&xs_evp2->d.msgtype,sizeof(int));
	    write(s->log_unit,&xs_evp2->d.msglen,sizeof(int));
	    write(s->log_unit,xs_evp2->d.msg,xs_evp2->d.msglen);

/*
 * I do not append the user event to the queue, because these events
 * do not need to be processed in real time. The packet is freed here
 */

	    free(xs_evp2->d.msg);
	    free(xs_evp2);
	  }
	}	
	break;
      default:
	lg("%s: Hw type %d not supported in thr!",__func__,hd->type);
	abort();
      }
      
    }
    if(!found)
      usleep(10000);
  }
  
  lg("Hw user thr out.");

  return NULL;
}

static void append_user_event(hw_user_stc *s,int devid,struct timespec *t,hw_dev_enum type,void *d)
{
  user_event_stc *ev=malloc(sizeof(user_event_stc)),*evp;
  
  ev->devid=devid;    
  ev->type=type;    
  ev->ev=d;

  ev->next=NULL;
  pthread_mutex_lock(&s->mtx);
  if(!s->events)
    s->events=ev;
  else
  {
    for(evp=s->events;evp->next;evp=evp->next)
      ;
    evp->next=ev;
  }
  pthread_mutex_unlock(&s->mtx);
}

static void get_datainfo(user_event_stc *ev,struct timespec **t,void **data,int *len)
{
  switch(ev->type)
  {
  case HW_DEV_GAMEPAD:
  {
    gp_event_stc *gp_ev=(gp_event_stc *)ev->ev;    
    (*t)=&gp_ev->reftime;
    (*data)=&gp_ev->ie;
    (*len)=sizeof(struct input_event);
  }
  break;
  case HW_DEV_SPNAV:
  {
    sp_event_stc *sp_ev=(sp_event_stc *)ev->ev;    
    (*t)=&sp_ev->reftime;
    (*data)=&sp_ev->d;
    (*len)=sizeof(sp_evdata_stc);
  }
  break;
  case HW_DEV_XSENSE:
    lg("ERROR! Xsense data should not be present in the user queue!");
    abort();
  default:
    lg("%s: Ev type %d not supported!",__func__,ev->type);
    abort();    
  }
}
