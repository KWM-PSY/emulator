/* spnav.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Management of a 3dconnexion Space Navigator device
 */

#include "hw.h"
#include <pthread.h>
#include <poll.h>

#define POLL_TIMEOUT 350

extern VALUE mod_hw,cls_hw_spnav;

static void free_hw_spnav(void *p);
static void stop_thread(hw_spnav_stc *s);
static void *sp_thr(void *arg);

static size_t hw_spnav_memsize(const void *p)
{
  return sizeof(hw_spnav_stc);
}

const rb_data_type_t hw_spnav_dtype={"hw_spnav",
				     {0,free_hw_spnav,hw_spnav_memsize,},
				     0,0,
				     RUBY_TYPED_FREE_IMMEDIATELY,};

VALUE new_hw_spnav(VALUE self,VALUE v_devname)
{
  hw_spnav_stc *s;
  VALUE sdata=TypedData_Make_Struct(cls_hw_spnav,hw_spnav_stc,&hw_spnav_dtype,s),v;
  int i;

  VALUE mod_moo=rb_define_module("Moo");

  bzero(s,sizeof(hw_spnav_stc));

  s->devname=strdup(RSTRING_PTR(v_devname));
  
  VALUE v_idname=rb_funcall(mod_moo,rb_intern("find_input_device"),1,v_devname);

  if(v_idname==Qfalse)
    rb_raise(rb_eArgError,"%s: Device %s not found",__func__,s->devname);
  s->idname=strdup(RSTRING_PTR(v_idname));
  s->unit=open(s->idname,O_RDONLY);
  if(s->unit<0)
    rb_raise(rb_eArgError,"%s: Device %s (%s) could not be opened (%s)",__func__,
	     s->devname,s->idname,strerror(errno));
  
/*
 * The thread for managing the spnav
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  
  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,sp_thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;  
}

static void free_hw_spnav(void *p)
{
  hw_spnav_stc *s=(hw_spnav_stc *)p;
  int i;

  stop_thread(s);

  close(s->unit);
  free(s->devname);
  free(s->idname);

  free(p);
}

VALUE hw_spnav_poll(VALUE self)
{
  hw_spnav_stc *s;  
  TypedData_Get_Struct(self,hw_spnav_stc,&hw_spnav_dtype,s);

  if(!s->events)
    return Qfalse;

  sp_event_stc *evq,*evp1,*evp2;

  pthread_mutex_lock(&s->mtx);
  evq=s->events;
  s->events=NULL;
  pthread_mutex_unlock(&s->mtx);

  VALUE to_ret=rb_ary_new(),v;

  for(evp1=evq;evp1;)
  {
    evp2=evp1;
    evp1=evp2->next;

    v=rb_ary_new_capa(3);
    v=rb_ary_new_capa(4);
      
    rb_ary_store(v,0,INT2FIX(evp2->d.type));
    rb_ary_store(v,1,INT2FIX(evp2->d.prog));
    switch(evp2->d.type)
    {
    case HW_SP_EVENT_REL:
      rb_ary_store(v,2,INT2FIX(evp2->d.val));
      break;
    case HW_SP_EVENT_BTN:
      rb_ary_store(v,2,evp2->d.val ? Qtrue : Qfalse);
      break;
    }
    rb_ary_store(v,3,rb_str_new((char *)&evp2->reftime,sizeof(struct timespec)));    
    rb_ary_push(to_ret,v);
    
    free(evp2);
  }    

  return to_ret;
}

VALUE hw_spnav_stop_thread(VALUE self)
{
  hw_spnav_stc *s;  
  TypedData_Get_Struct(self,hw_spnav_stc,&hw_spnav_dtype,s);

  stop_thread(s);

  return self;
}

static void stop_thread(hw_spnav_stc *s)
{
  if(s->thr && !s->leave_thread)
  {
    s->leave_thread=1;
    pthread_join(s->thr,NULL);
    s->thr=(pthread_t)0;
  }
}

static void *sp_thr(void *arg)
{
  hw_spnav_stc *s=(hw_spnav_stc *)arg;
  struct pollfd pfd={s->unit,POLLIN,0};
  struct input_event ie;
  int len,res,i;
  sp_event_stc *ev=NULL,*evp;
  int32_t new_val;

  while(!s->leave_thread)
  {
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);
    
    if(res<0)
    {
      lg("%s: Error in polling (%s)",__func__,strerror(errno));
      break;
    }

    if(res!=0)
    {
      len=read(s->unit,&ie,sizeof(struct input_event));
      if(len!=sizeof(struct input_event))
      {
	lg("%s: Error in reading (%s)",__func__,strerror(errno));
	break;
      }
      
      if(ie.type==EV_KEY)
      {
	for(i=0;i<HW_SP_N_BUTTONS;i++)
	  if(ie.code==hw_sp_buttons[i])
	  {
	    new_val=ie.value ? 1 : 0;
	    if(s->btn_cond[i]!=new_val)
	    {
	      s->btn_cond[i]=new_val;
	      ev=malloc(sizeof(sp_event_stc));
	      ev->d.type=HW_SP_EVENT_BTN;
	      ev->d.prog=i;
	      ev->d.val=s->btn_cond[i];
	      clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
	      ev->next=NULL;
	    }
	    break;
	  }
//	lg("code %d btn %d",ie.code,i);
      }
      else if(ie.type==EV_REL)
      {
	for(i=0;i<HW_SP_N_RELS;i++)
	  if(ie.code==hw_sp_rel[i])
	  {
	    ev=malloc(sizeof(sp_event_stc));
	    ev->d.type=HW_SP_EVENT_REL;
	    ev->d.prog=i;
	    ev->d.val=(signed int)ie.value;
	    clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
	    ev->next=NULL;
	    break;
	  }
//	lg("code %d vrel %d",ie.code,i);
      }

      if(ev) // a new event!
      {
	pthread_mutex_lock(&s->mtx);
	if(!s->events)
	  s->events=ev;
	else
	{
	  for(evp=s->events;evp->next;evp=evp->next)
	    ;
	  evp->next=ev;
	}
	pthread_mutex_unlock(&s->mtx);

	ev=NULL;
      }   
//      lg("EVE type %x code %d(0x%x) value %d",ie.type,ie.code,ie.code,ie.value);
    } 
  }

  lg("Spnav thr out.");
    
  return NULL;
}
	
 
