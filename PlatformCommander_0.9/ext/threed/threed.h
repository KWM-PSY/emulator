/* dists_threed.h */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Most of this code is superseded by glengine
 */

/* threed.h */

#ifndef THREED_H
#define THREED_H

/*
 * This one to stop compiler from complaining that
 * we include kernel headers directly
 */

#define __EXPORTED_HEADERS__

#include <maths_include.h>
#include "../moo/moo.h"

#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <complex.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/time.h>
#include <linux/types.h>
#include <GL/glew.h>
#include <GL/glx.h>
#include <pthread.h>

typedef struct
{
  hv pos[3],norm[3],cols[3]; // amb,spec,diff
  float shininess;
  hv2 texpos[3];
} threed_tria_stc;

typedef struct threed_node
{
  int prog,n_trias,n_nodes;
  threed_tria_stc *trias;
  hv attach_offset;
  hm4 mat;
  GLuint triangle_data,n_tex;
  int tex_x,tex_y,tex_size;
  char *texdata;
  
  float *stuff;
  void (*actionfunc)(struct threed_node *n);
  void *actiondata;
  
  struct threed_node **nodes;
} threed_node_stc;

typedef struct
{
  hv lower_verts[3],lower_joints[3][2],
    upper_verts[3],upper_joints[3][2];
} threed_base_stc;

typedef struct
{
  float pos[3],col[3],range;
  GLint unif_pos,unif_col,unif_range; // NOW NOT USED!!!
} threed_light_stc;

/*
 * Env structures
 */

typedef enum
{
  HMD_TYPE_CARLO,
  HMD_TYPE_DUMB,
} hmd_type_enum;

typedef struct
{
  char *name,*dpy_name;
  int progr,x,y,w,h,vers[2];
  uint8_t decor,exposed;
  hv position,look,up;
  hm4 v,p;
  float ratio,fovy,near,far;

  uint8_t *inactive_flgs;

  struct hmd_warp_stuff *belongs_to_hmw;
  
  Display *d;
  Window wnd;
  GLXContext ctx;

  GLuint program;
  GLint attr_coord,attr_normal,attr_amb,attr_diff,attr_spec,attr_shininess,attr_texpos,
    unif_m,unif_v,unif_p,unif_n_lights,unif_light_pos,unif_light_col,unif_light_range,unif_tex;

  pthread_mutex_t mtx;
} env_camera_stc;

typedef struct hmd_warp_stuff
{
  GLint attr_coord,attr_tex,unif_warp_factor,unif_scale_factor;
  GLuint program_warp,fbo,color_tex,depth_tex,quad_data;
  
  env_camera_stc *c;

  struct env_hmd *hmdp;
} hmd_warp_stuff_stc;

typedef struct
{
  hv p,n;
} threed_ply_vert_stc;

typedef struct
{
  threed_ply_vert_stc *v[3];
  hv n;
} threed_ply_face_stc;

typedef struct
{
  uint8_t has_norm;
  int n_v,n_f;
  threed_ply_vert_stc *v;
  threed_ply_face_stc *f;
} threed_ply_stc;

extern void lg(const char *fmt,...);

extern void threed_cylinder(threed_node_stc *n,float radius,float len,int n_refinements);
extern void threed_cone(threed_node_stc *n,float radius,float len,int n_refinements);
extern void threed_sphere(threed_node_stc *n,float radius,int n_refinements);
extern void threed_thick_tria(threed_node_stc *n,hv vert[3],float thickness);
extern void threed_random_trias(threed_node_stc *n,hv boxdim,int n_tria,float triadim,hv normal);
extern void threed_random_trias_regenerate_trias(threed_node_stc *n,hv boxdim,int n_tria);
extern void threed_random_trias_load_run(threed_node_stc *n,hv mainmotion,float randpct,hv randmotion[2]);
extern void threed_random_trias_position_run(threed_node_stc *n,float pos);

extern threed_node_stc *new_node(void);
extern void fill_node_stuff(threed_node_stc *n,uint8_t realloc_flg);
extern void free_node(threed_node_stc *n);
extern void init_trias(threed_tria_stc *t,int n_t);

extern threed_ply_stc *parse_ply(char *fn,float scale);
extern void free_ply_stc(threed_ply_stc *s);
extern void threed_ply(threed_node_stc *n,char *fn,float scale);

#endif
