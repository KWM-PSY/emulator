/* joystick.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Management of one specific joystick
 */

// joystick.c

#include <maths_include.h>
#include "moo.h"
#include <pthread.h>
#include <poll.h>
#include <linux/input.h>

#define POLL_TIMEOUT 350

typedef struct joys_event
{
  moo_joys_event_type type;
  int prog,val;
  struct timespec reftime;  
  struct joys_event *next;
} joys_event_stc;

typedef struct
{
  char *devname,*idname;
  int unit;

  uint8_t btn_cond[MOO_JOYS_N_BUTTONS];
  int32_t abs_cond[MOO_JOYS_N_ABS];

  joys_event_stc *events;

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;
} moo_joystick_stc;

extern VALUE mod_moo,cls_moo_joystick;

static void free_moo_joystick(void *p);
static void close_thread(moo_joystick_stc *s);
static void *joy_thr(void *arg);

VALUE new_moo_joystick(VALUE self,VALUE v_devname)
{
  moo_joystick_stc *s;
  VALUE sdata=Data_Make_Struct(cls_moo_joystick,moo_joystick_stc,NULL,free_moo_joystick,s),v;
  int i;

  bzero(s,sizeof(moo_joystick_stc));

  s->devname=strdup(RSTRING_PTR(v_devname));
  
  VALUE v_idname=rb_funcall(mod_moo,rb_intern("find_input_device"),1,v_devname);

  if(v_idname==Qfalse)
    rb_raise(rb_eArgError,"%s: Device %s not found",__func__,s->devname);
  s->idname=strdup(RSTRING_PTR(v_idname));
  s->unit=open(s->idname,O_RDONLY);
  if(s->unit<0)
    rb_raise(rb_eArgError,"%s: Device %s (%s) could not be opened (%s)",__func__,
	     s->devname,s->idname,strerror(errno));
  
/*
 * The thread for managing the joystick
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  
  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,joy_thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;  
}

static void free_moo_joystick(void *p)
{
  moo_joystick_stc *s=(moo_joystick_stc *)p;
  int i;

  close_thread(s);
  free(s->devname);
  free(s->idname);

  free(p);
}

VALUE moo_joystick_poll(VALUE self)
{
  moo_joystick_stc *s;  
  Data_Get_Struct(self,moo_joystick_stc,s);

  if(!s->events)
    return Qfalse;

  joys_event_stc *evq,*evp1,*evp2;

  pthread_mutex_lock(&s->mtx);
  evq=s->events;
  s->events=NULL;
  pthread_mutex_unlock(&s->mtx);

  VALUE to_ret=rb_ary_new(),v;

  for(evp1=evq;evp1;)
  {
    evp2=evp1;
    evp1=evp2->next;

    v=rb_ary_new_capa(4);
      
    rb_ary_store(v,0,INT2FIX(evp2->type));
    rb_ary_store(v,1,INT2FIX(evp2->prog));
    switch(evp2->type)
    {
    case JOY_EVENT_BTN:
      rb_ary_store(v,2,evp2->val ? Qtrue : Qfalse);
      break;
    case JOY_EVENT_ABS:
      rb_ary_store(v,2,DBL2NUM((float)evp2->val/moo_joys_vab_max[evp2->prog]));
      break;
    }    
    rb_ary_store(v,3,rb_str_new((char *)&evp2->reftime,sizeof(struct timespec)));    
    rb_ary_push(to_ret,v);
    
    free(evp2);
  }
  
  return to_ret;
}

VALUE moo_joystick_close_thread(VALUE self)
{
  moo_joystick_stc *s;  
  Data_Get_Struct(self,moo_joystick_stc,s);

  close_thread(s);

  return self;
}

static void close_thread(moo_joystick_stc *s)
{
  if(s->leave_thread)
    return;

  s->leave_thread=1;
  pthread_join(s->thr,NULL);

  close(s->unit);
}

static void *joy_thr(void *arg)
{
  moo_joystick_stc *s=(moo_joystick_stc *)arg;
  struct pollfd pfd={s->unit,POLLIN,0};
  struct input_event ie;
  int res,len,i,new_val;
  uint8_t cond;
  joys_event_stc *ev=NULL,*evp;
  
  while(!s->leave_thread)
  {
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);

    if(res<0)
    {
      lg("%s: Error in polling (%s)",__func__,strerror(errno));
      break;
    }
    
    if(res!=0)
    {
      len=read(s->unit,&ie,sizeof(struct input_event));
      if(len!=sizeof(struct input_event))
      {
	lg("%s: Error in reading (%s)",__func__,strerror(errno));
	break;
      }

      if(ie.type==EV_KEY)
      {	
	for(i=0;i<MOO_JOYS_N_BUTTONS;i++)
	  if(ie.code==moo_joys_buttons[i])
	  {
	    new_val=ie.value ? 1 : 0;
	    if(s->btn_cond[i]!=new_val)
	    {
	      s->btn_cond[i]=new_val;
	      ev=malloc(sizeof(joys_event_stc));
	      ev->type=JOY_EVENT_BTN;
	      ev->prog=i;
	      ev->val=s->btn_cond[i];
	      clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
	      ev->next=NULL;
	    }
	    break;
	  }
//	lg("code %d btn %d",ie.code,i);
      }
      else if(ie.type==EV_ABS)
      {
	for(i=0;i<MOO_JOYS_N_ABS;i++)
	  if(ie.code==moo_joys_vabs[i])
	  {
	    new_val=ie.value;
	    if(s->abs_cond[i]!=new_val)
	    {
	      s->abs_cond[i]=new_val;
	      ev=malloc(sizeof(joys_event_stc));
	      ev->type=JOY_EVENT_ABS;
	      ev->prog=i;
	      ev->val=s->abs_cond[i];
	      clock_gettime(CLOCK_MONOTONIC,&ev->reftime);
	      ev->next=NULL;
	    }
	    break;
	  }
//	lg("code %d vabs %d",ie.code,i);
      }

      if(ev) // a new event!
      {
	pthread_mutex_lock(&s->mtx);
	if(!s->events)
	  s->events=ev;
	else
	{
	  for(evp=s->events;evp->next;evp=evp->next)
	    ;
	  evp->next=ev;
	}
	pthread_mutex_unlock(&s->mtx);

	ev=NULL;
      }      
    }
  }

  lg("Joystick thr out.");
    
  return NULL;
}


