/* moo.h */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Moog-specific extension - include file
 */

#ifndef MOO_H
#define MOO_H

/*
 * This one to stop compiler from complaining that
 * we include kernel headers directly
 */

#define __EXPORTED_HEADERS__

#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <complex.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <linux/types.h>
#include <arpa/inet.h>

#define USB1608GX_2AO_V2_PID (0x0136)

#define TSTEP_SEC (1.0/60.0)
#define TSTEP_USEC 16667
#define MAXWAIT_USEC 30000

#define MOO_CMD_OFF 0
#define MOO_CMD_DISABLE 0xdc
#define MOO_CMD_PARK 0xd2
#define MOO_CMD_LOW_LIMIT_ENABLE 0xc8
#define MOO_CMD_LOW_LIMIT_DISABLE 0xbe
#define MOO_CMD_ENGAGE 0xb4
#define MOO_CMD_START 0xaf
#define MOO_CMD_LENGTH_MODE 0xac
#define MOO_CMD_DOF_MODE 0xaa
#define MOO_CMD_RESET 0xa0
#define MOO_CMD_INHIBIT 0x96
#define MOO_CMD_MDA_MODE 0xbc
#define MOO_CMD_NEW_POSITION 0x82
#define MOO_CMD_NEW_MDA 0x80
#define MOO_CMD_NEW_MDA_FILE 0x9b

/*
 * To get ACTU_MIN_H and ACTU_MAX_H when changing MOO_ACTU_FIXED:
 * - compile
 * - run scripts/actu_from_vertpos.rb and save output in a file
 * - Look for value closest to ACTU_MIN and ACTU_MAX
 */

#define MOO_N_ACTU 6
#define MOO_ACTU_MIN (float)0.03049 //0.03048 // corresponding to a height of 370mm
#define MOO_ACTU_MIN_MM (float)(MOO_ACTU_MIN*1000.0)
#define MOO_ACTU_MIN_H (float)0.507
//#define MOO_ACTU_IDLE (float)0.056374 // corresponding to a height of 370+47.6mm
//#define MOO_ACTU_IDLE_MM (float)(MOO_ACTU_IDLE*1000.0)
//#define MOO_ACTU_IDLE_H (float)0.4176
#define MOO_ACTU_IDLE (float)0.056374
#define MOO_ACTU_IDLE_MM (float)(MOO_ACTU_IDLE*1000.0)
#define MOO_ACTU_IDLE_H (float)0.7 // 0.558 //0.489 //0.4176
#define MOO_ACTU_MAX ((float)0.297)//0.3 //0.30988 // corresponding to a height of 785mm
#define MOO_ACTU_MAX_MM (float)(MOO_ACTU_MAX*1000.0)
#define MOO_ACTU_MAX_H (float)0.860
#define MOO_ACTU_PARK MOO_ACTU_MIN
#define MOO_ACTU_PARK_MM (float)(MOO_ACTU_PARK*1000.0)

/*
 * Fixed value from empirical work
 *
//#define MOO_ACTU_FIXED ((float)0.685)//((float)0.72-MOO_ACTU_MIN)
#define MOO_ACTU_FIXED ((float)0.705)//((float)0.72-MOO_ACTU_MIN)
//#define MOO_ACTU_FIXED ((float)0.7)
*/

/*
 * Fixed value from blueprint
 */

#define MOO_ACTU_FIXED ((float)0.70865)

#define MOO_ACTU_FIXED_MM (float)(MOO_ACTU_FIXED*1000.0)

#define MOO_MOOG_H_TO_SRV_H ((float)0.465)

#define MOO_LX_MIN ((float)-200.0)
#define MOO_LX_MAX ((float)200.0)
#define MOO_LX_PARK ((float)0.0)
#define MOO_LY_MIN (MOO_ACTU_MIN_H*1000.0)
#define MOO_LY_MAX (MOO_ACTU_MAX_H*1000.0)
#define MOO_LY_PARK (MOO_LY_MIN+1.0)
#define MOO_LZ_MIN ((float)-200.0)
#define MOO_LZ_MAX ((float)200.0)
#define MOO_LZ_PARK ((float)0.0)
#define MOO_YAW_MIN ((float)(-60.0*M_PI/360.0))
#define MOO_YAW_MAX ((float)(60.0*M_PI/360.0))
#define MOO_YAW_PARK ((float)0.0)
#define MOO_PITCH_MIN ((float)(-60.0*M_PI/360.0))
#define MOO_PITCH_MAX ((float)(60.0*M_PI/360.0))
#define MOO_PITCH_PARK ((float)0.0)
#define MOO_ROLL_MIN ((float)(-60.0*M_PI/360.0))
#define MOO_ROLL_MAX ((float)(60.0*M_PI/360.0))
#define MOO_ROLL_PARK ((float)0.0)

#define MOO_JOYS_N_BUTTONS 9
#define MOO_JOYS_N_ABS 6

#define MOO_SPN_N_RELS 6
#define MOO_SPN_N_BUTTONS 2

typedef enum
{
  MOO_RESP_ESTOP=1,
  MOO_RESP_SNUBBER,
  MOO_RESP_ACTU_RUNAWAY,
  MOO_RESP_BATT_FAULT, // 5
  MOO_RESP_LOW_IDLE_RATE,
  MOO_RESP_MOTOR_THERMAL_FAULT,
  MOO_RESP_RANGE_ERROR,
  MOO_RESP_INVALID_FRAME,
  MOO_RESP_WATCHDOG, // 10
  MOO_RESP_LIMIT_SWITCH_FAULT,
  MOO_RESP_DRIVE_BUS_FAULT,
  MOO_RESP_AMPLIFIER_FAULT,
  MOO_RESP_COMM_FAULT,
  MOO_RESP_HOMING_FAULT, // 15
  MOO_RESP_ENVELOPE_FAULT,
  MOO_RESP_TORQUE_MON_FAULT,
  MOO_RESP_ESTOP_SENSE,
  MOO_RESP_AMPLI_ENABLE,
  MOO_RESP_DRIVE_BUS_SENSE, // 20
  MOO_RESP_LIMIT_SHUNT,
  MOO_RESP_LIMIT_SWITCH_SENSE,
  MOO_RESP_AMPLI_FAULT,
  MOO_RESP_THERMAL_FAULT_SENSE,
  MOO_RESP_BASE_AT_HOME, // 25
  MOO_RESP_MAX
} moo_resp_enum;

typedef enum
{
  MOO_MODE_LENGTH,
  MOO_MODE_DOF,
  MOO_MODE_MDA,
  MOO_MODE_SPARE
} moo_mode_enum;

typedef enum
{
  MOO_MACH_STATE_POWERUP,
  MOO_MACH_STATE_IDLE,
  MOO_MACH_STATE_STBY,
  MOO_MACH_STATE_ENGAGED,
  MOO_MACH_STATE_PARKING=7,
  MOO_MACH_STATE_FAULT1,
  MOO_MACH_STATE_FAULT2,
  MOO_MACH_STATE_FAULT3,
  MOO_MACH_STATE_DISABLED,
  MOO_MACH_STATE_INHIBITED
} moo_mach_state_enum;

typedef enum
{
  MOO_SRVR_STATE_OFF,
  MOO_SRVR_STATE_IDLE,
  MOO_SRVR_STATE_ENGAGING,
  MOO_SRVR_STATE_ENGAGED,
  MOO_SRVR_STATE_PARKING_WAIT,
  MOO_SRVR_STATE_PARKING,
  MOO_SRVR_STATE_RESET,
  MOO_SRVR_STATE_FAULT,
} moo_srvr_state_enum;
  
typedef enum
{
  DIO_NOTUSED,
  DIO_IN,
  DIO_IN_LATCHOFF,
  DIO_IN_LATCHON,
  DIO_OUT,
} mcc_dio_dir_enum;

typedef enum
{
  AN_IN,
  AN_OUT,
  DIG_IN,
  DIG_OUT
} moo_comedi_ch_type;

typedef enum
{
  ALG_LIN,
  ALG_SIN,
  ALG_CASTELJAU,
  ALG_SINEACC,
  ALG_DAMPEDOSC,
} moo_algo_type;

typedef enum
{
  QT_NO_ERROR,
  QT_ERROR_SHORT,
  QT_ERROR_LONG,
  QT_ERROR_FAST,
} moo_quicktest_error;

typedef enum
{
  JOY_BTN_ACT,
  JOY_BTN_B_1,
  JOY_BTN_B_3,
  JOY_BTN_B_4,
  JOY_BTN_B_2,
  JOY_BTN_B_7,
  JOY_BTN_B_6,
  JOY_BTN_B_8,
  JOY_BTN_B_5,
} moo_joys_btn_enum;

typedef enum
{
  JOY_VABS_MAIN_X,
  JOY_VABS_MAIN_Y,
  JOY_VABS_SUB_X,
  JOY_VABS_SUB_Y,
  JOY_VABS_MAIN_Z,
  JOY_VABS_FLAP,
} moo_joys_vabs_enum;

typedef enum
{
  JOY_EVENT_BTN,
  JOY_EVENT_ABS,
} moo_joys_event_type;

typedef enum
{
  SPN_BTN_LEFT,
  SPN_BTN_RIGHT,
} moo_spn_btn_enum;

typedef enum
{
  SPN_REL_X,
  SPN_REL_Y,
  SPN_REL_Z,
  SPN_REL_YANK,
  SPN_REL_PITCH,
  SPN_REL_ROLL,
} moo_spn_rel_enum;

typedef enum
{
  SPN_EVENT_REL,
  SPN_EVENT_BTN,
} moo_spn_event_type;

typedef union
{
  uint32_t b[3];
  struct
  {
    uint8_t torque_mon_fault:1,envelope_fault:1,homing_fault:1,comm_fault:1,amplifier_fault:1,drive_bus_fault:1,
      limit_switch_fault:1,watchdog_fault:1,invalid_frame:1,command_range_error:1,motor_thermal_fault:1,
      low_idle_rate:1,battery_fault:1,actuator_runaway:1,snubber_fault:1,estop:1;
    uint32_t f1:16;

    uint8_t base_at_home:1,thermal_fault_sense:1,amplifier_fault_sense:1,limit_switch_sense:1,limit_shunt_command:1,
      drive_bus_sense:1,amplifier_enable_command:1,estop_sense:1;
    uint32_t f2:24;
    
    uint8_t encoded_machine_state:4,f4:1,mode:2,feedback_type:1,mda_file_number;
    uint32_t f3:16;
  } d;
} __attribute__((packed)) response_datapacket_stc;

typedef struct
{
  hv *pf,*pt;
  float extension;
  hm4 rotm;
  int8_t fail;
} actu_stc;

typedef struct
{
  hv pos,ypr,joints[3][6],from_rotcenter,to_rotcenter,movplat_rotcenter_offs;
  actu_stc actu[6];
  hm4 rotm;
} platf_stc;

typedef struct
{
  uint8_t sleep_flg;
  hv lin,ang;
  moo_algo_type algo;
  float time;
  VALUE special_step_v;
  void *special_step_ptr;
} step_stc;

typedef struct
{
  int n_bins;
  unsigned long *accum_user,*accum_sys;
  struct rusage ru_latest;
} temporito_stc;

static const int commands[]={MOO_CMD_OFF,MOO_CMD_DISABLE,MOO_CMD_PARK,MOO_CMD_LOW_LIMIT_ENABLE,
			     MOO_CMD_LOW_LIMIT_DISABLE,MOO_CMD_ENGAGE,MOO_CMD_START,MOO_CMD_LENGTH_MODE,
			     MOO_CMD_DOF_MODE,MOO_CMD_RESET,MOO_CMD_INHIBIT,MOO_CMD_MDA_MODE,
			     MOO_CMD_NEW_POSITION,MOO_CMD_NEW_MDA,MOO_CMD_NEW_MDA_FILE,-1};
  
static const char *command_labels[]=
{
  "Off","Disable","Park","Low limit enable","Low limit disable","Engage","Start","Length mode","DOF mode","Reset","Inhibit",
  "MDA mode","New position","New mda","New mda file",
  NULL
};
static const char *resp_labels[]=
{
  "*notused*","Estop","Snubber","Actuator runaway","Battery fault","Low idle rate","Motor thrm fault","Range error",
  "Invalid frame","Watchdog","Lim sw fault","Drive bus fault","Ampl fault","Comms fault","Homing fault",
  "Envelope fault","Torque mon fault","Estop sns","Ampl enbl cmd","Drive bus sns","Lim shunt cmd",
  "Lim sw sns","Ampl fault sns","Thrm fault sns","Base at home",
  NULL
};
static const char *mode_labels[]=
{
  "Length mode","DOF mode","MDA mode","SPARE",
  NULL
};
static const char *machine_state_labels[]=
{
  "Power up","Idle","Standby","Engaged","SPARE1","SPARE2","SPARE3",
  "Parking","Fault 1","Fault 2","Fault 3","Disabled","Inhibited",
  "SPARE4","SPARE5","SPARE6",
  NULL
};
static const char *srvr_state_labels[]=
{
  "Off","Idle","Engaging","Engaged","Wait for parking","Parking","Reset","Fault",
  NULL
};
static const char *quicktest_error_labels[]=
{
  "No error","Too short","Too long","Too fast",
  NULL
};

const static uint8_t dof_mapper_to_moog[6]={5,4,1,2,3,0},dof_mapper_to_server[6]={5,2,3,4,1,0};

const static uint16_t moo_joys_buttons[MOO_JOYS_N_BUTTONS]={303,288,290,291,289,294,293,295,292};
const static char *moo_joys_btn_lbls[]={"ACT","B_1","B_3","B_4","B_2","B_7","B_6","B_8","B_5",NULL};
  
const static uint16_t moo_joys_vabs[MOO_JOYS_N_ABS]={0,1,16,17,5,6};
const static uint16_t moo_joys_vab_max[MOO_JOYS_N_ABS]={512,512,1,1,32,128};
const static char *moo_joys_vabs_lbls[]={"MAIN_X","MAIN_Y","SUB_X","SUB_Y","MAIN_Z","FLAP",NULL};

const static uint16_t moo_spn_rel[MOO_SPN_N_RELS]={0,2,1,4,3,5};
const static char *moo_spn_rel_lbls[]={"x","y","z","yank","pitch","roll",NULL};

const static uint16_t moo_spn_buttons[MOO_SPN_N_BUTTONS]={256,257};
const static char *moo_spn_btn_lbls[]={"LEFT","RIGHT",NULL};

extern void lg(const char *fmt,...);

extern void moogidea2serveridea(const float mi[6],hv sl,hv sa);
extern void serveridea2moogidea(const hv sl,const hv sa,float mi[6]);

extern temporito_stc *teto_create(int n_bins);
extern void teto_checkpoint(temporito_stc *s,int cp);
extern void print_teto_results_and_dealloc(temporito_stc *s);

#endif
