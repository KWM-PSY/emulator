/* keyb_input.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Keyboard keypresses handling
 */

#include <maths_include.h>
#include "moo.h"
#include <pthread.h>
#include <poll.h>
#include <linux/input.h>

#define POLL_TIMEOUT 350

typedef struct
{
  char *lbl;
  int keycode;
  uint8_t cur_cond;
} kc_stc;

typedef struct kc_event
{
  kc_stc *kc;
  uint8_t cond;
  struct timespec reftime;  
  struct kc_event *next;
} kc_event_stc;

typedef struct
{
  char *devname,*idname;
  int unit,n_kc;
  kc_stc *kc;
  kc_event_stc *kc_events;

  pthread_t thr;
  pthread_mutex_t mtx;
  uint8_t leave_thread;
} moo_keyb_input_stc;

extern VALUE mod_moo,cls_moo_keyb_input;

static void free_moo_keyb_input(void *p);
static void *ki_thr(void *arg);

VALUE new_moo_keyb_input(VALUE self,VALUE v_devname,VALUE v_keycodes)
{
  moo_keyb_input_stc *s;
  VALUE sdata=Data_Make_Struct(cls_moo_keyb_input,moo_keyb_input_stc,NULL,free_moo_keyb_input,s),v;
  int i;
  kc_stc *kcp;

  bzero(s,sizeof(moo_keyb_input_stc));

  s->n_kc=RARRAY_LEN(v_keycodes);
  s->kc=malloc(sizeof(moo_keyb_input_stc)*s->n_kc);

  for(kcp=s->kc,i=0;i<s->n_kc;i++,kcp++)
  {
    kcp->keycode=FIX2INT(rb_ary_entry(v_keycodes,i));
    kcp->lbl=NULL;
    kcp->cur_cond=0;
  }  

  s->devname=strdup(RSTRING_PTR(v_devname));

  VALUE v_idname=rb_funcall(mod_moo,rb_intern("find_input_device"),1,v_devname);

  if(v_idname==Qfalse)
    rb_raise(rb_eArgError,"%s: Device %s not found",__func__,s->devname);
  s->idname=strdup(RSTRING_PTR(v_idname));

  s->unit=open(s->idname,O_RDONLY);
  if(s->unit<0)
    rb_raise(rb_eArgError,"%s: Device %s (%s) could not be opened (%s)",__func__,
	     s->devname,s->idname,strerror(errno));
  
/*
 * The thread for reading the keypresses
 */

  int sts;
  
  sts=pthread_mutex_init(&s->mtx,NULL);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d creating mutex (%s)",__func__,sts,strerror(errno));
  
  s->leave_thread=0; 

  sts=pthread_create(&s->thr,NULL,ki_thr,s);
  if(sts)
    rb_raise(rb_eArgError,"%s: error #%d starting thread (%s)",__func__,sts,strerror(errno));

  return sdata;  
}

static void free_moo_keyb_input(void *p)
{
  moo_keyb_input_stc *s=(moo_keyb_input_stc *)p;
  int i;
  kc_event_stc *kcep1,*kcep2;
  kc_stc *kcp;

  if(s->thr)
  {
    s->leave_thread=1;
    pthread_join(s->thr,NULL);
  }

  close(s->unit);
  free(s->devname);
  free(s->idname);

  if(s->kc_events)
  {
    for(kcep1=s->kc_events;kcep1;)
    {
      kcep2=kcep1;
      kcep1=kcep2->next;
      free(kcep2);
    }
  }
  
  for(kcp=s->kc,i=0;i<s->n_kc;i++,kcp++)
    free(kcp->lbl);
  free(s->kc);

  free(p);
}

VALUE moo_keyb_feed_labels(VALUE self,VALUE v_lbls)
{
  moo_keyb_input_stc *s;  
  Data_Get_Struct(self,moo_keyb_input_stc,s);

  if(RARRAY_LEN(v_lbls)!=s->n_kc)
    rb_raise(rb_eArgError,"%s: bad array size (%ld)",__func__,RARRAY_LEN(v_lbls));

  int i;
  VALUE v;
  kc_stc *kcp;

  for(kcp=s->kc,i=0;i<s->n_kc;i++,kcp++)
  {
    v=rb_ary_entry(v_lbls,i);

    if(v==Qnil)
    {
      free(kcp->lbl);
      kcp->lbl=NULL;
    }
    else
    {
      kcp->lbl=realloc(kcp->lbl,RSTRING_LEN(v)+1);
      strcpy(kcp->lbl,RSTRING_PTR(v));
    }
  }

  return self;
}  

VALUE moo_keyb_input_poll(VALUE self)
{
  moo_keyb_input_stc *s;  
  Data_Get_Struct(self,moo_keyb_input_stc,s);

  if(!s->kc_events)
    return Qfalse;

  kc_event_stc *kcq,*kcp1,*kcp2;

  pthread_mutex_lock(&s->mtx);
  kcq=s->kc_events;
  s->kc_events=NULL;
  pthread_mutex_unlock(&s->mtx);

  VALUE to_ret=rb_ary_new(),v;

  for(kcp1=kcq;kcp1;)
  {
    kcp2=kcp1;
    kcp1=kcp2->next;

    if(kcp2->kc->lbl)
    {
      v=rb_ary_new_capa(3);
      
      rb_ary_store(v,0,rb_str_new_cstr(kcp2->kc->lbl));
      rb_ary_store(v,1,kcp2->cond ? Qtrue : Qfalse);
      
      rb_ary_store(v,2,rb_str_new((char *)&kcp2->reftime,sizeof(struct timespec)));    
      rb_ary_push(to_ret,v);
    }    
    
    free(kcp2);
  }    

  return to_ret;
}

static void *ki_thr(void *arg)
{
  moo_keyb_input_stc *s=(moo_keyb_input_stc *)arg;
  struct pollfd pfd={s->unit,POLLIN,0};
  struct input_event ie;
  int res,len,i;
  kc_stc *kc;
  kc_event_stc *kce,*kcp;
  uint8_t cond;
  
  while(!s->leave_thread)
  {
    pfd.events=POLLIN;
    res=poll(&pfd,1,POLL_TIMEOUT);

    if(res<0)
    {
      lg("%s: Error in polling (%s)",__func__,strerror(errno));
      break;
    }
    
    if(res!=0)
    {
      len=read(s->unit,&ie,sizeof(struct input_event));
      if(len!=sizeof(struct input_event))
      {
	lg("%s: Error in reading (%s)",__func__,strerror(errno));
	break;
      }
      
      if(ie.type!=EV_KEY || ie.value==2)
	continue;
      
      for(kc=s->kc,i=0;i<s->n_kc;i++,kc++)
	if(kc->keycode==ie.code)
	  break;
      
      if(i<s->n_kc && kc->cur_cond!=ie.value)
      {
	kce=malloc(sizeof(kc_event_stc));
	kce->kc=kc;
	kc->cur_cond=kce->cond=ie.value;

/*
 * time is compared to monotonic value - so we cannot make use of the time stamp included in the
 * input_event stc
 */

	clock_gettime(CLOCK_MONOTONIC,&kce->reftime);
	kce->next=NULL;
	
	pthread_mutex_lock(&s->mtx);

	if(!s->kc_events)
	  s->kc_events=kce;
	else
	{
	  for(kcp=s->kc_events;kcp->next;kcp=kcp->next)
	    ;
	  kcp->next=kce;
	}

	pthread_mutex_unlock(&s->mtx);
      }      
    }
  }

  lg("Keyboard input out.");
    
  return NULL;
}


