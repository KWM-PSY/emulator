/* init.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Specific moog-oriented extension - definitions and declarations
 */

/* init.c */

#include <ctype.h>
#include <carlobase.h>
#include <maths_include.h>
#include "../threed/threed.h"
#include "moo.h"

#define LN 16

VALUE mod_moo,cls_moo_platf,
  cls_moo_server,cls_moo_keyb_input,cls_moo_joystick,cls_moo_spnav,
  cls_moo_xsense,cls_moo_udplistener;

VALUE moo_response_parser(VALUE self,VALUE v_bfr);
static VALUE moo_compose_response(VALUE self,VALUE v_dof,VALUE v_feedback_type,VALUE v_mode,VALUE v_state,VALUE v_flgs);
static VALUE moo_time_now(VALUE self);
static VALUE moo_time_diff(VALUE self,VALUE v_t1,VALUE v_t2);
static VALUE moo_time_diff_usec(VALUE self,VALUE v_t1,VALUE v_t2);
static VALUE moo_time_diff_from_now(VALUE self,VALUE v_t);
static VALUE moo_time_add(VALUE self,VALUE v_t,VALUE v_sec);
static VALUE moo_msec_from_timespec(VALUE self,VALUE v_ts);
static VALUE moo_hexprint(VALUE self,VALUE v_tag,VALUE v_s);
static VALUE moo_cmd_desc(VALUE self,VALUE v_cmd);
static VALUE moo_open_logfile(VALUE self,VALUE lp_v,VALUE nm_v);
static VALUE moo_core_unlimited(VALUE self);
static VALUE moo_set_stack_size(VALUE self,VALUE v_size);
static VALUE moo_quick_verify(VALUE self,VALUE v_plat,VALUE v_rotcenter,VALUE v_beg_lin,VALUE v_beg_ang,VALUE v_steps,VALUE v_timestep,VALUE v_max_allowed_val);
static VALUE moo_moog_idea_to_server_idea(VALUE self,VALUE v_a);
static VALUE moo_server_idea_to_moog_idea(VALUE self,VALUE v_a);

extern VALUE new_moo_platf(VALUE self,VALUE v_base,VALUE v_rotbase,VALUE v_rotcenter,VALUE v_actu_min,VALUE v_actu_max,VALUE v_actu_park);
extern VALUE moo_platf_move(VALUE self,VALUE v_vals);
extern VALUE moo_platf_try_motion(VALUE self,VALUE v_vfrom,VALUE v_vto,VALUE v_nsteps);

extern VALUE new_moo_server(VALUE self,VALUE v_lower_joints,VALUE v_upper_joints,VALUE v_rotcenter,
			    VALUE v_loc_addr,VALUE v_rem_addr,VALUE v_inport,VALUE v_outport,VALUE v_logflg);
extern VALUE moo_server_close(VALUE self);
extern VALUE moo_server_feed_action(VALUE self,VALUE v_dof,VALUE v_time,VALUE v_algo,VALUE v_override);
extern VALUE moo_server_feed_action_speed(VALUE self,VALUE v_dof,VALUE v_speed);
extern VALUE moo_server_feed_immediate(VALUE self,VALUE v_dof,VALUE v_max_lin,VALUE v_max_ang);
extern VALUE moo_server_feed_immediate_regardless(VALUE self,VALUE v_dof);
extern VALUE moo_server_freeze(VALUE self);
extern VALUE moo_server_cur_sts(VALUE self);
extern VALUE moo_server_engage(VALUE self,VALUE v_op_mode);
extern VALUE moo_server_park(VALUE self);
extern VALUE moo_server_cur_rotcenter(VALUE self);
extern VALUE moo_server_modify_rotcenter(VALUE self,VALUE v_rotcenter);
extern VALUE moo_server_get_actu_l(VALUE self,VALUE v_dof);

extern VALUE new_moo_keyb_input(VALUE self,VALUE v_devname,VALUE v_keycodes);
extern VALUE moo_keyb_input_poll(VALUE self);
extern VALUE moo_keyb_feed_labels(VALUE self,VALUE v_lbls);

extern VALUE new_moo_joystick(VALUE self,VALUE v_devname);
extern VALUE moo_joystick_poll(VALUE self);
extern VALUE moo_joystick_close_thread(VALUE self);

extern VALUE new_moo_spnav(VALUE self,VALUE v_devname);
extern VALUE moo_spnav_poll(VALUE self);

extern VALUE new_moo_xsense(VALUE self,VALUE v_devname);
extern VALUE moo_xsense_send_msg(VALUE self,VALUE v_msg_id,VALUE v_payload);
extern VALUE moo_xsense_poll(VALUE self);
extern VALUE moo_xsense_close_thread(VALUE self);
  
extern VALUE new_moo_udplistener(VALUE self,VALUE v_loc_addr,VALUE v_portin);
extern VALUE moo_udplistener_sendto(VALUE self,VALUE v_addr,VALUE v_port,VALUE v_pkt);
extern VALUE moo_udplistener_poll(VALUE self);
extern VALUE moo_udplistener_ping(VALUE self,VALUE v_loc_addr,VALUE v_loc_port,VALUE v_payload);

static const char *command_desc(int cmd);

extern void apply_motion(platf_stc *p);
extern void build_platform(float *bv,float *bvr,hv rotcenter,platf_stc *p);

void Init_moo(void)
{
  VALUE v;
  char **ptr;
  
  mod_moo=rb_define_module("Moo");

  rb_define_const(mod_moo,"TSTEP_SEC",DBL2NUM(TSTEP_SEC));
  rb_define_const(mod_moo,"TSTEP_USEC",INT2FIX(TSTEP_USEC));

  rb_define_const(mod_moo,"CMD_OFF",INT2FIX(MOO_CMD_OFF));
  rb_define_const(mod_moo,"CMD_DISABLE",INT2FIX(MOO_CMD_DISABLE));
  rb_define_const(mod_moo,"CMD_PARK",INT2FIX(MOO_CMD_PARK));
  rb_define_const(mod_moo,"CMD_LOW_LIMIT_ENABLE",INT2FIX(MOO_CMD_LOW_LIMIT_ENABLE));
  rb_define_const(mod_moo,"CMD_LOW_LIMIT_DISABLE",INT2FIX(MOO_CMD_LOW_LIMIT_DISABLE));
  rb_define_const(mod_moo,"CMD_ENGAGE",INT2FIX(MOO_CMD_ENGAGE));
  rb_define_const(mod_moo,"CMD_START",INT2FIX(MOO_CMD_START));
  rb_define_const(mod_moo,"CMD_LENGTH_MODE",INT2FIX(MOO_CMD_LENGTH_MODE));
  rb_define_const(mod_moo,"CMD_DOF_MODE",INT2FIX(MOO_CMD_DOF_MODE));
  rb_define_const(mod_moo,"CMD_RESET",INT2FIX(MOO_CMD_RESET));
  rb_define_const(mod_moo,"CMD_INHIBIT",INT2FIX(MOO_CMD_INHIBIT));
  rb_define_const(mod_moo,"CMD_MDA_MODE",INT2FIX(MOO_CMD_MDA_MODE));
  rb_define_const(mod_moo,"CMD_NEW_POSITION",INT2FIX(MOO_CMD_NEW_POSITION));
  rb_define_const(mod_moo,"CMD_NEW_MDA",INT2FIX(MOO_CMD_NEW_MDA));
  rb_define_const(mod_moo,"CMD_NEW_MDA_FILE",INT2FIX(MOO_CMD_NEW_MDA_FILE));

  rb_define_const(mod_moo,"RESP_ESTOP",INT2FIX(MOO_RESP_ESTOP));
  rb_define_const(mod_moo,"RESP_SNUBBER",INT2FIX(MOO_RESP_SNUBBER));
  rb_define_const(mod_moo,"RESP_ACTU_RUNAWAY",INT2FIX(MOO_RESP_ACTU_RUNAWAY));
  rb_define_const(mod_moo,"RESP_BATT_FAULT",INT2FIX(MOO_RESP_BATT_FAULT));
  rb_define_const(mod_moo,"RESP_LOW_IDLE_RATE",INT2FIX(MOO_RESP_LOW_IDLE_RATE));
  rb_define_const(mod_moo,"RESP_MOTOR_THERMAL_FAULT",INT2FIX(MOO_RESP_MOTOR_THERMAL_FAULT));
  rb_define_const(mod_moo,"RESP_RANGE_ERROR",INT2FIX(MOO_RESP_RANGE_ERROR));
  rb_define_const(mod_moo,"RESP_INVALID_FRAME",INT2FIX(MOO_RESP_INVALID_FRAME));
  rb_define_const(mod_moo,"RESP_WATCHDOG",INT2FIX(MOO_RESP_WATCHDOG));
  rb_define_const(mod_moo,"RESP_LIMIT_SWITCH_FAULT",INT2FIX(MOO_RESP_LIMIT_SWITCH_FAULT));
  rb_define_const(mod_moo,"RESP_DRIVE_BUS_FAULT",INT2FIX(MOO_RESP_DRIVE_BUS_FAULT));
  rb_define_const(mod_moo,"RESP_AMPLIFIER_FAULT",INT2FIX(MOO_RESP_AMPLIFIER_FAULT));
  rb_define_const(mod_moo,"RESP_COMM_FAULT",INT2FIX(MOO_RESP_COMM_FAULT));
  rb_define_const(mod_moo,"RESP_HOMING_FAULT",INT2FIX(MOO_RESP_HOMING_FAULT));
  rb_define_const(mod_moo,"RESP_ENVELOPE_FAULT",INT2FIX(MOO_RESP_ENVELOPE_FAULT));
  rb_define_const(mod_moo,"RESP_TORQUE_MON_FAULT",INT2FIX(MOO_RESP_TORQUE_MON_FAULT));
  rb_define_const(mod_moo,"RESP_ESTOP_SENSE",INT2FIX(MOO_RESP_ESTOP_SENSE));
  rb_define_const(mod_moo,"RESP_AMPLI_ENABLE",INT2FIX(MOO_RESP_AMPLI_ENABLE));
  rb_define_const(mod_moo,"RESP_DRIVE_BUS_SENSE",INT2FIX(MOO_RESP_DRIVE_BUS_SENSE));
  rb_define_const(mod_moo,"RESP_LIMIT_SHUNT",INT2FIX(MOO_RESP_LIMIT_SHUNT));
  rb_define_const(mod_moo,"RESP_LIMIT_SWITCH_SENSE",INT2FIX(MOO_RESP_LIMIT_SWITCH_SENSE));
  rb_define_const(mod_moo,"RESP_AMPLI_FAULT",INT2FIX(MOO_RESP_AMPLI_FAULT));
  rb_define_const(mod_moo,"RESP_THERMAL_FAULT_SENSE",INT2FIX(MOO_RESP_THERMAL_FAULT_SENSE));
  rb_define_const(mod_moo,"RESP_BASE_AT_HOME",INT2FIX(MOO_RESP_BASE_AT_HOME));

  rb_define_const(mod_moo,"N_ACTU",INT2FIX(MOO_N_ACTU));
  rb_define_const(mod_moo,"ACTU_MIN",DBL2NUM(MOO_ACTU_MIN));
  rb_define_const(mod_moo,"ACTU_IDLE",DBL2NUM(MOO_ACTU_IDLE));
  rb_define_const(mod_moo,"ACTU_MAX",DBL2NUM(MOO_ACTU_MAX));
  rb_define_const(mod_moo,"ACTU_PARK",DBL2NUM(MOO_ACTU_PARK));
  rb_define_const(mod_moo,"ACTU_FIXED",DBL2NUM(MOO_ACTU_FIXED));
  rb_define_const(mod_moo,"ACTU_MIN_MM",DBL2NUM(MOO_ACTU_MIN_MM));
  rb_define_const(mod_moo,"ACTU_IDLE_MM",DBL2NUM(MOO_ACTU_IDLE_MM));
  rb_define_const(mod_moo,"ACTU_MAX_MM",DBL2NUM(MOO_ACTU_MAX_MM));
  rb_define_const(mod_moo,"ACTU_PARK_MM",DBL2NUM(MOO_ACTU_PARK_MM));
  rb_define_const(mod_moo,"ACTU_FIXED_MM",DBL2NUM(MOO_ACTU_FIXED_MM));
  rb_define_const(mod_moo,"ACTU_MIN_H",DBL2NUM(MOO_ACTU_MIN_H));
  rb_define_const(mod_moo,"ACTU_IDLE_H",DBL2NUM(MOO_ACTU_IDLE_H));
  rb_define_const(mod_moo,"ACTU_MAX_H",DBL2NUM(MOO_ACTU_MAX_H));

//  rb_define_const(mod_moo,"ACTU_MOOG_IDLE",DBL2NUM(MOO_ACTU_MOOG_IDLE));
//  rb_define_const(mod_moo,"ACTU_DIFF",DBL2NUM(MOO_ACTU_DIFF));
  rb_define_const(mod_moo,"MOOG_H_TO_SRV_H",DBL2NUM(MOO_MOOG_H_TO_SRV_H));

  rb_define_const(mod_moo,"LX_MIN",DBL2NUM(MOO_LX_MIN));
  rb_define_const(mod_moo,"LX_MAX",DBL2NUM(MOO_LX_MAX));
  rb_define_const(mod_moo,"LX_PARK",DBL2NUM(MOO_LX_PARK));
  rb_define_const(mod_moo,"LY_MIN",DBL2NUM(MOO_LY_MIN));
  rb_define_const(mod_moo,"LY_MAX",DBL2NUM(MOO_LY_MAX));
  rb_define_const(mod_moo,"LY_PARK",DBL2NUM(MOO_LY_PARK));
  rb_define_const(mod_moo,"LZ_MIN",DBL2NUM(MOO_LZ_MIN));
  rb_define_const(mod_moo,"LZ_MAX",DBL2NUM(MOO_LZ_MAX));
  rb_define_const(mod_moo,"LZ_PARK",DBL2NUM(MOO_LZ_PARK));
  rb_define_const(mod_moo,"YAW_MIN",DBL2NUM(MOO_YAW_MIN));
  rb_define_const(mod_moo,"YAW_MAX",DBL2NUM(MOO_YAW_MAX));
  rb_define_const(mod_moo,"YAW_PARK",DBL2NUM(MOO_YAW_PARK));
  rb_define_const(mod_moo,"PITCH_MIN",DBL2NUM(MOO_PITCH_MIN));
  rb_define_const(mod_moo,"PITCH_MAX",DBL2NUM(MOO_PITCH_MAX));
  rb_define_const(mod_moo,"PITCH_PARK",DBL2NUM(MOO_PITCH_PARK));
  rb_define_const(mod_moo,"ROLL_MIN",DBL2NUM(MOO_ROLL_MIN));
  rb_define_const(mod_moo,"ROLL_MAX",DBL2NUM(MOO_ROLL_MAX));
  rb_define_const(mod_moo,"ROLL_PARK",DBL2NUM(MOO_ROLL_PARK));

  rb_define_const(mod_moo,"MODE_LENGTH",INT2FIX(MOO_MODE_LENGTH));
  rb_define_const(mod_moo,"MODE_DOF",INT2FIX(MOO_MODE_DOF));
  rb_define_const(mod_moo,"MODE_MDA",INT2FIX(MOO_MODE_MDA));
  rb_define_const(mod_moo,"MODE_SPARE",INT2FIX(MOO_MODE_SPARE));

  rb_define_const(mod_moo,"ALG_LIN",INT2FIX(ALG_LIN));
  rb_define_const(mod_moo,"ALG_SIN",INT2FIX(ALG_SIN));
  rb_define_const(mod_moo,"ALG_CASTELJAU",INT2FIX(ALG_CASTELJAU));
  rb_define_const(mod_moo,"ALG_SINEACC",INT2FIX(ALG_SINEACC));
  rb_define_const(mod_moo,"ALG_DAMPEDOSC",INT2FIX(ALG_DAMPEDOSC));

  rb_define_const(mod_moo,"MACH_STATE_POWERUP",INT2FIX(MOO_MACH_STATE_POWERUP));
  rb_define_const(mod_moo,"MACH_STATE_IDLE",INT2FIX(MOO_MACH_STATE_IDLE));
  rb_define_const(mod_moo,"MACH_STATE_STBY",INT2FIX(MOO_MACH_STATE_STBY));
  rb_define_const(mod_moo,"MACH_STATE_ENGAGED",INT2FIX(MOO_MACH_STATE_ENGAGED));
  rb_define_const(mod_moo,"MACH_STATE_PARKING",INT2FIX(MOO_MACH_STATE_PARKING));
  rb_define_const(mod_moo,"MACH_STATE_FAULT1",INT2FIX(MOO_MACH_STATE_FAULT1));
  rb_define_const(mod_moo,"MACH_STATE_FAULT2",INT2FIX(MOO_MACH_STATE_FAULT2));
  rb_define_const(mod_moo,"MACH_STATE_FAULT3",INT2FIX(MOO_MACH_STATE_FAULT3));
  rb_define_const(mod_moo,"MACH_STATE_DISABLED",INT2FIX(MOO_MACH_STATE_DISABLED));
  rb_define_const(mod_moo,"MACH_STATE_INHIBITED",INT2FIX(MOO_MACH_STATE_INHIBITED));

  rb_define_const(mod_moo,"SRVR_STATE_IDLE",INT2FIX(MOO_SRVR_STATE_IDLE));
  rb_define_const(mod_moo,"SRVR_STATE_ENGAGING",INT2FIX(MOO_SRVR_STATE_ENGAGING));
  rb_define_const(mod_moo,"SRVR_STATE_ENGAGED",INT2FIX(MOO_SRVR_STATE_ENGAGED));
  rb_define_const(mod_moo,"SRVR_STATE_PARKING",INT2FIX(MOO_SRVR_STATE_PARKING));
  rb_define_const(mod_moo,"SRVR_STATE_FAULT",INT2FIX(MOO_SRVR_STATE_FAULT));

  rb_define_const(mod_moo,"QT_NO_ERROR",INT2FIX(QT_NO_ERROR));
  rb_define_const(mod_moo,"QT_ERROR_SHORT",INT2FIX(QT_ERROR_SHORT));
  rb_define_const(mod_moo,"QT_ERROR_LONG",INT2FIX(QT_ERROR_LONG));
  rb_define_const(mod_moo,"QT_ERROR_FAST",INT2FIX(QT_ERROR_FAST));

  v=rb_ary_new();
  for(ptr=(char **)command_labels;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(mod_moo,"COMMAND_LABELS",v);
  v=rb_ary_new();
  for(ptr=(char **)resp_labels;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(mod_moo,"RESP_LABELS",v);
  v=rb_ary_new();
  for(ptr=(char **)mode_labels;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(mod_moo,"MODE_LABELS",v);
  v=rb_ary_new();
  for(ptr=(char **)machine_state_labels;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(mod_moo,"MACH_STATE_LABELS",v);
  v=rb_ary_new();
  for(ptr=(char **)srvr_state_labels;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(mod_moo,"SRVR_STATE_LABELS",v);
  v=rb_ary_new();
  for(ptr=(char **)quicktest_error_labels;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(mod_moo,"QUICKTEST_ERROR_LABELS",v);
  
  rb_define_singleton_method(mod_moo,"response_parser",moo_response_parser,1);
  rb_define_singleton_method(mod_moo,"compose_response",moo_compose_response,5);
  rb_define_singleton_method(mod_moo,"time_now",moo_time_now,0);
  rb_define_singleton_method(mod_moo,"time_diff",moo_time_diff,2);
  rb_define_singleton_method(mod_moo,"time_diff_usec",moo_time_diff_usec,2);
  rb_define_singleton_method(mod_moo,"time_diff_from_now",moo_time_diff_from_now,1);
  rb_define_singleton_method(mod_moo,"time_add",moo_time_add,2);
  rb_define_singleton_method(mod_moo,"msec_from_timespec",moo_msec_from_timespec,1);
  rb_define_singleton_method(mod_moo,"hexprint",moo_hexprint,2);
  rb_define_singleton_method(mod_moo,"cmd_desc",moo_cmd_desc,1);
  rb_define_singleton_method(mod_moo,"open_logfile",moo_open_logfile,2);
  rb_define_singleton_method(mod_moo,"core_unlimited",moo_core_unlimited,0);
  rb_define_singleton_method(mod_moo,"set_stack_size",moo_set_stack_size,1);
  rb_define_singleton_method(mod_moo,"quick_verify",moo_quick_verify,7);
  rb_define_singleton_method(mod_moo,"moog_idea_to_server_idea",moo_moog_idea_to_server_idea,1);
  rb_define_singleton_method(mod_moo,"server_idea_to_moog_idea",moo_server_idea_to_moog_idea,1);
  
  cls_moo_platf=rb_define_class_under(mod_moo,"Platf",rb_cObject);
  rb_define_singleton_method(cls_moo_platf,"new",new_moo_platf,6);
  rb_define_method(cls_moo_platf,"move",moo_platf_move,1);
  rb_define_method(cls_moo_platf,"try_motion",moo_platf_try_motion,3);

  cls_moo_server=rb_define_class_under(mod_moo,"Server",rb_cObject);
  rb_define_singleton_method(cls_moo_server,"new",new_moo_server,8);
  rb_define_method(cls_moo_server,"close",moo_server_close,0);
  rb_define_method(cls_moo_server,"feed_action",moo_server_feed_action,4);
  rb_define_method(cls_moo_server,"feed_action_speed",moo_server_feed_action_speed,2);
  rb_define_method(cls_moo_server,"feed_immediate",moo_server_feed_immediate,3);
  rb_define_method(cls_moo_server,"feed_immediate_regardless",moo_server_feed_immediate_regardless,1);
  rb_define_method(cls_moo_server,"freeze",moo_server_freeze,0);
  rb_define_method(cls_moo_server,"cur_sts",moo_server_cur_sts,0);
  rb_define_method(cls_moo_server,"engage",moo_server_engage,1);
  rb_define_method(cls_moo_server,"park",moo_server_park,0);
  rb_define_method(cls_moo_server,"cur_rotcenter",moo_server_cur_rotcenter,0);
  rb_define_method(cls_moo_server,"modify_rotcenter",moo_server_modify_rotcenter,1);
  rb_define_method(cls_moo_server,"get_actuator_lengths",moo_server_get_actu_l,1);

  cls_moo_keyb_input=rb_define_class_under(mod_moo,"Keyb_input",rb_cObject);
  rb_define_singleton_method(cls_moo_keyb_input,"new",new_moo_keyb_input,2);
  rb_define_method(cls_moo_keyb_input,"poll",moo_keyb_input_poll,0);
  rb_define_method(cls_moo_keyb_input,"feed_labels",moo_keyb_feed_labels,1);

  cls_moo_joystick=rb_define_class_under(mod_moo,"Joystick",rb_cObject);
  rb_define_const(cls_moo_joystick,"BTN_ACT",INT2FIX(JOY_BTN_ACT));
  rb_define_const(cls_moo_joystick,"BTN_B_1",INT2FIX(JOY_BTN_B_1));
  rb_define_const(cls_moo_joystick,"BTN_B_2",INT2FIX(JOY_BTN_B_2));
  rb_define_const(cls_moo_joystick,"BTN_B_3",INT2FIX(JOY_BTN_B_3));
  rb_define_const(cls_moo_joystick,"BTN_B_4",INT2FIX(JOY_BTN_B_4));
  rb_define_const(cls_moo_joystick,"BTN_B_5",INT2FIX(JOY_BTN_B_5));
  rb_define_const(cls_moo_joystick,"BTN_B_6",INT2FIX(JOY_BTN_B_6));
  rb_define_const(cls_moo_joystick,"BTN_B_7",INT2FIX(JOY_BTN_B_7));
  rb_define_const(cls_moo_joystick,"BTN_B_8",INT2FIX(JOY_BTN_B_8));
  rb_define_const(cls_moo_joystick,"VABS_MAIN_X",INT2FIX(JOY_VABS_MAIN_X));
  rb_define_const(cls_moo_joystick,"VABS_MAIN_Y",INT2FIX(JOY_VABS_MAIN_Y));
  rb_define_const(cls_moo_joystick,"VABS_MAIN_Z",INT2FIX(JOY_VABS_MAIN_Z));
  rb_define_const(cls_moo_joystick,"VABS_SUB_X",INT2FIX(JOY_VABS_SUB_X));
  rb_define_const(cls_moo_joystick,"VABS_SUB_Y",INT2FIX(JOY_VABS_SUB_Y));
  rb_define_const(cls_moo_joystick,"VABS_FLAP",INT2FIX(JOY_VABS_FLAP));
  rb_define_const(cls_moo_joystick,"EVENT_BTN",INT2FIX(JOY_EVENT_BTN));
  rb_define_const(cls_moo_joystick,"EVENT_ABS",INT2FIX(JOY_EVENT_ABS));
  rb_define_singleton_method(cls_moo_joystick,"new",new_moo_joystick,1);
  rb_define_method(cls_moo_joystick,"poll",moo_joystick_poll,0);
  rb_define_method(cls_moo_joystick,"close_thread",moo_joystick_close_thread,0);

  v=rb_ary_new();
  for(ptr=(char **)moo_joys_btn_lbls;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(cls_moo_joystick,"BTN_LBLS",v);
  v=rb_ary_new();
  for(ptr=(char **)moo_joys_vabs_lbls;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(cls_moo_joystick,"VABS_LBLS",v);

  cls_moo_spnav=rb_define_class_under(mod_moo,"Spnav",rb_cObject);
  rb_define_const(cls_moo_spnav,"BTN_LEFT",INT2FIX(SPN_BTN_LEFT));
  rb_define_const(cls_moo_spnav,"BTN_RIGHT",INT2FIX(SPN_BTN_RIGHT));
  rb_define_singleton_method(cls_moo_spnav,"new",new_moo_spnav,1);
  rb_define_method(cls_moo_spnav,"poll",moo_spnav_poll,0);

  v=rb_ary_new();
  for(ptr=(char **)moo_spn_rel_lbls;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(cls_moo_spnav,"REL_LBLS",v);
  v=rb_ary_new();
  for(ptr=(char **)moo_spn_btn_lbls;(*ptr);ptr++)
    rb_ary_push(v,rb_str_new_cstr(*ptr));
  rb_define_const(cls_moo_spnav,"BTN_LBLS",v);

  cls_moo_xsense=rb_define_class_under(mod_moo,"Xsense",rb_cObject);
  rb_define_singleton_method(cls_moo_xsense,"new",new_moo_xsense,1);
  rb_define_method(cls_moo_xsense,"send_msg",moo_xsense_send_msg,2);
  rb_define_method(cls_moo_xsense,"poll",moo_xsense_poll,0);
  rb_define_method(cls_moo_xsense,"close_thread",moo_xsense_close_thread,0);

  cls_moo_udplistener=rb_define_class_under(mod_moo,"Udplistener",rb_cObject);
  rb_define_singleton_method(cls_moo_udplistener,"new",new_moo_udplistener,2);
  rb_define_method(cls_moo_udplistener,"sendto",moo_udplistener_sendto,3);
  rb_define_method(cls_moo_udplistener,"poll",moo_udplistener_poll,0);
  rb_define_singleton_method(cls_moo_udplistener,"ping",moo_udplistener_ping,3);
}

VALUE moo_response_parser(VALUE self,VALUE v_bfr)
{
  response_datapacket_stc response;

  memcpy(&response,RSTRING_PTR(v_bfr),sizeof(response_datapacket_stc));

  VALUE to_ret=rb_ary_new_capa(3);
  VALUE v=rb_ary_new();

  if(response.d.estop)
    rb_ary_push(v,INT2FIX(MOO_RESP_ESTOP));
  if(response.d.snubber_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_SNUBBER));
  if(response.d.actuator_runaway)
    rb_ary_push(v,INT2FIX(MOO_RESP_ACTU_RUNAWAY));
  if(response.d.battery_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_BATT_FAULT));
  if(response.d.low_idle_rate)
    rb_ary_push(v,INT2FIX(MOO_RESP_LOW_IDLE_RATE));
  if(response.d.motor_thermal_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_MOTOR_THERMAL_FAULT));
  if(response.d.command_range_error)
    rb_ary_push(v,INT2FIX(MOO_RESP_RANGE_ERROR));
  if(response.d.invalid_frame)
    rb_ary_push(v,INT2FIX(MOO_RESP_INVALID_FRAME));
  if(response.d.watchdog_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_WATCHDOG));
  if(response.d.limit_switch_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_LIMIT_SWITCH_FAULT));
  if(response.d.drive_bus_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_DRIVE_BUS_FAULT));
  if(response.d.amplifier_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_AMPLIFIER_FAULT));
  if(response.d.comm_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_COMM_FAULT));
  if(response.d.homing_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_HOMING_FAULT));
  if(response.d.envelope_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_ENVELOPE_FAULT));
  if(response.d.torque_mon_fault)
    rb_ary_push(v,INT2FIX(MOO_RESP_TORQUE_MON_FAULT));
  if(response.d.estop_sense)
    rb_ary_push(v,INT2FIX(MOO_RESP_ESTOP_SENSE));
  if(response.d.amplifier_enable_command)
    rb_ary_push(v,INT2FIX(MOO_RESP_AMPLI_ENABLE));
  if(response.d.drive_bus_sense)
    rb_ary_push(v,INT2FIX(MOO_RESP_DRIVE_BUS_SENSE));
  if(response.d.limit_shunt_command)
    rb_ary_push(v,INT2FIX(MOO_RESP_LIMIT_SHUNT));
  if(response.d.limit_switch_sense)
    rb_ary_push(v,INT2FIX(MOO_RESP_LIMIT_SWITCH_SENSE));
  if(response.d.amplifier_fault_sense)
    rb_ary_push(v,INT2FIX(MOO_RESP_AMPLI_FAULT));
  if(response.d.thermal_fault_sense)
    rb_ary_push(v,INT2FIX(MOO_RESP_THERMAL_FAULT_SENSE));
  if(response.d.base_at_home)
    rb_ary_push(v,INT2FIX(MOO_RESP_BASE_AT_HOME));

  rb_ary_store(to_ret,0,v);
  rb_ary_store(to_ret,1,INT2FIX(response.d.feedback_type));
  rb_ary_store(to_ret,2,INT2FIX(response.d.mode));
  rb_ary_store(to_ret,3,INT2FIX(response.d.encoded_machine_state));

  return to_ret;
}

static VALUE moo_compose_response(VALUE self,VALUE v_dof,VALUE v_feedback_type,VALUE v_mode,VALUE v_state,VALUE v_flgs)
{
  response_datapacket_stc r;
  VALUE v;
  int n_flgs=RARRAY_LEN(v_flgs),i,f;

  bzero(&r,sizeof(response_datapacket_stc));

  r.d.feedback_type=FIX2INT(v_feedback_type);
  r.d.mode=FIX2INT(v_mode);
  r.d.encoded_machine_state=FIX2INT(v_state);

  for(i=0;i<n_flgs;i++)
  {
    f=FIX2INT(rb_ary_entry(v_flgs,i));
    switch(f)
    {
    case MOO_RESP_ESTOP:
      r.d.estop=1;
      break;
    case MOO_RESP_SNUBBER:
      r.d.snubber_fault=1;
      break;
    case MOO_RESP_ACTU_RUNAWAY:
      r.d.actuator_runaway=1;
      break;
    case MOO_RESP_BATT_FAULT:
      r.d.battery_fault=1;
      break;
    case MOO_RESP_LOW_IDLE_RATE:
      r.d.low_idle_rate=1;
      break;
    case MOO_RESP_MOTOR_THERMAL_FAULT:
      r.d.motor_thermal_fault=1;
      break;
    case MOO_RESP_RANGE_ERROR:
      r.d.command_range_error=1;
      break;
    case MOO_RESP_INVALID_FRAME:
      r.d.invalid_frame=1;
      break;
    case MOO_RESP_WATCHDOG:
      r.d.watchdog_fault=1;
      break;
    case MOO_RESP_LIMIT_SWITCH_FAULT:
      r.d.limit_switch_fault=1;
      break;
    case MOO_RESP_DRIVE_BUS_FAULT:
      r.d.drive_bus_fault=1;
      break;
    case MOO_RESP_AMPLIFIER_FAULT:
      r.d.amplifier_fault=1;
      break;
    case MOO_RESP_COMM_FAULT:
      r.d.comm_fault=1;
      break;
    case MOO_RESP_HOMING_FAULT:
      r.d.homing_fault=1;
      break;
    case MOO_RESP_ENVELOPE_FAULT:
      r.d.envelope_fault=1;
      break;
    case MOO_RESP_TORQUE_MON_FAULT:
      r.d.torque_mon_fault=1;
      break;
    case MOO_RESP_ESTOP_SENSE:
      r.d.estop_sense=1;
      break;
    case MOO_RESP_AMPLI_ENABLE:
      r.d.amplifier_enable_command=1;
      break;
    case MOO_RESP_DRIVE_BUS_SENSE:
      r.d.drive_bus_sense=1;
      break;
    case MOO_RESP_LIMIT_SHUNT:
      r.d.limit_shunt_command=1;
      break;
    case MOO_RESP_LIMIT_SWITCH_SENSE:
      r.d.limit_switch_sense=1;
      break;
    case MOO_RESP_AMPLI_FAULT:
      r.d.amplifier_fault=1;
      break;
    case MOO_RESP_THERMAL_FAULT_SENSE:
      r.d.thermal_fault_sense=1;
      break;
    case MOO_RESP_BASE_AT_HOME:
      r.d.base_at_home=1;
      break;
    default:
      rb_raise(rb_eArgError,"%s: Bad flag (%d)!",__func__,f);
    }
  }
  
  uint32_t bfr[10];
  hv sl,sa;

  memcpy(bfr,r.b,sizeof(response_datapacket_stc));
  
  for(i=0;i<3;i++)
  {
    sl[i]=NUM2DBL(rb_ary_entry(v_dof,i));
    sa[i]=NUM2DBL(rb_ary_entry(v_dof,i+3));
  }
//  fprintf(stdout,"\rDOF x %10.3f y %10.3f z %10.3f || y %10.3f p %10.3f r %10.3f",
//	  sl[0],sl[1],sl[2],sa[0]*180.0/M_PI,sa[1]*180.0/M_PI,sa[2]*180.0/M_PI);
  
  float *fp=(float *)(bfr+3);

  serveridea2moogidea(sl,sa,fp);

  for(i=0;i<10;i++)
    bfr[i]=htonl(bfr[i]);

  return rb_str_new((char *)bfr,sizeof(uint32_t)*10);
}

static VALUE moo_time_now(VALUE self)
{
  time_t t1;
  struct timespec t2;  

  t1=time(NULL);
  clock_gettime(CLOCK_MONOTONIC,&t2);  

  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,rb_str_new((char *)&t1,sizeof(time_t)));
  rb_ary_store(to_ret,1,rb_str_new((char *)&t2,sizeof(struct timespec)));

  return to_ret;
}

static VALUE moo_time_diff(VALUE self,VALUE v_t1,VALUE v_t2)
{
  struct timespec *t1=(struct timespec *)RSTRING_PTR(v_t1),*t2=(struct timespec *)RSTRING_PTR(v_t2);

  return INT2NUM(timediff(t1,t2));
}

static VALUE moo_time_diff_usec(VALUE self,VALUE v_t1,VALUE v_t2)
{
  struct timespec *t1=(struct timespec *)RSTRING_PTR(v_t1),*t2=(struct timespec *)RSTRING_PTR(v_t2);

  return INT2NUM(timediff_usec(t1,t2));
}

static VALUE moo_time_diff_from_now(VALUE self,VALUE v_t)
{
  struct timespec *t=(struct timespec *)RSTRING_PTR(v_t),now;  

  clock_gettime(CLOCK_MONOTONIC,&now);

  return INT2NUM(timediff(t,&now));
}

static VALUE moo_time_add(VALUE self,VALUE v_t,VALUE v_sec)
{
  struct timespec t;
  float sec=NUM2DBL(v_sec);

  memcpy(&t,RSTRING_PTR(v_t),sizeof(struct timespec));
  timeadd(&t,(unsigned long long int)(sec*1000000.0));

  return rb_str_new((char *)&t,sizeof(struct timespec));
}

static VALUE moo_msec_from_timespec(VALUE self,VALUE v_ts)
{
  struct timespec *ts=(struct timespec *)RSTRING_PTR(v_ts);

  return INT2NUM(ts->tv_sec*1000+ts->tv_nsec/1000000LL);
}

static VALUE moo_hexprint(VALUE self,VALUE v_tag,VALUE v_s)
{
  lg("%s: [%s]",__func__,RSTRING_PTR(v_tag));
  hexprint(RSTRING_PTR(v_s),RSTRING_LEN(v_s));

  return self;
}

static VALUE moo_cmd_desc(VALUE self,VALUE v_cmd)
{
  int cmd=FIX2INT(v_cmd);

  return rb_str_new_cstr(command_desc(cmd));
}

static VALUE moo_open_logfile(VALUE self,VALUE lp_v,VALUE nm_v)
{
  char bfr1[512],bfr2[512],*lp,*nm;
  time_t now=time(NULL);
  struct tm *tp=localtime(&now);  

  sprintf(bfr1,"%s/%s.%d.%%y%%m%%d.%%H%%M%%S.log",RSTRING_PTR(lp_v),RSTRING_PTR(nm_v),getpid());
  strftime(bfr2,256,bfr1,tp);

  freopen(bfr2,"w+",stderr);
  setvbuf(stderr,NULL,_IONBF,0);  
  
  return self;
}

static VALUE moo_core_unlimited(VALUE self)
{
  struct rlimit core_limit;

  core_limit.rlim_cur=RLIM_INFINITY;
  core_limit.rlim_max=RLIM_INFINITY;

  if(setrlimit(RLIMIT_CORE,&core_limit)<0)
  {
//    mrb_raisef(mrb,E_TYPE_ERROR,"%S: Error in setrlimit (%S)!",mrb_str_new_cstr(mrb,__func__),
//             mrb_str_new_cstr(mrb,strerror(errno)));
    lg("%s: !!! error in setrlimit (%s)",__func__,strerror(errno));
    return Qfalse;
  }

  return self;
}

static VALUE moo_set_stack_size(VALUE self,VALUE v_size)
{
  struct rlimit limit;
  int v=FIX2INT(v_size);

  limit.rlim_cur=limit.rlim_max=v;

  if(setrlimit(RLIMIT_STACK,&limit)<0)
  {
//    mrb_raisef(mrb,E_TYPE_ERROR,"%S: Error in setrlimit (%S)!",mrb_str_new_cstr(mrb,__func__),
//             mrb_str_new_cstr(mrb,strerror(errno)));
    lg("%s: !!! error in setrlimit for a stack of %d (%s)",__func__,v,strerror(errno));
    return Qfalse;
  }

  return self;
}

//#define QV_DUMP

static VALUE moo_quick_verify(VALUE self,VALUE v_plat,VALUE v_rotcenter,VALUE v_beg_lin,VALUE v_beg_ang,
			      VALUE v_steps,VALUE v_timestep,VALUE v_max_allowed_vel)
{
  threed_base_stc b;
  float timestep=NUM2DBL(v_timestep),max_allowed_vel=NUM2DBL(v_max_allowed_vel);
  int n_steps=RARRAY_LEN(v_steps);
  hv beg[2],cur[2],targ[2],rotcenter;
  maths_casteljau_stc interpolator;
  struct timespec t1,t2;

#ifdef QV_DUMP
  char qvfn[256];
  sprintf(qvfn,"/tmp/qv.%ld.csv",time(NULL));
  FILE *qvf=fopen(qvfn,"w");  
#endif  

  interpolator.n_steps=6;
  interpolator.steps=malloc(sizeof(double)*12);
  interpolator.steps[0]=interpolator.steps[1]=interpolator.steps[2]=0.0;
  interpolator.steps[3]=interpolator.steps[4]=interpolator.steps[5]=1.0;  

  clock_gettime(CLOCK_MONOTONIC,&t1);  

  memcpy(&b,RSTRING_PTR(v_plat),sizeof(threed_base_stc));
  memcpy(rotcenter,RSTRING_PTR(v_rotcenter),sizeof(hv));

  platf_stc p;

  build_platform((float *)b.lower_joints,(float *)b.upper_joints,rotcenter,&p);

  step_stc *steps=malloc(sizeof(step_stc)*n_steps),*sp;
  int i,j;
  VALUE v,v2;

  for(i=0;i<3;i++)
  {
    beg[0][i]=cur[0][i]=NUM2DBL(rb_ary_entry(v_beg_lin,i));
    beg[1][i]=cur[1][i]=NUM2DBL(rb_ary_entry(v_beg_ang,i));
  }

  bzero(steps,sizeof(step_stc)*n_steps);
  
  for(sp=steps,i=0;i<n_steps;i++,sp++)
  {
    v=rb_ary_entry(v_steps,i);
    v2=rb_ary_entry(v,0);

    sp->algo=FIX2INT(rb_ary_entry(v,2));

    if(v2==Qnil)
      sp->sleep_flg=1;
    else if(sp->algo!=ALG_SINEACC && sp->algo!=ALG_DAMPEDOSC)
      for(j=0;j<3;j++)
      {
	sp->lin[j]=NUM2DBL(rb_ary_entry(v2,j));
	sp->ang[j]=NUM2DBL(rb_ary_entry(v2,j+3));
      }
    
    switch(sp->algo)
    {
    case ALG_LIN:
    case ALG_SIN:
    case ALG_CASTELJAU:
      sp->time=NUM2DBL(rb_ary_entry(v,1));
      break;
    case ALG_SINEACC:
      sp->special_step_v=rb_ary_entry(v,1);
      Data_Get_Struct(sp->special_step_v,maths_accsin_stc,sp->special_step_ptr);
      sp->time=((maths_accsin_stc *)sp->special_step_ptr)->time;
      memcpy(sp->lin,((maths_accsin_stc *)sp->special_step_ptr)->l.to,sizeof(hv));
      memcpy(sp->ang,((maths_accsin_stc *)sp->special_step_ptr)->a.to,sizeof(hv));      
      break;
    case ALG_DAMPEDOSC:
      sp->special_step_v=rb_ary_entry(v,1);
      TypedData_Get_Struct(sp->special_step_v,maths_dho_stc,&maths_dho_dtype,sp->special_step_ptr);

      maths_dho_stc *d=(maths_dho_stc *)sp->special_step_ptr;

      sp->time=d->t_step*d->n_steps;
      memcpy(sp->lin,d->l.steps[d->n_steps-1],sizeof(hv));
      memcpy(sp->ang,d->a.steps[d->n_steps-1],sizeof(hv));
      break;
    }
  }

  memcpy(targ[0],steps[0].lin,sizeof(hv));
  memcpy(targ[1],steps[0].ang,sizeof(hv));  

  float f,time_idea,time_at_beg_step,step_pos,*begp,*curp,*targp,old_actu[MOO_N_ACTU];
  double cur_vel=0.0,max_vel=0.0;
  int step_idea,broken_actu=-1;
  actu_stc *actup;
  uint8_t first_time_flg;
  moo_quicktest_error error_type=QT_NO_ERROR;
    
  bzero(old_actu,sizeof(float)*MOO_N_ACTU);
  
  for(first_time_flg=1,time_idea=time_at_beg_step=0.0,step_idea=0,sp=steps+step_idea;broken_actu<0;time_idea+=timestep)
  {
    while(time_at_beg_step+sp->time<time_idea) // go to next step
    {      
      step_idea++;
      if(step_idea>=n_steps)
	break;
      time_at_beg_step+=sp->time;
      sp=steps+step_idea;

      if(!sp->sleep_flg)
      {
	memcpy(beg,targ,sizeof(hv)*2);	
	memcpy(targ[0],sp->lin,sizeof(hv));
	memcpy(targ[1],sp->ang,sizeof(hv));

//	lg("now ==> POS x %10.3f-%10.3f y %10.3f-%10.3f z %10.3f-%10.3f YPR y %10.3f-%10.3f p %10.3f-%10.3f r %10.3f-%10.3f. LASTS %f",
//	   beg[0][0],targ[0][0],beg[0][1],targ[0][1],beg[0][2],targ[0][2],
//	   beg[1][0],targ[1][0],beg[1][1],targ[1][1],beg[1][2],targ[1][2],sp->time);
	
      }
    }
    
    if(step_idea>=n_steps || broken_actu>=0)
      break;
    
    if(!sp->sleep_flg)
    {
      f=(time_idea-time_at_beg_step)/sp->time;

      if(sp->algo==ALG_SINEACC)
      {
	v=rb_funcall(sp->special_step_v,rb_intern("pos_at"),1,DBL2NUM(f));
	memcpy(cur[0],RSTRING_PTR(rb_ary_entry(v,0)),sizeof(hv));
	memcpy(cur[1],RSTRING_PTR(rb_ary_entry(v,1)),sizeof(hv));
	memcpy(p.pos,cur[0],sizeof(hv));
	memcpy(p.ypr,cur[1],sizeof(hv));
//	lg("T%6.3f ==> POS x %10.3f y %10.3f z %10.3f YPR y %10.3f p %10.3f r %10.3f",
//	   time_idea,p.pos[0],p.pos[1],p.pos[2],p.ypr[0]*180.0/M_PI,p.ypr[1]*180.0/M_PI,p.ypr[2]*180.0/M_PI);
      }
      else if(sp->algo==ALG_DAMPEDOSC)
      {
	v=rb_funcall(sp->special_step_v,rb_intern("pos_at"),1,DBL2NUM(f));
	memcpy(cur[0],RSTRING_PTR(rb_ary_entry(v,0)),sizeof(hv));
	memcpy(cur[1],RSTRING_PTR(rb_ary_entry(v,1)),sizeof(hv));
	memcpy(p.pos,cur[0],sizeof(hv));
	memcpy(p.ypr,cur[1],sizeof(hv));
      }
      else
      {
	switch(sp->algo)
	{
	case ALG_LIN:
	  step_pos=f;
	  break;
	case ALG_SIN:
	  step_pos=sinf(f*M_PI-M_PI_2)*0.5+0.5;
	  break;
	case ALG_CASTELJAU:
	  step_pos=casteljau_calc(&interpolator,f);
	  break;
	default:
	  rb_raise(rb_eArgError,"%s: Sorry: interpolator %d not supported!",__func__,sp->algo);
	}      
	
	for(begp=(float *)beg,curp=(float *)cur,targp=(float *)targ,i=0;i<6;i++,begp++,curp++,targp++)
	  (*curp)=(*begp)+((*targp)-(*begp))*step_pos;

	memcpy(p.pos,cur[0],sizeof(hv));
	memcpy(p.ypr,cur[1],sizeof(hv));
      }
      
      apply_motion(&p);

/*
 * Velocity
 */

      for(actup=p.actu,i=0;i<MOO_N_ACTU;i++,actup++)
      {
	if(!first_time_flg) // not first step
	{
	  cur_vel=(fabs(old_actu[i]-actup->extension)/1000.0)/timestep;
	  if(cur_vel>max_vel)
	    max_vel=cur_vel;
	  if(cur_vel>max_allowed_vel)
	  {
	    lg("Ahia VEL %g %f on actu %d (%.2f to %.2f)",cur_vel,max_allowed_vel,i,old_actu[i],actup->extension);
	    broken_actu=i;
	    error_type=QT_ERROR_FAST;
	    break;
	  }
//	  lg("%6d: %d Vel %g",(int)(time_idea*1000),i,cur_vel);
	}
	old_actu[i]=actup->extension;
      }

      /* if(max_vel<=0.0) */
      /* { */
      /* 	broken_actu=MOO_N_ACTU; */
      /* 	error_type=QT_ERROR_TOOSLOW; */
      /* } */
      
      first_time_flg=0;

/*
 * Actuator lengths
 */

      for(actup=p.actu,i=0;i<MOO_N_ACTU;i++,actup++)
      {
	if(actup->extension<=MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM)
	{
	  lg("Ahia MIN %f %f (actu %d, at time %f)",actup->extension,MOO_ACTU_MIN_MM+MOO_ACTU_FIXED_MM,i,time_idea);
	  broken_actu=i;
	  error_type=QT_ERROR_SHORT;
	  break;
	}
	if(actup->extension>=MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM)
	{
	  lg("Ahia MAX %f %f (actu %d, at time %f)",actup->extension,MOO_ACTU_MAX_MM+MOO_ACTU_FIXED_MM,i,time_idea);
	  broken_actu=i;
	  error_type=QT_ERROR_LONG;
	  break;
	}
      }
    }
    
    /* lg("At %2.2d:%2.2d:%3.3d (%3d): %.3f %.3f %.3f %.3f %.3f %.3f (ba %d, sptime %f) Lin %.3f %.3f %.3f", */
    /*    (int)(time_idea/60.0),(int)time_idea%60,((int)(time_idea*1000.0)%1000),step_idea, */
    /*    p.actu[0].extension,p.actu[1].extension,p.actu[2].extension,p.actu[3].extension,p.actu[4].extension,p.actu[5].extension,broken_actu,sp->time, */
    /*    p.pos[0],p.pos[1],p.pos[2]); */
    
#ifdef QV_DUMP
    fprintf(qvf,"%f,%f,%f,%f,%f,%f,%f\n",time_idea,p.actu[0].extension,p.actu[1].extension,p.actu[2].extension,
	    p.actu[3].extension,p.actu[4].extension,p.actu[5].extension);
#endif  
  }

#ifdef QV_DUMP
  fclose(qvf);
#endif  
  
  free(steps);
  free(interpolator.steps);
  
  clock_gettime(CLOCK_MONOTONIC,&t2);
  
  lg("Quick_verify for %2.2d:%2.2d:%3.3d took %d usec (vel %g, %s)",
     (int)(time_idea/60.0),(int)time_idea%60,((int)(time_idea*1000.0)%1000),timediff_usec(&t1,&t2),max_vel,max_vel==0.0 ? "ZERO!" : "nonz");
  
  if(broken_actu<0)
  {
    VALUE to_ret=rb_ary_new_capa(2);
    rb_ary_store(to_ret,0,Qtrue);
    rb_ary_store(to_ret,1,DBL2NUM(max_vel));
    
    return to_ret;
  }
  
  VALUE to_ret=rb_ary_new_capa(12+MOO_N_ACTU);
  
  rb_ary_store(to_ret,0,INT2FIX((int)(time_idea*1000.0)));
  rb_ary_store(to_ret,1,INT2FIX(step_idea));
  rb_ary_store(to_ret,2,INT2FIX((int)((time_idea-time_at_beg_step)*1000.0)));
  rb_ary_store(to_ret,3,INT2FIX(broken_actu));
  rb_ary_store(to_ret,4,INT2FIX(error_type));
  
  for(i=0;i<3;i++)
  {
    rb_ary_store(to_ret,5+i,DBL2NUM(p.pos[i]));
    rb_ary_store(to_ret,8+i,DBL2NUM(p.ypr[i]));
  }  
  for(actup=p.actu,i=0;i<MOO_N_ACTU;i++,actup++)
    rb_ary_store(to_ret,11+i,DBL2NUM(actup->extension));
  rb_ary_store(to_ret,11+MOO_N_ACTU,DBL2NUM(max_vel));
  
  return to_ret;
}

static VALUE moo_moog_idea_to_server_idea(VALUE self,VALUE v_a)
{
  float mi[6],si[6];
  hv sl,sa;
  int i;

  for(i=0;i<6;i++)
    mi[i]=NUM2DBL(rb_ary_entry(v_a,i));

  moogidea2serveridea(mi,sl,sa);

  VALUE to_ret=rb_ary_new_capa(6);

  for(i=0;i<3;i++)
  {
    rb_ary_store(to_ret,i,DBL2NUM(sl[i]));
    rb_ary_store(to_ret,i+3,DBL2NUM(sa[i]));
  }

  return to_ret;
}

static VALUE moo_server_idea_to_moog_idea(VALUE self,VALUE v_a)
{
  hv sl,sa;
  float mi[6];
  int i;

  for(i=0;i<3;i++)
  {
    sl[i]=NUM2DBL(rb_ary_entry(v_a,i));
    sa[i]=NUM2DBL(rb_ary_entry(v_a,i+3));
  }

  serveridea2moogidea(sl,sa,mi);

  VALUE to_ret=rb_ary_new_capa(6);

  for(i=0;i<6;i++)
    rb_ary_store(to_ret,i,DBL2NUM(mi[i]));

  return to_ret;
}

void lg(const char *fmt,...)
{
  char tstr[17];
  time_t now=time(NULL);
  va_list ap;

  strftime(tstr,17,"[%y%m%d.%H%M%S] ",localtime(&now));
  fprintf(stderr,tstr);  
  
  va_start(ap,fmt);
  vfprintf(stderr,fmt,ap);
  va_end(ap);

  fputc('\n',stderr);
  fflush(stderr);
}

void hexprint(char *ptr,int len)
{
  int i,j;

  for(i=0;i<len;i+=LN)
  {
    fprintf(stderr,"%6.6x: ",i);
    for(j=0;j<LN;j++)
    {
      if(i+j<len)
        fprintf(stderr,"%2.2x ",ptr[i+j]);
      else
        fputs("   ",stderr);
    }
    fputs("| ",stderr);
    for(j=0;j<LN;j++)
      if(i+j<len)
        fprintf(stderr,"%c",isprint(ptr[i+j]) ? ptr[i+j] : '.');
    fputc('\n',stderr);
  }
}

inline uint8_t timepast(struct timespec *t1,struct timespec *t2)
{
  if(t2->tv_sec>t1->tv_sec)
    return 1;
  if(t2->tv_sec<t1->tv_sec)
    return 0;
  return (t2->tv_nsec>t1->tv_nsec) ? 1 : 0;
}

inline void timeadd(struct timespec *t1,unsigned long long int usecs)
{
  t1->tv_nsec+=usecs*1000LL;
  t1->tv_sec+=t1->tv_nsec/1000000000LL;
  t1->tv_nsec%=1000000000LL;
}

inline long long int timediff(struct timespec *t1,struct timespec *t2)
{
  return ((t2->tv_sec-t1->tv_sec)*1000000000LL+t2->tv_nsec-t1->tv_nsec)/1000000LL;
}

inline long long int timediff_from_now(struct timespec *t)
{
  struct timespec now;  

  clock_gettime(CLOCK_MONOTONIC,&now);

  return timediff(t,&now);
}

inline long long int timediff_usec(struct timespec *t1,struct timespec *t2)
{
  return ((t2->tv_sec-t1->tv_sec)*1000000LL)+(t2->tv_nsec-t1->tv_nsec)/1000LL;
}

static const char *command_desc(int cmd)
{
  int i;

  for(i=0;commands[i]>=0;i++)
    if(cmd==commands[i])
      return command_labels[i];

  return "*UNKNOWN*";
}

void moogidea2serveridea(const float mi[6],hv sl,hv sa)
{
  sl[0]=mi[5]*1000.0; // lateral
  sl[1]=(-mi[2]+MOO_MOOG_H_TO_SRV_H)*1000.0; // heave
  sl[2]=mi[3]*1000.0; // surge
  
  sa[0]=mi[4]; // YAW
  sa[1]=-mi[1]; // PITCH
  sa[2]=-mi[0]; // ROLL
//  lg("=> mi %.3f %.3f %.3f %.3f %.3f %.3f sl %.3f %.3f %.3f sa %.3f %.3f %.3f",mi[0],mi[1],mi[2],mi[3],mi[4],mi[5],sl[0],sl[1],sl[2],sa[0],sa[1],sa[2]);
}

void serveridea2moogidea(const hv sl,const hv sa,float mi[6])
{
  mi[0]=-sa[2]; // ROLL
  mi[1]=-sa[1]; // PITCH

  mi[2]=-(sl[1]/1000.0-MOO_MOOG_H_TO_SRV_H); // heave
  mi[3]=sl[2]/1000.0; // surge

  mi[4]=sa[0]; // YAW

  mi[5]=sl[0]/1000.0; // lateral
}
