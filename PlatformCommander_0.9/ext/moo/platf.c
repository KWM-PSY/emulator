/* platf.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * C code managing the virtual idea of platform.
 * Was my first approach to managing the platform. Still used even if not so extensively.
 */

#include <maths_include.h>
#include "moo.h"

typedef struct
{
  platf_stc p;
  int actu_park,actu_min,actu_max;
} moo_platf_stc;

extern VALUE cls_moo_platf;

static void free_moo_platf(void *p);
void apply_motion(platf_stc *p);
void build_platform(float *bv,float *bvr,hv rotcenter,platf_stc *p);
void modify_platform_rotcenter(platf_stc *p,hv rotcenter);

const static hv vert_v={0.0,1.0,0.0};

VALUE new_moo_platf(VALUE self,VALUE v_base,VALUE v_rotbase,VALUE v_rotcenter,VALUE v_actu_min,VALUE v_actu_max,VALUE v_actu_park)
{
  moo_platf_stc *s;
  VALUE sdata=Data_Make_Struct(cls_moo_platf,moo_platf_stc,NULL,free_moo_platf,s);

  bzero(s,sizeof(moo_platf_stc));

  float *bv=(float *)RSTRING_PTR(v_base),*bvr=(float *)RSTRING_PTR(v_rotbase),dist,ang;
  hv rotcenter;
  int i;

  memcpy(rotcenter,RSTRING_PTR(v_rotcenter),sizeof(hv));
  
  build_platform(bv,bvr,rotcenter,&(s->p));
  
  s->actu_min=FIX2INT(v_actu_min);
  s->actu_max=FIX2INT(v_actu_max);
  s->actu_park=FIX2INT(v_actu_park);
  
  return sdata;
}

static void free_moo_platf(void *p)
{
  moo_platf_stc *s=(moo_platf_stc *)p;

  free(s);
}

VALUE moo_platf_move(VALUE self,VALUE v_vals)
{
  moo_platf_stc *s;  
  Data_Get_Struct(self,moo_platf_stc,s);

  int i;
  actu_stc *acp;
  
  for(i=0;i<3;i++)
  {
    s->p.pos[i]=NUM2DBL(rb_ary_entry(v_vals,i));
    s->p.ypr[i]=NUM2DBL(rb_ary_entry(v_vals,i+3));
  }
  
  apply_motion(&s->p);

  VALUE to_ret=rb_ary_new_capa(7),v;
  
  for(acp=s->p.actu,i=0;i<6;i++,acp++)
  {
    if(acp->extension<s->actu_min)
      acp->fail=-1;
    else if(acp->extension>s->actu_max)
      acp->fail=1;
    else
      acp->fail=0;
    
    v=rb_ary_new_capa(3);
    rb_ary_store(v,0,rb_str_new((char *)acp->rotm,sizeof(hm4)));
    rb_ary_store(v,1,DBL2NUM(acp->extension));
    rb_ary_store(v,2,INT2FIX(acp->fail));
    rb_ary_store(to_ret,i,v);    
  }
  rb_ary_store(to_ret,6,rb_str_new((char *)s->p.rotm,sizeof(hm4)));    

  return to_ret;
}
  
VALUE moo_platf_try_motion(VALUE self,VALUE v_vfrom,VALUE v_vto,VALUE v_nsteps)
{
  moo_platf_stc *s;  
  Data_Get_Struct(self,moo_platf_stc,s);
  
  platf_stc p;
  hv posf,yprf,post,yprt;
  int i,j,nsteps=FIX2INT(v_nsteps);
  float frac=1.0/nsteps,fval,tval;
  actu_stc *acp;

  for(i=0;i<3;i++)
  {
    posf[i]=NUM2DBL(rb_ary_entry(v_vfrom,i));
    yprf[i]=NUM2DBL(rb_ary_entry(v_vfrom,i+3));
    post[i]=NUM2DBL(rb_ary_entry(v_vto,i));
    yprt[i]=NUM2DBL(rb_ary_entry(v_vto,i+3));
  }
  memcpy(&p,&s->p,sizeof(platf_stc));
  for(acp=p.actu,i=0;i<MOO_N_ACTU;i++,acp++)
  {
    acp->pf=p.joints[0]+i;
    acp->pt=p.joints[2]+((i+1)%MOO_N_ACTU);    
  }

  for(fval=1.0,tval=0.0,i=0;i<nsteps;i++,fval-=frac,tval+=frac)
  {
    for(j=0;j<3;j++)
    {
      p.pos[j]=posf[j]*fval+post[j]*tval;
      p.ypr[j]=yprf[j]*fval+yprt[j]*tval;
    }
//    lg("At step %d (%.2f,%.2f): pos %.2f, %.2f, %.2f ypr %.2f, %.2f, %.2f",i,fval,tval,
//       p.pos[0],p.pos[1],p.pos[2],p.ypr[0]*180.0/M_PI,p.ypr[1]*180.0/M_PI,p.ypr[2]*180.0/M_PI);
    
    apply_motion(&p);
    for(acp=p.actu,j=0;j<6;j++,acp++)
      if(acp->extension<s->actu_min || acp->extension>s->actu_max)
      {
	VALUE to_ret=rb_ary_new_capa(6);

	rb_ary_store(to_ret,0,INT2FIX(i));
	rb_ary_store(to_ret,1,INT2FIX(j));
	rb_ary_store(to_ret,2,acp->extension<s->actu_min ? Qfalse :Qtrue);
	rb_ary_store(to_ret,3,DBL2NUM(acp->extension));
	rb_ary_store(to_ret,4,rb_str_new((char *)p.pos,sizeof(hv)));
	rb_ary_store(to_ret,5,rb_str_new((char *)p.ypr,sizeof(hv)));

	return to_ret;
      }
  }

  return Qtrue;
}

void apply_motion(platf_stc *p)
{
  hv4 q;
  hm4 qm,m1,m2,m3;
  hv mv,traslvect,v;

  mat4_trasl(p->from_rotcenter,m1);
  vect_negate(p->ypr,v);
  mat4_tait_bryan(v,m2);
  mat4_mult(m1,m2,m3);
  mat4_trasl(p->to_rotcenter,m1);
  mat4_mult(m3,m1,qm);
  v[0]=-p->pos[0];
  v[1]=p->pos[1];
  v[2]=p->pos[2];
  mat4_trasl(v,m1);
  mat4_mult(qm,m1,p->rotm);

  memcpy(p->movplat_rotcenter_offs,qm[3],sizeof(hv));

  memcpy(p->joints[2],p->joints[1],sizeof(hv)*MOO_N_ACTU);

  int i;
  actu_stc *acp;

  for(acp=p->actu,i=0;i<MOO_N_ACTU;i++,acp++)
  {
    mat4_vect_mult(p->rotm,*(acp->pt),v);
    vect_sub(v,*(acp->pf),mv);
    acp->extension=vect_normalize(mv,traslvect);
    /* lg("==> p %.2f,%.2f,%.2f/%.2f,%.2f,%.2f ypr %.2f,%.2f,%.2f ,attu%d -> %.2f", */
    /*    (*(acp->pf))[0],(*(acp->pf))[1],(*(acp->pf))[2],v[0],v[1],v[2], */
    /*    p->ypr[0]*180.0/M_PI,p->ypr[1]*180.0/M_PI,p->ypr[2]*180.0/M_PI, */
    /*    i+1,acp->extension); */

    mat4_vect_over_another(traslvect,vert_v,acp->rotm);
  }
}

void build_platform(float *bv,float *bvr,hv rotcenter,platf_stc *p)
{
  actu_stc *acp;
  int i;
  
  bzero(p,sizeof(platf_stc));

  memcpy(p->joints[0],bv,sizeof(hv)*MOO_N_ACTU);
  memcpy(p->joints[1],bvr,sizeof(hv)*MOO_N_ACTU);
  memcpy(p->joints[2],bvr,sizeof(hv)*MOO_N_ACTU);

  modify_platform_rotcenter(p,rotcenter);
  
  for(acp=p->actu,i=0;i<MOO_N_ACTU;i++,acp++)
  {
    acp->pf=p->joints[0]+i;
    acp->pt=p->joints[2]+((i+1)%MOO_N_ACTU);
//    lg("ACCTU %d: %.3f %.3f %.3f <=> %.3f %.3f %.3f",i,
//       (*acp->pf)[0],(*acp->pf)[1],(*acp->pf)[2],
//       (*acp->pt)[0],(*acp->pt)[1],(*acp->pt)[2]);
  }  
}

void modify_platform_rotcenter(platf_stc *p,hv rotcenter)
{
  int i;
  
  memcpy(p->to_rotcenter,rotcenter,sizeof(hv));

  for(i=0;i<3;i++)
    p->from_rotcenter[i]=-p->to_rotcenter[i];
}
