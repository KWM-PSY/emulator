/* init.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Ruby extension for memory block handling from C
 */

#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <math.h>
#include <complex.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/time.h>
#include <linux/types.h>

static VALUE cls_memblock;

static VALUE new_memblock(VALUE self,VALUE v_tag,VALUE v_size);
static VALUE memblock_ptr(VALUE self);
static VALUE memblock_dump(VALUE self,VALUE v_unit);
static VALUE memblock_dump_as_ppm_32_24(VALUE self,VALUE v_name,VALUE v_w,VALUE v_h);

void Init_memblock(void)
{
  VALUE v;
  char **ptr;
  
  cls_memblock=rb_define_class("Memblock",rb_cObject);
  rb_define_singleton_method(cls_memblock,"new",new_memblock,2);
  rb_define_method(cls_memblock,"ptr",memblock_ptr,0);
  rb_define_method(cls_memblock,"dump",memblock_dump,1);
  rb_define_method(cls_memblock,"dump_as_ppm_32_24",memblock_dump_as_ppm_32_24,3);
}

typedef struct
{
  char *tag;
  int size;
  void *b;
} memblock_stc;

static void free_memblock(void *p);

static size_t memblock_memsize(const void *p)
{
  return sizeof(memblock_stc);
}

const rb_data_type_t memblock_dtype={"memblock",
				     {0,free_memblock,memblock_memsize,},
				     0,0,
				     RUBY_TYPED_FREE_IMMEDIATELY,};

static VALUE new_memblock(VALUE self,VALUE v_tag,VALUE v_size)
{
  memblock_stc *s;
  VALUE sdata=TypedData_Make_Struct(cls_memblock,memblock_stc,&memblock_dtype,s);
  
  bzero(s,sizeof(memblock_stc));

  s->tag=strdup(RSTRING_PTR(v_tag));
  s->size=FIX2INT(v_size);
  s->b=malloc(s->size);

  if(!s->b)
    rb_raise(rb_eArgError,"%s: could not create memblock %s of size %d (%s)",__func__,s->tag,s->size,strerror(errno));

  bzero(s->b,s->size);

  return sdata;
}
  
static void free_memblock(void *p)
{
  memblock_stc *s=(memblock_stc *)p;

  free(s->b);
  free(s);
}

static VALUE memblock_ptr(VALUE self)
{
  memblock_stc *s;  
  TypedData_Get_Struct(self,memblock_stc,&memblock_dtype,s);

  uint64_t ptr_v=(uint64_t)(s->b);    
  return rb_str_new((char *)(&ptr_v),sizeof(uint64_t));
}

static VALUE memblock_dump(VALUE self,VALUE v_unit)
{
  memblock_stc *s;  
  TypedData_Get_Struct(self,memblock_stc,&memblock_dtype,s);
  
  int unit=FIX2INT(v_unit);

  write(unit,s->b,s->size);
  return self;
}

static VALUE memblock_dump_as_ppm_32_24(VALUE self,VALUE v_name,VALUE v_w,VALUE v_h)
{
  memblock_stc *s;  
  TypedData_Get_Struct(self,memblock_stc,&memblock_dtype,s);

  int w=FIX2INT(v_w),h=FIX2INT(v_h);

  if(w*h*4!=s->size)
    rb_raise(rb_eArgError,"%s: w=%d and h=%d, but size %d (should be %d)",__func__,w,h,w*h*4,s->size);
    
  char *name=RSTRING_PTR(v_name);  
  FILE *unit=fopen(name,"w");

  if(!unit)
    rb_raise(rb_eArgError,"%s: could not create output file %s (%s)",__func__,name,strerror(errno));

  fprintf(unit,"P6\n%d %d\n255\n",w,h);

  uint8_t *ptr;
  int i;  

  for(ptr=s->b,i=0;i<w*h;i++,ptr+=4)    
    fwrite(ptr,3,1,unit);
  
  fclose(unit);
  return self;
}

