/* init.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Some generic utilities in C
 */

#include "carlobase.h"
#include <sys/ioctl.h>
#include <linux/fs.h>

#define LN 16

VALUE mod_carlogen,cls_xml;
  
static VALUE cg_time_now(VALUE self);
static VALUE cg_time_diff(VALUE self,VALUE v_t1,VALUE v_t2);
static VALUE cg_time_diff_usec(VALUE self,VALUE v_t1,VALUE v_t2);
static VALUE cg_time_diff_from_now(VALUE self,VALUE v_t);
static VALUE cg_time_add(VALUE self,VALUE v_t,VALUE v_sec);
static VALUE cg_msec_from_timespec(VALUE self,VALUE v_ts);

static VALUE cg_hexprint(VALUE self,VALUE v_tag,VALUE v_s);

static VALUE cg_filesystem_size(VALUE self,VALUE v_path);
static VALUE cg_write_at_offset(VALUE self,VALUE v_path,VALUE v_offset,VALUE v_stuff);
static VALUE cg_read_at_offset(VALUE self,VALUE v_path,VALUE v_offset,VALUE v_len);

static VALUE cg_frame_from_grey(VALUE self,VALUE v_f);
static VALUE cg_frame_tobiirecord_valextract(VALUE self,VALUE v_s);

extern VALUE cg_xml_parse_file(VALUE self,VALUE fname_v);
extern VALUE cg_xml_parse_data(VALUE self,VALUE stuff_v);

void Init_carlobase(void)
{
  VALUE v;
  
  mod_carlogen=rb_define_module("Cg");

  rb_define_const(mod_carlogen,"SIZEOF_STRUCT_TIMESPEC",INT2FIX(sizeof(struct timespec)));

  rb_define_singleton_method(mod_carlogen,"time_now",cg_time_now,0);
  rb_define_singleton_method(mod_carlogen,"time_diff",cg_time_diff,2);
  rb_define_singleton_method(mod_carlogen,"time_diff_usec",cg_time_diff_usec,2);
  rb_define_singleton_method(mod_carlogen,"time_diff_from_now",cg_time_diff_from_now,1);
  rb_define_singleton_method(mod_carlogen,"time_add",cg_time_add,2);
  rb_define_singleton_method(mod_carlogen,"msec_from_timespec",cg_msec_from_timespec,1);

  rb_define_singleton_method(mod_carlogen,"hexprint",cg_hexprint,2);

  rb_define_singleton_method(mod_carlogen,"filesystem_size",cg_filesystem_size,1);
  rb_define_singleton_method(mod_carlogen,"write_at_offset",cg_write_at_offset,3);
  rb_define_singleton_method(mod_carlogen,"read_at_offset",cg_read_at_offset,3);

  rb_define_singleton_method(mod_carlogen,"frame_from_grey",cg_frame_from_grey,1);
  rb_define_singleton_method(mod_carlogen,"frame_tobiirecord_valextract",cg_frame_tobiirecord_valextract,1);

  cls_xml=rb_define_class_under(mod_carlogen,"Xml",rb_cObject);
  rb_define_singleton_method(cls_xml,"parse_file",cg_xml_parse_file,1);
  rb_define_singleton_method(cls_xml,"parse_data",cg_xml_parse_data,1);
}

void lg(const char *fmt,...)
{
  char tstr[17];
  time_t now=time(NULL);
  va_list ap;

  strftime(tstr,17,"[%y%m%d.%H%M%S] ",localtime(&now));
  fprintf(stderr,tstr);  
  
  va_start(ap,fmt);
  vfprintf(stderr,fmt,ap);
  va_end(ap);

  fputc('\n',stderr);
  fflush(stderr);
}

void hexprint(char *ptr,int len)
{
  int i,j;

  for(i=0;i<len;i+=LN)
  {
    fprintf(stderr,"%6.6x: ",i);
    for(j=0;j<LN;j++)
    {
      if(i+j<len)
        fprintf(stderr,"%2.2x ",ptr[i+j]);
      else
        fputs("   ",stderr);
    }
    fputs("| ",stderr);
    for(j=0;j<LN;j++)
      if(i+j<len)
        fprintf(stderr,"%c",isprint(ptr[i+j]) ? ptr[i+j] : '.');
    fputc('\n',stderr);
  }
}

inline uint8_t timepast(struct timespec *t1,struct timespec *t2)
{
  if(t2->tv_sec>t1->tv_sec)
    return 1;
  if(t2->tv_sec<t1->tv_sec)
    return 0;
  return (t2->tv_nsec>t1->tv_nsec) ? 1 : 0;
}

inline void timeadd(struct timespec *t1,unsigned long long int usecs)
{
  t1->tv_nsec+=usecs*1000LL;
  t1->tv_sec+=t1->tv_nsec/1000000000LL;
  t1->tv_nsec%=1000000000LL;
}

inline long long int timediff(struct timespec *t1,struct timespec *t2)
{
  return ((t2->tv_sec-t1->tv_sec)*1000000000LL+t2->tv_nsec-t1->tv_nsec)/1000000LL;
}

inline long long int timediff_from_now(struct timespec *t)
{
  struct timespec now;  

  clock_gettime(CLOCK_MONOTONIC,&now);

  return timediff(t,&now);
}

inline long long int timediff_usec(struct timespec *t1,struct timespec *t2)
{
  return ((t2->tv_sec-t1->tv_sec)*1000000LL)+(t2->tv_nsec-t1->tv_nsec)/1000LL;
}


static VALUE cg_time_now(VALUE self)
{
  time_t t1;
  struct timespec t2;  

  t1=time(NULL);
  clock_gettime(CLOCK_MONOTONIC,&t2);  

  VALUE to_ret=rb_ary_new_capa(2);

  rb_ary_store(to_ret,0,rb_str_new((char *)&t1,sizeof(time_t)));
  rb_ary_store(to_ret,1,rb_str_new((char *)&t2,sizeof(struct timespec)));

  return to_ret;
}

static VALUE cg_time_diff(VALUE self,VALUE v_t1,VALUE v_t2)
{
  struct timespec *t1=(struct timespec *)RSTRING_PTR(v_t1),*t2=(struct timespec *)RSTRING_PTR(v_t2);

  return INT2NUM(timediff(t1,t2));
}

static VALUE cg_time_diff_usec(VALUE self,VALUE v_t1,VALUE v_t2)
{
  struct timespec *t1=(struct timespec *)RSTRING_PTR(v_t1),*t2=(struct timespec *)RSTRING_PTR(v_t2);

  return INT2NUM(timediff_usec(t1,t2));
}

static VALUE cg_time_diff_from_now(VALUE self,VALUE v_t)
{
  struct timespec *t=(struct timespec *)RSTRING_PTR(v_t),now;  

  clock_gettime(CLOCK_MONOTONIC,&now);

  return INT2NUM(timediff(t,&now));
}

static VALUE cg_time_add(VALUE self,VALUE v_t,VALUE v_sec)
{
  struct timespec t;
  float sec=NUM2DBL(v_sec);

  memcpy(&t,RSTRING_PTR(v_t),sizeof(struct timespec));
  timeadd(&t,(unsigned long long int)(sec*1000000.0));

  return rb_str_new((char *)&t,sizeof(struct timespec));
}

static VALUE cg_msec_from_timespec(VALUE self,VALUE v_ts)
{
  struct timespec *ts=(struct timespec *)RSTRING_PTR(v_ts);

  return INT2NUM(ts->tv_sec*1000+ts->tv_nsec/1000000LL);
}

static VALUE cg_hexprint(VALUE self,VALUE v_tag,VALUE v_s)
{
  lg("%s: [%s]",__func__,RSTRING_PTR(v_tag));
  hexprint(RSTRING_PTR(v_s),RSTRING_LEN(v_s));

  return self;
}

static VALUE cg_filesystem_size(VALUE self,VALUE v_path)
{
  int unit=open(RSTRING_PTR(v_path),O_RDONLY);

  if(unit<=0)
  {
    lg("%s: could not open %s (%s)",__func__,RSTRING_PTR(v_path),strerror(errno));
    return Qfalse;
  }

  unsigned long long fs;
  int res=ioctl(unit,BLKGETSIZE64,&fs);

  if(res<0)
  {
    lg("%s: could not get size of %s (%s)",__func__,RSTRING_PTR(v_path),strerror(errno));
    return Qfalse;
  }

  close(unit);

  return ULONG2NUM(fs);
}

static VALUE cg_write_at_offset(VALUE self,VALUE v_path,VALUE v_offset,VALUE v_stuff)
{
  int unit=open(RSTRING_PTR(v_path),O_WRONLY);

  if(unit<=0)
  {
    lg("%s: could not open %s (%s)",__func__,RSTRING_PTR(v_path),strerror(errno));
    return Qfalse;
  }

  off_t offset=NUM2ULONG(v_offset),olen=RSTRING_LEN(v_stuff),len=lseek(unit,0L,SEEK_END);

  if(len<offset+olen)
  {
    close(unit);
    lg("%s: stuff does not fit in %s (%lld/%lld/%lld)",__func__,RSTRING_PTR(v_path),offset,olen,len);
    return Qfalse;
  }

  lseek(unit,offset,SEEK_SET);
  int ret=write(unit,RSTRING_PTR(v_stuff),olen);
  close(unit);

  if(ret!=olen)
  {
    lg("%s: could not write %lld bytes on %s (%s)",__func__,olen,RSTRING_PTR(v_path),strerror(errno));
    return Qfalse;
  }

  return Qtrue;
}

static VALUE cg_read_at_offset(VALUE self,VALUE v_path,VALUE v_offset,VALUE v_len)
{
  int unit=open(RSTRING_PTR(v_path),O_RDONLY);

  if(unit<=0)
  {
    lg("%s: could not open %s (%s)",__func__,RSTRING_PTR(v_path),strerror(errno));
    return Qfalse;
  }

  off_t offset=NUM2ULONG(v_offset),olen=NUM2ULONG(v_len),len=lseek(unit,0L,SEEK_END);
  
  if(len<offset+olen)
  {
    close(unit);
    lg("%s: no space for stuff in %s (%lld/%lld/%lld)",__func__,RSTRING_PTR(v_path),offset,olen,len);
    return Qfalse;
  }

  lseek(unit,offset,SEEK_SET);
  uint8_t stuff[olen];  
  int ret=read(unit,stuff,olen);
  close(unit);

  if(ret!=olen)
  {
    lg("%s: could not read %lld bytes from %s (%s)",__func__,olen,RSTRING_PTR(v_path),strerror(errno));
    return Qfalse;
  }

  return rb_str_new((char *)stuff,olen);
}

static VALUE cg_frame_from_grey(VALUE self,VALUE v_f)
{
  int len=RSTRING_LEN(v_f),i;
  uint8_t *bfr=malloc(len*3),*p1,*p2;

  for(p1=(uint8_t *)RSTRING_PTR(v_f),p2=bfr,i=0;i<len;i++,p1++,p2+=3)
    p2[0]=p2[1]=p2[2]=*p1;

  VALUE to_ret=rb_str_new((char *)bfr,len*3);

  free(bfr);
  return to_ret;
}

static uint16_t valextract_be_u16(uint8_t *p)
{
  uint16_t v=p[0]<<8|p[1];

  return v;
}
static int16_t valextract_be_s16(uint8_t *p)
{
  int16_t v=p[0]<<8|p[1];

  return v;
}
static uint32_t valextract_be_u32(uint8_t *p)
{
  uint32_t v=p[0]<<24|p[1]<<16|p[2]<<8|p[3];

  return v;
}
static int32_t valextract_be_s32(uint8_t *p)
{
  int32_t v=p[0]<<24|p[1]<<16|p[2]<<8|p[3];

  return v;
}
  
static VALUE cg_frame_tobiirecord_valextract(VALUE self,VALUE v_s)
{
  uint8_t *b=(uint8_t *)RSTRING_PTR(v_s);
  VALUE to_ret=rb_ary_new(),v;

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_u32(b+0x40)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x64]+b[0x65]+b[0x66]+b[0x67])>0 ? Qtrue : Qfalse);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x64)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x6d]+b[0x6e]+b[0x6f]+b[0x70])>0 ? Qtrue : Qfalse);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x6d)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x76]+b[0x77]+b[0x78]+b[0x79])>0 ? Qtrue : Qfalse);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x76)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0xb5)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0xbe)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s16(b+0xc9)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0xfd]==0xff && b[0xfe]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0xfd)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x119]==1) ? Qtrue : Qfalse);
  rb_ary_store(v,1,Qnil);
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x13c]==0xff && b[0x13d]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x13e)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x149]==0xff && b[0x14a]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x14b)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x171]==0xff && b[0x172]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s16(b+0x173)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x17a]==0xff && b[0x17b]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s16(b+0x17c)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x1b9]+b[0x1ba]+b[0x1bb]+b[0x1bc])>0 ? Qtrue : Qfalse);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x1b9)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x1c2]+b[0x1c3]+b[0x1c4]+b[0x1c5])>0 ? Qtrue : Qfalse);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x1c2)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x1cb]+b[0x1cc]+b[0x1cd]+b[0x1ce])>0 ? Qtrue : Qfalse);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x1cb)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x1e9]==1) ? Qtrue : Qfalse);
  rb_ary_store(v,1,Qnil);
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x20a)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x213)));
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,b[0x21d]==0 ? Qtrue : Qfalse);
  rb_ary_store(v,1,INT2NUM(valextract_be_s16(b+0x21e)));
  rb_ary_push(to_ret,v);
  
  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x23a]==1) ? Qtrue : Qfalse);
  rb_ary_store(v,1,Qnil);
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x252]==0xff && b[0x253]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x252)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x26e]==1) ? Qtrue : Qfalse);
  rb_ary_store(v,1,Qnil);
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x291]==0xff && b[0x292]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x293)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x29e]==0xff && b[0x29f]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x2a0)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x2c6]==0xff && b[0x2c7]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s16(b+0x2c8)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x2cf]==0xff && b[0x2d0]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s16(b+0x2d1)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x2ed]==1) ? Qtrue : Qfalse);
  rb_ary_store(v,1,Qnil);
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_u16(b+0x322)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x344)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x356]==0xff && b[0x357]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s16(b+0x358)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x375]==1) ? Qtrue : Qfalse);
  rb_ary_store(v,1,Qnil);
  rb_ary_push(to_ret,v);

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x395)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,(b[0x39e]==0xff && b[0x39f]==0xff) ? Qfalse : Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x39e)));
  rb_ary_push(to_ret,v);  

  v=rb_ary_new_capa(2);
  rb_ary_store(v,0,Qtrue);
  rb_ary_store(v,1,INT2NUM(valextract_be_s32(b+0x3a7)));
  rb_ary_push(to_ret,v);

  return to_ret;
}

