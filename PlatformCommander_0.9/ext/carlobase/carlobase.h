/* carlobase.h */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Some generic C utilities - include file
 */

#ifndef CARLOBASE_H
#define CARLOBASE_H

/*
 * This one to stop compiler from complaining that
 * we include kernel headers directly
 */

#define __EXPORTED_HEADERS__

#include "ruby.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <ctype.h>

extern void lg(const char *fmt,...);
extern void hexprint(char *ptr,int len);

extern uint8_t timepast(struct timespec *t1,struct timespec *t2);
extern void timeadd(struct timespec *t1,unsigned long long int usecs);
extern long long int timediff(struct timespec *t1,struct timespec *t2);
extern long long int timediff_from_now(struct timespec *t);
extern long long int timediff_usec(struct timespec *t1,struct timespec *t2);

#endif
