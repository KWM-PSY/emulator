/* xml.c */

/*
 * ***--MANAGED--***
 * 
 * ==========================================================================
 * Copyright (C) 2020 Carlo Prelz, University of Bern
 * 
 * carlo.prelz@humdek.unibe.ch
 * 
 * This file is part of Platform Commander.
 * 
 * Platform Commander is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Platform Commander is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Platform Commander. If not, see <http://www.gnu.org/licenses/>.
 * ==========================================================================
 *
 * Code to parse XML files and save them into Ruby hashes (makes use of libxml)
 */

// xml.c

#include <maths_include.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

typedef struct xml_node
{
  int n_attrs,n_children;
  struct xml_node **children;
  char *name,***attrs,*content;
} xml_node_stc;

static VALUE recurse_add_to_value(xml_node_stc *n);
static xml_node_stc *load_xmlfile(char *path);
static xml_node_stc *load_xmldata(char *stuff);
static xml_node_stc *recurse_parse_tree(xmlNode *n);
static void recurse_free_parsed_tree(xml_node_stc *n);
//static char *xn_attr(xml_node_stc *n,char *id);
//static xml_node_stc *xn_child(xml_node_stc *n,char *id);

VALUE cg_xml_parse_file(VALUE self,VALUE fname_v)
{
  char *fname=RSTRING_PTR(fname_v);
  xml_node_stc *n=load_xmlfile(fname);
  VALUE to_ret=recurse_add_to_value(n);
  
  recurse_free_parsed_tree(n);
  
  return to_ret;
}

VALUE cg_xml_parse_data(VALUE self,VALUE stuff_v)
{
  char *stuff=malloc(RSTRING_LEN(stuff_v)+1);

  memcpy(stuff,RSTRING_PTR(stuff_v),RSTRING_LEN(stuff_v));
  stuff[RSTRING_LEN(stuff_v)]=0;

  xml_node_stc *n=load_xmldata(stuff);
  VALUE to_ret=recurse_add_to_value(n);
  
  recurse_free_parsed_tree(n);
  free(stuff);
  
  return to_ret;
}

static VALUE recurse_add_to_value(xml_node_stc *n)
{
  VALUE vattrs=rb_hash_new(),vchildren=rb_ary_new(),to_ret=rb_hash_new();
  int i;

  rb_hash_aset(to_ret,ID2SYM(rb_intern("name")),rb_str_new_cstr(n->name));

  if(n->content)
    rb_hash_aset(to_ret,ID2SYM(rb_intern("content")),rb_str_new_cstr(n->content));
  
  for(i=0;i<n->n_attrs;i++)
    rb_hash_aset(vattrs,ID2SYM(rb_intern(n->attrs[i][0])),rb_str_new_cstr(n->attrs[i][1]));
  
  for(i=0;i<n->n_children;i++)
    rb_ary_push(vchildren,recurse_add_to_value(n->children[i]));
  
  rb_hash_aset(to_ret,ID2SYM(rb_intern("attrs")),vattrs);
  rb_hash_aset(to_ret,ID2SYM(rb_intern("children")),vchildren);

  return to_ret;
}

static xml_node_stc *load_xmlfile(char *path)
{
  xmlDoc *f=xmlReadFile(path,NULL,XML_PARSE_HUGE);
  
  if(!f)
  {
    lg("Error parsing %s!",path);
    exit(1);
  }
  
  xml_node_stc *rn=recurse_parse_tree(xmlDocGetRootElement(f));
  
  xmlFreeDoc(f);
  xmlCleanupParser();

  return rn;
}

static xml_node_stc *load_xmldata(char *stuff)
{
  xmlDoc *f=xmlReadDoc((xmlChar *)stuff,NULL,NULL,XML_PARSE_HUGE);
  
  if(!f)
  {
    lg("Error parsing %d bytes!",strlen(stuff));
    exit(1);
  }
  
  xml_node_stc *rn=recurse_parse_tree(xmlDocGetRootElement(f));
  
  xmlFreeDoc(f);
  xmlCleanupParser();

  return rn;
}

static xml_node_stc *recurse_parse_tree(xmlNode *n)
{
  xml_node_stc *nn=malloc(sizeof(xml_node_stc));
  xmlAttr *attr;
  xmlChar *attrtxt,*contenttxt;  
  xmlNode *cur;
  int coldlen,clen;

  bzero(nn,sizeof(xml_node_stc));
  nn->name=strdup((char *)n->name);

  for(attr=n->properties;attr;attr=attr->next)
  {
    nn->attrs=realloc(nn->attrs,sizeof(char **)*(nn->n_attrs+1));
    nn->attrs[nn->n_attrs]=malloc(sizeof(char *)*2);
    attrtxt=xmlNodeGetContent((xmlNodePtr)attr);

    nn->attrs[nn->n_attrs][0]=strdup((char *)attr->name);
    nn->attrs[nn->n_attrs][1]=strdup((char *)attrtxt);
    
    xmlFree(attrtxt);

    nn->n_attrs++;
  }

  for(cur=n->children;cur;cur=cur->next)
  {
    if(cur->type==XML_ELEMENT_NODE)
    {
      nn->children=realloc(nn->children,sizeof(xml_node_stc *)*(nn->n_children+1));
      nn->children[nn->n_children]=recurse_parse_tree(cur);
      nn->n_children++;
    }
    else if(cur->type==XML_TEXT_NODE)
    {
      contenttxt=xmlNodeGetContent(cur);
      if(contenttxt)
      {
	coldlen=(nn->content ? strlen(nn->content) : 0);
	clen=coldlen+strlen((char *)contenttxt);
	nn->content=realloc(nn->content,clen+1);
	strcpy(nn->content+coldlen,(char *)contenttxt);
	
	xmlFree(contenttxt);
      }
    }
  }
  
  return nn;
}

static void recurse_free_parsed_tree(xml_node_stc *n)
{
  int i;
  
  free(n->name);  
  
  for(i=0;i<n->n_attrs;i++)
  {
    free(n->attrs[i][0]);
    free(n->attrs[i][1]);
    free(n->attrs[i]);
  }
  free(n->attrs);

  for(i=0;i<n->n_children;i++)
    recurse_free_parsed_tree(n->children[i]);
  
  free(n->children);

  free(n->content);

  free(n);
}
